﻿Imports System.IO

Module UserLocal


#Region "Windows authentication"

    'Windows authentication UserName.
    Function GetUserName() As String
        Dim Namexx As String = System.Environment.UserName
        Console.WriteLine("Read Usernamn " & Namexx)
        Return Namexx
    End Function

    'Windows authentication ComputerName.
    Function GetComputerName() As String
        Dim Namexx As String = System.Environment.MachineName
        Return Namexx
    End Function

    'Windows authentication OperativSystem.
    Function GetOperativSystem() As String
        Dim Namexx As String = My.Computer.Info.OSFullName
        Return Namexx
    End Function

    'Windows authentication Platform.
    Function GetPlatform() As String
        Dim Namexx As String = My.Computer.Info.OSPlatform
        Return Namexx
    End Function

    'Windows authentication Version.
    Function GetVersion() As String
        Dim Namexx As String = My.Computer.Info.OSVersion
        Return Namexx
    End Function

    'Windows authentication Language.
    Function GetLanguage() As String
        Dim Namexx As String = My.Computer.Info.InstalledUICulture.ToString
        Return Namexx
    End Function
#End Region

#Region "Create directory for local user"
    'Create catalog DeviceID3D
    Private Sub DeviceID3D_Directory(ByVal directory As String)

        Console.WriteLine("Create Project directory path:" & directory)

        Dim di As DirectoryInfo = New DirectoryInfo(directory)
        Try
            ' Determine whether the directory exists.
            If di.Exists Then
                ' Indicate that it already exists.
                Console.WriteLine("That path 'Create Project Directory' exists already.")
                Return
            End If

            ' Try to create the directory.
            di.Create()
            Console.WriteLine("The directory 'Create Project Directory' was created successfully.")

        Catch e As Exception
            Console.WriteLine("The process failed: {0}", e.ToString())
            MessageBox.Show("The process failed: {0}", e.ToString())
        End Try
    End Sub

    'Create catalog Project
    Private Sub Project_Directory(ByVal directory As String)

        Console.WriteLine("Create Project directory path:" & directory)

        Dim di As DirectoryInfo = New DirectoryInfo(directory)
        Try
            ' Determine whether the directory exists.
            If di.Exists Then
                ' Indicate that it already exists.
                Console.WriteLine("That path 'Create Project Directory' exists already.")
                Return
            End If

            ' Try to create the directory.
            di.Create()
            Console.WriteLine("The directory 'Create Project Directory' was created successfully.")

            ' Delete the directory.
            'di.Delete()
            'Console.WriteLine("The directory was deleted successfully.")

        Catch e As Exception
            Console.WriteLine("The process failed: {0}", e.ToString())
            MessageBox.Show("The process failed: {0}", e.ToString())
        End Try
    End Sub
#End Region

End Module
