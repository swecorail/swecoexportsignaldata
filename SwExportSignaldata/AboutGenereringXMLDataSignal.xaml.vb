﻿Public Class AboutGenereringXMLDataSignal

    Private Sub About_Loaded(sender As Object, e As RoutedEventArgs) Handles About.Loaded

        ' Set the title of the form.
        Dim ApplicationTitle As String
        If My.Application.Info.Title <> "" Then
            ApplicationTitle = My.Application.Info.Title
        Else
            ApplicationTitle = System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.AssemblyName)
        End If
        Me.Title = String.Format("About {0}", ApplicationTitle)

        ' Initialize all of the text displayed on Application
        LabelSwecoWork.Text = "Trafikstyrning"
        LabelProductName.Text = My.Application.Info.ProductName
        LabelVersion.Text = String.Format("Version {0}", My.Application.Info.Version.ToString)
        LabelCopyright.Text = My.Application.Info.Copyright
        LabelCompanyName.Text = My.Application.Info.CompanyName
    End Sub
End Class
