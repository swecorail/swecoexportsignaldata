﻿Imports System.Data.OleDb
Imports System.IO
Imports Microsoft.Win32

Public Class OpenProjectVtool

#Region "Private Data "
    Private SourceDataDatabasVTool As New OpenFileDialog 'dialogbox välj Databas VTool projekt
    Private SourceDataUserDrawingVTool As New OpenFileDialog 'dialogbox välj Grafisk instruktionsritning VTool projekt
    Private MissingXMLFileTrue As Boolean = False 'Avbryt direkt XML generering av leverans
#End Region

#Region "Drag över DatabasVTool"
    Private Sub tbDatabasVTool_PreviewDragEnter(sender As Object, e As DragEventArgs)
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effects = DragDropEffects.Copy
        Else
            e.Effects = DragDropEffects.None
        End If
    End Sub

    Private Sub tbDatabasVTool_PreviewDrop(sender As Object, e As DragEventArgs)
        Dim files() As String = e.Data.GetData(DataFormats.FileDrop)
        For Each path In files
            tbDatabasVTool.Text = path
            SourceDataDatabasVTool.FileName = path
        Next
    End Sub

    Private Sub tbDatabasVTool_PreviewDragOver(sender As Object, e As DragEventArgs)
        e.Handled = True
    End Sub
#End Region

#Region "Drag över UserDrawingVTool"
    Private Sub tbtUserDrawingVTool_PreviewDrop(sender As Object, e As DragEventArgs)
        Dim files() As String = e.Data.GetData(DataFormats.FileDrop)
        For Each path In files
            tbtUserDrawingVTool.Text = path
            SourceDataUserDrawingVTool.FileName = path
        Next
    End Sub

    Private Sub tbtUserDrawingVTool_PreviewDragOver(sender As Object, e As DragEventArgs)
        e.Handled = True
    End Sub

    Private Sub tbtUserDrawingVTool_PreviewDragEnter(sender As Object, e As DragEventArgs)
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effects = DragDropEffects.Copy
        Else
            e.Effects = DragDropEffects.None
        End If
    End Sub
#End Region

#Region "Välj Grafisk instruktionsritning"
    Private Sub btUserDrawingVTool_Click(sender As Object, e As RoutedEventArgs)
        Try
            SourceDataUserDrawingVTool.Title = "Välj Grafisk instruktionsritning"

            ' Open dialog box and retrieve dialog result when ShowDialog returns
            Dim result As Boolean = SourceDataUserDrawingVTool.ShowDialog()

            If result = True Then
                tbtUserDrawingVTool.Text = SourceDataUserDrawingVTool.FileName
            End If

        Catch ex As Exception
            DisplayError(ex)
        End Try
    End Sub
#End Region

#Region "Välj Databas"
    Private Sub btDataDatabasVTool_Click(sender As Object, e As RoutedEventArgs)

        Try
            SourceDataDatabasVTool.Title = "Välj Databas"

            ' Open dialog box and retrieve dialog result when ShowDialog returns
            Dim result As Boolean = SourceDataDatabasVTool.ShowDialog()

            If result = True Then
                tbDatabasVTool.Text = SourceDataDatabasVTool.FileName
            End If


        Catch ex As Exception
            DisplayError(ex)
        End Try
    End Sub
#End Region

    Private Sub btGenerate_Click(sender As Object, e As RoutedEventArgs)

        'Skapa sökväg till valt projekt
        If SourceDataVTool() = True Then

            'projektmapp temp leverans
            folder = FolderReceiverProject & "\" & importUserDrawingVTool & " " & Date.Today

            'förslag på tillfällig mapp
            Select Case MsgBox("Import VTool projektering: " & vbCrLf & folder & vbCrLf &
                                  " kommer att automatisk skapas på din disk." & vbCrLf & vbCrLf &
                                  "Vill du spara projektet förbestämd mapp ?", MsgBoxStyle.YesNo)

                Case MsgBoxResult.Yes


                Case MsgBoxResult.No


                    Try
                        ' Configure save file dialog box 
                        Dim exportsignaldata As New SaveFileDialog()
                        exportsignaldata.Title = "Skapa Projektmapp för exportfil till BIS"
                        exportsignaldata.CheckPathExists = True

                        ' Open dialog box and retrieve dialog result when ShowDialog returns
                        Dim result As Boolean = exportsignaldata.ShowDialog()

                        If result = True Then
                            'projektmapp temp leverans



                        End If

                    Catch ex As Exception

                    End Try




            End Select




            'skapa temp mapp för snabbare beräkningar
            CreateNewLocalMapp(folder)

            ''valt databas och grafisk fil
            'importDatabasVTool = fileinfoDatabas.Name
            'importUserDrawingVTool = fileinfoUserDrawing.Name

            'kopier alla filer till temp mapp
            CopyAllVToolFiles(importFolderVTool, folder, importDatabasVTool, importUserDrawingVTool, MissingXMLFileTrue)

            'någon fil saknas vid VTool generering
            If MissingXMLFileTrue = True Then
                Exit Sub
            End If

            'rensa lokal databas VTool
            For Each Dp As VToolsInmatningDataSet.DriftplatserRow In VtoolsDataSet.Driftplatser.Rows
                Dp.Delete()
            Next

            'DataAdaper
            VtoolsDataAdaper.UpdateAll(VtoolsDataSet)

            'Läs in csv-filer i temporär databas 
            Console.WriteLine("Läs in csv-filer i temporär databas :" & folder & "\")

            Inmatnign(folder & "\")
            'Inmatnign("C:\temp\ANDA\CSV Filer\Skavstaby\")

            ''Dim FilterString As String = "plnr='950'"
            ''Dim ResultRows() As VToolsInmatningDataSet.DriftplatserRow = VtoolsDataSet.Driftplatser.Select(FilterString)

            'skriv ut XML-fil för varje driftplats ID
            For Each Driftplats As VToolsInmatningDataSet.DriftplatserRow In VtoolsDataSet.Driftplatser
                Dim filePath As String = folder & "\" & Driftplats.plnamn & ".xml"

                Console.WriteLine("skriv ut XML-fil för varje driftplats ID :" & filePath & "Källa :" & folder & "\")
                SkrivutVtoolsData(filePath, Driftplats, folder & "\")
                Driftplats.GetRörelsevägarRows()
            Next


            ''skriv ut XML-fil för varje driftplats ID
            'For Each Driftplats As VToolsInmatningDataSet.DriftplatserRow In VtoolsDataSet.Driftplatser
            '    Dim filePath As String = "C:\Users\SEDHAL\Documents\ANDA-leverans\Test_" & Driftplats.plnamn & ".xml"
            '    SkrivutVtoolsData(filePath, Driftplats, "C:\temp\ANDA\CSV Filer\Skavstaby\")
            '    Driftplats.GetRörelsevägarRows()
            'Next


        End If

        MessageBox.Show("Klart. ")

    End Sub


    Private Sub CopyAllVToolFiles(ByVal sourcedirectory As String, ByVal destinationdirectory As String,
                                  ByVal databaseVTools As String, ByVal dmiVTools As String, ByRef filemissing As Boolean)

        'Copy FrontProtection
        If Not ProofOfDelivery_ToCopyAlways(sourcedirectory & "\" & importFrontProtection, destinationdirectory & "\" & importFrontProtection) = True Then
            filemissing = True
            Exit Sub
        End If

        'Copy RoadParts
        If Not ProofOfDelivery_ToCopyAlways(sourcedirectory & "\" & importRoadParts, destinationdirectory & "\" & importRoadParts) = True Then
            filemissing = True
            Exit Sub
        End If

        'Copy RoadsOfRoadParts
        If Not ProofOfDelivery_ToCopyAlways(sourcedirectory & "\" & importRoadsOfRoadParts, destinationdirectory & "\" & importRoadsOfRoadParts) = True Then
            filemissing = True
            Exit Sub
        End If

        'Copy ConflictingRoutes_tv
        If Not ProofOfDelivery_ToCopyAlways(sourcedirectory & "\" & importConflictingRoutes_tv, destinationdirectory & "\" & importConflictingRoutes_tv) = True Then
            filemissing = True
            Exit Sub
        End If

        'Copy ConflictingRoutes_vv
        If Not ProofOfDelivery_ToCopySelected(sourcedirectory & "\" & importConflictingRoutes_vv, destinationdirectory & "\" & importConflictingRoutes_vv) = True Then
            filemissing = True
            Exit Sub
        End If

        'Copy TC_T1_Mapping
        If Not ProofOfDelivery_ToCopyAlways(sourcedirectory & "\" & importTC_T1_Mapping, destinationdirectory & "\" & importTC_T1_Mapping) = True Then
            filemissing = True
            Exit Sub
        End If

        'Copy BIS_pl_config
        If Not ProofOfDelivery_ToCopyAlways(sourcedirectory & "\" & importBIS_pl_config, destinationdirectory & "\" & importBIS_pl_config) = True Then
            filemissing = True
            Exit Sub
        End If

        'Copy Database Vtools
        If Not ProofOfDelivery_ToCopyAlways(sourcedirectory & "\" & databaseVTools, destinationdirectory & "\" & databaseVTools) = True Then
            filemissing = True
            Exit Sub
        End If

        'Copy DMI Vtools
        If Not ProofOfDelivery_ToCopyAlways(sourcedirectory & "\" & dmiVTools, destinationdirectory & "\" & dmiVTools) = True Then
            filemissing = True
            Exit Sub
        End If
    End Sub




#Region "Vald källa projekt VTool"
    Private Function SourceDataVTool() As Boolean

        Dim ProjectDirectoryDatabas As String = ""
        Dim ProjectDirectoryUserDrawing As String = ""

        'databas
        Dim fileinfoDatabas As New FileInfo(tbDatabasVTool.Text)
        ProjectDirectoryDatabas = fileinfoDatabas.DirectoryName
        importDatabasVTool = tbDatabasVTool.Text

        'Grafisk instruktionsritning
        Dim fileinfoUserDrawing As New FileInfo(tbtUserDrawingVTool.Text)
        ProjectDirectoryUserDrawing = fileinfoUserDrawing.DirectoryName
        importUserDrawingVTool = tbtUserDrawingVTool.Text

        'samma mapp för båda filer
        If fileinfoDatabas.DirectoryName = fileinfoUserDrawing.DirectoryName Then

            'valt projekt och dess mapp
            importFolderVTool = fileinfoDatabas.DirectoryName.ToString

            'valt databas och grafisk fil
            importDatabasVTool = fileinfoDatabas.Name
            importUserDrawingVTool = fileinfoUserDrawing.Name

            Console.WriteLine("Valt projekt och dess databas. " & importDatabasVTool)
            Console.WriteLine("Valt projekt och dess grafiska fil. " & importUserDrawingVTool)

            Return True

        Else
            MessageBox.Show("Grafisk intruktionsritning och databas ligger inte under samma mapp. " &
                            " Grafisk Grafisk intruktionsritning = " & fileinfoUserDrawing.DirectoryName &
                            " och databas = " & fileinfoDatabas.DirectoryName)
            Return False
        End If
    End Function
#End Region

    Private Sub Exportkatalog_VTool()

        'bestäm huvudkatalog projekt
        Dim fileinfo As New FileInfo(SourceDataDatabasVTool.SafeFileName)
        ProjectDirectory = fileinfo.DirectoryName
        OnlyFileNameProject = SourceDataDatabasVTool.SafeFileName

    End Sub

#Region "Skapa internt mapp för lagring av VTool projekt"
    Private Sub CreateNewLocalMapp(ByVal directory As String)

        Console.WriteLine("Create Project directory path:" & directory)

        Dim di As DirectoryInfo = New DirectoryInfo(directory)
        Try
            ' Determine whether the directory exists.
            If di.Exists Then
                ' Indicate that it already exists.
                Console.WriteLine("That path 'Create Project Directory' exists already.")
                Return
            End If

            ' Try to create the directory.
            di.Create()
            Console.WriteLine("The directory 'Create Project Directory' was created successfully.")

            ' Delete the directory.
            'di.Delete()




        Catch ex As Exception
            DisplayError(ex)
            MissingXMLFileTrue = True
        End Try
    End Sub
#End Region


    Private Sub btClose_Click(sender As Object, e As RoutedEventArgs)
        Me.Close()
    End Sub

    Private Sub OpenImportProjektVTool_Loaded(sender As Object, e As RoutedEventArgs)

        'VtoolsDataSet = New VToolsInmatningDataSet

        'VtoolsDataAdaper = New VToolsInmatningDataSetTableAdapters.TableAdapterManager

        'VtoolsDataAdaper.DriftplatserTableAdapter = New VToolsInmatningDataSetTableAdapters.DriftplatserTableAdapter

        'VtoolsDataAdaper.RörelsevägarTableAdapter = New VToolsInmatningDataSetTableAdapters.RörelsevägarTableAdapter
        'VtoolsDataAdaper.VägdelariRörelsevägarTableAdapter = New VToolsInmatningDataSetTableAdapters.VägdelariRörelsevägarTableAdapter
        'VtoolsDataAdaper.KonflikterTableAdapter = New VToolsInmatningDataSetTableAdapters.KonflikterTableAdapter
        'VtoolsDataAdaper.SlutpunkterTableAdapter = New VToolsInmatningDataSetTableAdapters.SlutpunkterTableAdapter

        'VtoolsDataAdaper.VägdelarTableAdapter = New VToolsInmatningDataSetTableAdapters.VägdelarTableAdapter
        'VtoolsDataAdaper.LåsningarTableAdapter = New VToolsInmatningDataSetTableAdapters.LåsningarTableAdapter
        'VtoolsDataAdaper.KörningarTableAdapter = New VToolsInmatningDataSetTableAdapters.KörningarTableAdapter
        'VtoolsDataAdaper.ViaPunkterTableAdapter = New VToolsInmatningDataSetTableAdapters.ViaPunkterTableAdapter
        'VtoolsDataAdaper.MotriktadeObjektTableAdapter = New VToolsInmatningDataSetTableAdapters.MotriktadeObjektTableAdapter
        'VtoolsDataAdaper.SpårledningarTableAdapter = New VToolsInmatningDataSetTableAdapters.SpårledningarTableAdapter
        'VtoolsDataAdaper.ObjektiVägTableAdapter = New VToolsInmatningDataSetTableAdapters.ObjektiVägTableAdapter

        'VtoolsbacktobackDataAdapter = New VToolsInmatningDataSetTableAdapters.BacktoBackTableAdapter


        'VtoolsDataAdaper.BisObjektTableAdapter = New VToolsInmatningDataSetTableAdapters.BisObjektTableAdapter
        'VtoolsDataAdaper.FrontSkyddTableAdapter = New VToolsInmatningDataSetTableAdapters.FrontSkyddTableAdapter
        'VtoolsDataAdaper.KörningFrontTableAdapter = New VToolsInmatningDataSetTableAdapters.KörningFrontTableAdapter
        'VtoolsDataAdaper.LåsningarFrontTableAdapter = New VToolsInmatningDataSetTableAdapters.LåsningarFrontTableAdapter
        'VtoolsDataAdaper.FrontObjektTableAdapter = New VToolsInmatningDataSetTableAdapters.FrontObjektTableAdapter
        'VtoolsDataAdaper.FrontSkyddOmTableAdapter = New VToolsInmatningDataSetTableAdapters.FrontSkyddOmTableAdapter
        'VtoolsDataAdaper.KonfliktOmEjTableAdapter = New VToolsInmatningDataSetTableAdapters.KonfliktOmEjTableAdapter
        'VtoolsDataAdaper.SplobjektTableAdapter = New VToolsInmatningDataSetTableAdapters.SplobjektTableAdapter

        'VtoolsDataAdaper.DriftplatserTableAdapter.Fill(VtoolsDataSet.Driftplatser)
        'VtoolsDataAdaper.BisObjektTableAdapter.Fill(VtoolsDataSet.BisObjekt)
        'VtoolsDataAdaper.RörelsevägarTableAdapter.Fill(VtoolsDataSet.Rörelsevägar)
        'VtoolsDataAdaper.SlutpunkterTableAdapter.Fill(VtoolsDataSet.Slutpunkter)
        'VtoolsDataAdaper.VägdelarTableAdapter.Fill(VtoolsDataSet.Vägdelar)
        'VtoolsDataAdaper.VägdelariRörelsevägarTableAdapter.Fill(VtoolsDataSet.VägdelariRörelsevägar)
        'VtoolsDataAdaper.LåsningarTableAdapter.Fill(VtoolsDataSet.Låsningar)
        'VtoolsDataAdaper.KörningarTableAdapter.Fill(VtoolsDataSet.Körningar)
        'VtoolsDataAdaper.ViaPunkterTableAdapter.Fill(VtoolsDataSet.ViaPunkter)
        'VtoolsDataAdaper.MotriktadeObjektTableAdapter.Fill(VtoolsDataSet.MotriktadeObjekt)
        'VtoolsDataAdaper.ObjektiVägTableAdapter.Fill(VtoolsDataSet.ObjektiVäg)
        'VtoolsDataAdaper.SpårledningarTableAdapter.Fill(VtoolsDataSet.Spårledningar)
        'VtoolsDataAdaper.FrontSkyddTableAdapter.Fill(VtoolsDataSet.FrontSkydd)
        'VtoolsDataAdaper.FrontObjektTableAdapter.Fill(VtoolsDataSet.FrontObjekt)
        'VtoolsDataAdaper.KörningFrontTableAdapter.Fill(VtoolsDataSet.KörningFront)
        'VtoolsDataAdaper.LåsningarFrontTableAdapter.Fill(VtoolsDataSet.LåsningarFront)
        'VtoolsDataAdaper.KonflikterTableAdapter.Fill(VtoolsDataSet.Konflikter)
        'VtoolsDataAdaper.KonfliktOmEjTableAdapter.Fill(VtoolsDataSet.KonfliktOmEj)
        'VtoolsDataAdaper.FrontSkyddOmTableAdapter.Fill(VtoolsDataSet.FrontSkyddOm)
        'VtoolsDataAdaper.SplobjektTableAdapter.Fill(VtoolsDataSet.Splobjekt)

        'VtoolsbacktobackDataAdapter.Fill(VtoolsDataSet.BacktoBack)

        ''AddHandler
        'AddHandler VtoolsDataAdaper.VägdelarTableAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleVägDelarRowUpdated)

        'AddHandler VtoolsDataAdaper.RörelsevägarTableAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleRörelserRowUpdated)
        'AddHandler VtoolsDataAdaper.BisObjektTableAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleBisIDRowUpdated)

        'AddHandler VtoolsDataAdaper.KonflikterTableAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleKonfliktIDRowUpdated)

        'AddHandler VtoolsDataAdaper.FrontSkyddTableAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleFrontskyddIDRowUpdated)
        'AddHandler VtoolsDataAdaper.SlutpunkterTableAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleSlutpunkterRowUpdated)
        'AddHandler VtoolsbacktobackDataAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleBacktobackRowUpdated)
    End Sub

#Region "Error Handler"
    Private Sub DisplayError(ByVal ex As Exception)
        Dim errText As String = "Error - " & vbCrLf
        Do While (Not ex Is Nothing)
            errText = errText & ex.Message & vbCrLf
            ex = ex.InnerException
        Loop
        MessageBox.Show(errText, "An Error occurred.", MessageBoxButton.OK, MessageBoxImage.Error)
    End Sub
#End Region

End Class
