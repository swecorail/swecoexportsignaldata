﻿Imports System.IO
Imports System.Text
Imports RorelsevagarOchSkyddLib
Imports System.Data


Module ImportCVSFile


#Region "Read VTools CSV-files and export to XML-file"
    'create new RoadsOfRoadParts xml-file
    Public Sub ImportRoadsOfRoadParts_ExportXML(ByVal MappSökväg As String)
        Dim allLinesroadsofroadsparts As List(Of String) = New List(Of String)
        Dim allLinesroadparts As List(Of String) = New List(Of String)

        Dim AllaFrontskydd As List(Of String) = New List(Of String)
        Dim ProjektLista As List(Of String) = New List(Of String)
        Dim VägKonflikter As List(Of String) = New List(Of String)
        Dim ObjPerSpår As List(Of String) = New List(Of String)

        Dim counterline As Integer = 2
        Dim tempTyp As String = ""

        'UTF-8 Encoding.UTF32

        Try
            ' Börja med projekt information 
            FixInStream(ProjektLista, MappSökväg & importBIS_pl_config)
            ' Fixa Väg konflikter
            FixInStream(VägKonflikter, MappSökväg & importConflictingRoutes_tv)
            'read textfile RoadsOfRoadParts and add to line
            FixInStream(allLinesroadsofroadsparts, MappSökväg & importRoadsOfRoadParts)

            FixInStream(allLinesroadparts, MappSökväg & importRoadParts)

            FixInStream(AllaFrontskydd, MappSökväg & importFrontProtection)

            FixInStream(ObjPerSpår, MappSökväg & importTC_T1_Mapping)

            FyllDriftTabell(VtoolsDataSet.Driftplatser, ProjektLista)

            FyllVägDelar(VtoolsDataSet, allLinesroadparts, MappSökväg)

            FyllRörelseTabell(allLinesroadsofroadsparts, MappSökväg)

            FyllFrontSkydd(AllaFrontskydd, MappSökväg)

            FyllKonflikterTabell(VägKonflikter)

            FyllObjektPerSpl(ObjPerSpår, MappSökväg)

            If System.IO.File.Exists(MappSökväg & importConflictingRoutes_vv) = True Then
                VägKonflikter = New List(Of String)
                FixInStream(VägKonflikter, MappSökväg & importConflictingRoutes_vv)
                FyllKonflikterTabell(VägKonflikter)
            End If

            If System.IO.File.Exists(MappSökväg & importBacktoBack) = True Then
                Dim BacktoBack As List(Of String) = New List(Of String)
                FixInStream(BacktoBack, MappSökväg & importBacktoBack)
                FyllBacktoback(BacktoBack, MappSökväg)
            End If

        Catch ex As Exception
            DisplayError(ex)
        End Try
    End Sub

    Private Sub FixInStream(ByRef StringList As List(Of String), ByVal FilePath As String)
        Try

            Dim InStream As StreamReader = New StreamReader(FilePath, Encoding.Default)

            Do While Not InStream.EndOfStream
                'tempTyp = reader.ReadLine()
                StringList.Add(InStream.ReadLine())
            Loop
            InStream.Close()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub FyllDriftTabell(ByRef DriftPlatser As VToolsInmatningDataSet.DriftplatserDataTable, ByRef InList As List(Of String))

        Dim TmpString As String
        Dim i As Integer = 0
        Dim ResultRows() As DataRow
        Dim FilterString As String
        Try
            For Each Row As String In InList
                If i > 0 Then
                    Dim NyRad As VToolsInmatningDataSet.DriftplatserRow = DriftPlatser.NewDriftplatserRow()
                    TmpString = FindInString(Row, ";")
                    NyRad.plnr = CInt(TmpString)
                    NyRad.plsign = FindInString(Row, ";")
                    NyRad.plnamn = FindInString(Row, ";")
                    NyRad.pltyp = FindInString(Row, ";")
                    FindInString(Row, ";")
                    NyRad.stlvtyp = FindInString(Row, ";")

                    FilterString = "plnr='" & NyRad.plnr & "'"
                    ResultRows = VtoolsDataSet.Driftplatser.Select(FilterString)
                    If ResultRows.Count < 1 Then
                        DriftPlatser.Rows.Add(NyRad)
                    Else
                        If StrComp(ResultRows(0).Item("stlvtyp"), "-") = 0 Then
                            ResultRows(0).Item("stlvtyp") = NyRad.stlvtyp
                        End If
                    End If
                End If
                i += 1
            Next
            VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
        Catch ex As Exception
            MessageBox.Show("BIS_pl_config.csv filen: " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
            Throw New ApplicationException()
        End Try


    End Sub

    Private Sub FyllVägDelar(ByRef InDataSet As VToolsInmatningDataSet, ByRef InLista As List(Of String), ByVal FolderPath As String)
        Dim i As Integer = 0
        Try

            'Dim BörjanPunkt As List(Of String) = New List(Of String)
            'Dim SlutPunkt As List(Of String) = New List(Of String)
            Dim ViaPunktNamn As String
            Dim ViaPunktID As String
            Dim Spårledningar As String
            Dim SpårledningarID As String
            Dim Låsning As String
            Dim LåsningsID As String
            Dim Körning As String
            Dim KörningID As String
            Dim MotRiktad As String
            Dim MotRiktadID As String
            Dim ResultRows() As DataRow
            Dim FilterString As String
            Dim VägID As Integer
            Dim ObjIväg As String
            Dim ObjIvägID As String

            For Each Row As String In InLista
                If i > 0 Then
                    Dim BörjanPunkt As List(Of String) = New List(Of String)
                    Dim SlutPunkt As List(Of String) = New List(Of String)
                    Dim AllPointsExist As Boolean = True
                    Dim NyRad As VToolsInmatningDataSet.VägdelarRow = InDataSet.Vägdelar.NewVägdelarRow
                    NyRad.Beteckning = FindInString(Row, ";")
                    NyRad.Typ = FindInString(Row, ";")
                    FindInString(Row, ";")
                    'börjanpunkt i riktigt namn, behövs igentligen inte men nu tar jag hand om den 
                    BörjanPunkt.Add(FindInString(Row, ";"))
                    ViaPunktNamn = FindInString(Row, ";")
                    'SlutPunkt i riktigt namn, behövs igentligen inte men nu tar jag hand om den 
                    SlutPunkt.Add(FindInString(Row, ";"))
                    Spårledningar = FindInString(Row, ";")
                    ' Objekt if vägdelen 
                    ObjIväg = FindInString(Row, ";")
                    Låsning = FindInString(Row, ";")
                    Körning = FindInString(Row, ";")
                    ' Motriktade signaler 
                    MotRiktad = FindInString(Row, ";")

                    For j As Integer = 1 To 3
                        BörjanPunkt.Add(FindInString(Row, ","))
                    Next
                    FilterString = "plnr='" & BörjanPunkt(1) & "' AND typ='" & BörjanPunkt(2) & "' AND BisID='" & BörjanPunkt(3) & "'"
                    ResultRows = VtoolsDataSet.BisObjekt.Select(FilterString)
                    If ResultRows.Count > 0 Then
                        NyRad.BörjanPunktID = ResultRows(0).Item("ID")
                    Else
                        If StrComp(BörjanPunkt(1), "0") <> 0 Then
                            Dim NyObjekt As VToolsInmatningDataSet.BisObjektRow = VtoolsDataSet.BisObjekt.NewBisObjektRow
                            NyObjekt.plnr = BörjanPunkt(1)
                            NyObjekt.typ = BörjanPunkt(2)
                            NyObjekt.BisID = BörjanPunkt(3)
                            VtoolsDataSet.BisObjekt.Rows.Add(NyObjekt)
                            VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
                            NyRad.BörjanPunktID = NyObjekt.ID
                        Else
                            AllPointsExist = False
                            Dim TmpString As String = "Vägdel " & NyRad.Beteckning & " av typ " & NyRad.Typ & " Förkastas pga saknat BisID för börjanpunkt"
                            MakeLogUpdate(FolderPath & "\Bortrensade rörelser vid körning " & System.DateTime.Today & ".txt", TmpString)
                        End If

                    End If

                    FindInString(Row, ";")
                    ViaPunktID = FindInString(Row, ";")

                    For j As Integer = 1 To 3
                        SlutPunkt.Add(FindInString(Row, ","))
                    Next

                    FilterString = "plnr='" & SlutPunkt(1) & "' AND typ='" & SlutPunkt(2) & "' AND BisID='" & SlutPunkt(3) & "'"
                    ResultRows = VtoolsDataSet.BisObjekt.Select(FilterString)

                    If ResultRows.Count > 0 Then
                        NyRad.SlutPunktID = ResultRows(0).Item("ID")
                    Else
                        If StrComp(SlutPunkt(1), "0") <> 0 Then
                            Dim NyObjekt As VToolsInmatningDataSet.BisObjektRow = VtoolsDataSet.BisObjekt.NewBisObjektRow
                            NyObjekt.plnr = SlutPunkt(1)
                            NyObjekt.typ = SlutPunkt(2)
                            NyObjekt.BisID = SlutPunkt(3)
                            VtoolsDataSet.BisObjekt.Rows.Add(NyObjekt)
                            VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
                            NyRad.SlutPunktID = NyObjekt.ID
                        Else
                            AllPointsExist = False
                            Dim TmpString As String = "Vägdel " & NyRad.Beteckning & " av typ " & NyRad.Typ & " Förkastas pga saknat BisID för slutPunkt"
                            MakeLogUpdate(FolderPath & "\Bortrensade rörelser vid körning " & System.DateTime.Today & ".txt", TmpString)
                        End If
                    End If

                    FindInString(Row, ";")
                    SpårledningarID = FindInString(Row, ";")

                    ' Objekt if vägdelen 
                    ObjIvägID = FindInString(Row, ";")

                    LåsningsID = FindInString(Row, ";")
                    KörningID = FindInString(Row, ";")
                    MotRiktadID = FindInString(Row, ";")

                    If AllPointsExist = True Then
                        FilterString = "Beteckning='" & NyRad.Beteckning & "' AND Typ='" & NyRad.Typ & "'"

                        ResultRows = InDataSet.Vägdelar.Select(FilterString)
                        If ResultRows.Count < 1 Then
                            InDataSet.Vägdelar.Rows.Add(NyRad)
                            VtoolsDataAdaper.UpdateAll(InDataSet)
                            VägID = NyRad.ID
                        Else
                            VägID = ResultRows(0).Item("ID")
                        End If

                        FyllUnderTabeller(InDataSet, ViaPunktNamn, ViaPunktID, VägID, "Viapunkter", FolderPath)
                        FyllUnderTabeller(InDataSet, Låsning, LåsningsID, VägID, "Låsningar", FolderPath)
                        FyllUnderTabeller(InDataSet, Körning, KörningID, VägID, "Körningar", FolderPath)
                        FyllUnderTabeller(InDataSet, Spårledningar, SpårledningarID, VägID, "Spårledningar", FolderPath)
                        FyllUnderTabeller(InDataSet, MotRiktad, MotRiktadID, VägID, "MotriktadeSignaler", FolderPath)
                        FyllUnderTabeller(InDataSet, ObjIväg, ObjIvägID, VägID, "ObjektIväg", FolderPath)
                    End If
                End If
                i += 1
            Next
        Catch ex As Exception
            MessageBox.Show("RoadParts.csv filen rad: " & i & ": " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
            Throw New ApplicationException()
        End Try

    End Sub

    Private Sub FyllUnderTabeller(ByRef InmatningsDataSet As VToolsInmatningDataSet, ByVal InString As String, InStringID As String,
                                  ByVal VägDelsID As Integer, TabellTyp As String, FolderPath As String)

        Dim plnr As String
        Dim Typ As String
        Dim BisID As String
        Dim Riktning As String
        Dim FilterString As String
        Dim BoolVar As Boolean = True

        Dim ResultRows() As DataRow
        Dim TTmp As String

        Try
            Do Until InStringID = ""
                Dim NyRad As DataRow
                TTmp = InString
                Select Case TabellTyp
                    Case "Körningar", "KörningarFront"
                        Dim TmpString As String = FindInString(TTmp, ",")
                        If StrComp(TmpString, "riktning ut låst") <> 0 Then
                            plnr = FindInString(InStringID, ",")
                            Typ = FindInString(InStringID, ",")
                            BisID = FindInString(InStringID, ",")
                            Riktning = FindInString(InStringID, "_")
                            BoolVar = True
                        Else
                            plnr = ""
                            Typ = ""
                            BisID = ""
                            Riktning = TmpString
                            BoolVar = False
                        End If
                    Case Else
                        plnr = FindInString(InStringID, ",")
                        Typ = FindInString(InStringID, ",")
                        BisID = FindInString(InStringID, ",")
                        Riktning = FindInString(InStringID, "_")
                End Select

                If StrComp(plnr, "0") <> 0 Then
                    Select Case TabellTyp
                        Case "Viapunkter"
                            NyRad = InmatningsDataSet.ViaPunkter.NewRow
                        Case "Spårledningar"
                            NyRad = InmatningsDataSet.Spårledningar.NewRow
                        Case "MotriktadeSignaler"
                            NyRad = InmatningsDataSet.MotriktadeObjekt.NewRow
                        Case "Låsningar"
                            NyRad = InmatningsDataSet.Låsningar.NewRow
                        Case "Körningar"
                            NyRad = InmatningsDataSet.Körningar.NewRow
                        Case "ObjektIväg"
                            NyRad = InmatningsDataSet.ObjektiVäg.NewRow
                        Case "FrontSkyddsObjekt"
                            NyRad = InmatningsDataSet.FrontObjekt.NewRow
                        Case "OmFront"
                            NyRad = InmatningsDataSet.FrontSkyddOm.NewRow
                        Case "LåsningarFront"
                            NyRad = InmatningsDataSet.LåsningarFront.NewRow
                        Case "KörningarFront"
                            NyRad = InmatningsDataSet.KörningFront.NewRow
                        Case Else
                            ' kommer aldrig hända
                            NyRad = InmatningsDataSet.ViaPunkter.NewRow
                    End Select

                    Select Case TabellTyp
                        Case "Viapunkter", "Spårledningar", "MotriktadeSignaler", "Låsningar", "Körningar", "ObjektIväg"
                            NyRad.Item("VägDelsID") = VägDelsID
                        Case "KonfliktOmEj"
                            NyRad.Item("KonfliktID") = VägDelsID
                        Case Else
                            NyRad.Item("FrontID") = VägDelsID
                    End Select

                    NyRad.Item("Riktning") = Riktning

                    If BoolVar = True Then
                        FilterString = "plnr='" & plnr & "' AND typ='" & Typ & "' AND BisID='" & BisID & "'"
                        ResultRows = InmatningsDataSet.BisObjekt.Select(FilterString)

                        If ResultRows.Count > 0 Then
                            NyRad.Item("BisID") = ResultRows(0).Item("ID")
                        Else
                            Dim NyObjekt As VToolsInmatningDataSet.BisObjektRow = InmatningsDataSet.BisObjekt.NewBisObjektRow
                            NyObjekt.plnr = plnr
                            NyObjekt.typ = Typ
                            NyObjekt.BisID = BisID
                            InmatningsDataSet.BisObjekt.Rows.Add(NyObjekt)
                            VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
                            NyRad.Item("BisID") = NyObjekt.ID
                        End If
                    End If


                    Select Case TabellTyp
                        Case "Viapunkter"
                            InmatningsDataSet.ViaPunkter.Rows.Add(NyRad)
                        Case "Spårledningar"
                            InmatningsDataSet.Spårledningar.Rows.Add(NyRad)
                        Case "MotriktadeSignaler"
                            InmatningsDataSet.MotriktadeObjekt.Rows.Add(NyRad)
                        Case "Låsningar"
                            InmatningsDataSet.Låsningar.Rows.Add(NyRad)
                        Case "Körningar"
                            InmatningsDataSet.Körningar.Rows.Add(NyRad)
                        Case "ObjektIväg"
                            InmatningsDataSet.ObjektiVäg.Rows.Add(NyRad)
                        Case "FrontSkyddsObjekt"
                            InmatningsDataSet.FrontObjekt.Rows.Add(NyRad)
                        Case "OmFront"
                            InmatningsDataSet.FrontSkyddOm.Rows.Add(NyRad)
                        Case "LåsningarFront"
                            InmatningsDataSet.LåsningarFront.Rows.Add(NyRad)
                        Case "KörningarFront"
                            InmatningsDataSet.KörningFront.Rows.Add(NyRad)
                        Case Else
                            ' kommer aldrig hända
                            InmatningsDataSet.ViaPunkter.Rows.Add(NyRad)
                    End Select
                Else
                    ' Objektet finns inte i BIS skriv i listan om fel
                    Dim FörstaDel As String
                    FilterString = "ID='" & VägDelsID & "'"
                    Select Case TabellTyp
                        Case "Viapunkter", "Spårledningar", "MotriktadeSignaler", "Låsningar", "Körningar", "ObjektIväg"
                            ResultRows = VtoolsDataSet.Vägdelar.Select(FilterString)
                            FörstaDel = "Vägdel " & ResultRows(0).Item("Beteckning") & " saknar objekt i undergrupper, " & TabellTyp & ", kan behöva förkastas"

                        Case "KonfliktOmEj"
                            ResultRows = VtoolsDataSet.Konflikter.Select(FilterString)
                            FörstaDel = "Konflikten " & ResultRows(0).Item("ID") & " saknar objekt i omej konflikter, " & TabellTyp & ", kan behöva förkastas"
                        Case Else
                            ResultRows = VtoolsDataSet.FrontSkydd.Select(FilterString)
                            FörstaDel = "Frontskydd" & ResultRows(0).Item("ID") & " saknar objekt i undergrupper, " & TabellTyp & ", kan behöva förkastas"


                    End Select
                    MakeLogUpdate(FolderPath & "Bortrensade rörelser vid körning " & System.DateTime.Today & ".txt", FörstaDel)

                End If
                ' bytRad
                FindInString(InString, ",")
            Loop
            VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
        Catch ex As Exception
            Select Case TabellTyp
                Case "Viapunkter"
                    MessageBox.Show("RoadParts.csv filen, column Viapunkter: " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
                Case "Spårledningar"
                    MessageBox.Show("RoadParts.csv filen, column Vägdelsområde: " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
                Case "MotriktadeSignaler"
                    MessageBox.Show("RoadParts.csv filen, column Motriktade signaler: " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
                Case "Låsningar"
                    MessageBox.Show("RoadParts.csv filen, column Villkor för låsning: " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
                Case "Körningar"
                    MessageBox.Show("RoadParts.csv filen, column Villkor för körsignal: " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
                Case "ObjektIväg"
                    MessageBox.Show("RoadParts.csv filen, column Objekt i vägdelen: " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
                Case "FrontSkyddsObjekt"
                    MessageBox.Show("FrontProtection.csv filen, column Frontskyddsobjekt: " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
                Case "OmFront"
                    MessageBox.Show("FrontProtection.csv filen, column Om: " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
                Case "LåsningarFront"
                    MessageBox.Show("FrontProtection.csv filen, column Villkor för låsning: " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
                Case "KörningarFront"
                    MessageBox.Show("FrontProtection.csv filen, column Villkor för körsignal: " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
                Case Else
                    ' kommer aldrig hända
                    MessageBox.Show("RoadParts.csv filen, column Viapunkter: " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
            End Select
            Throw New ApplicationException()
        End Try
    End Sub

    Private Sub FyllRörelseTabell(InLista As List(Of String), ByVal FolderPath As String)
        Dim i As Integer = 0
        Try
            Dim FilterString As String
            Dim ResultRows() As DataRow
            Dim RörelseDelar As String
            For Each Row As String In InLista
                If i > 0 Then
                    Dim BörjanObjekt As List(Of String) = New List(Of String)
                    Dim SlutObjekt As List(Of String) = New List(Of String)
                    Dim NyRad As VToolsInmatningDataSet.RörelsevägarRow = VtoolsDataSet.Rörelsevägar.NewRörelsevägarRow
                    Dim AllPointsExist As Boolean = True
                    ' gammal beteckning används inte
                    FindInString(Row, ";")
                    NyRad.Typ = FindInString(Row, ";")
                    ' Börjanpunkt
                    FindInString(Row, ";")
                    'slutpunkt
                    FindInString(Row, ";")
                    'tågriktning
                    FindInString(Row, ";")
                    RörelseDelar = FindInString(Row, ";")
                    'BörjanObjekt
                    For j As Integer = 1 To 3
                        BörjanObjekt.Add(FindInString(Row, ","))
                    Next
                    FindInString(Row, ";")

                    For j As Integer = 1 To 3
                        SlutObjekt.Add(FindInString(Row, ","))
                    Next
                    FindInString(Row, ";")

                    NyRad.Beteckning = FindInString(Row, ";")

                    FilterString = "plnr='" & BörjanObjekt(0) & "' AND Typ='" & BörjanObjekt(1) & "' AND BisID='" & BörjanObjekt(2) & "'"
                    ResultRows = VtoolsDataSet.BisObjekt.Select(FilterString)
                    If ResultRows.Count > 0 Then
                        NyRad.BörjanPunktID = ResultRows(0).Item("ID")
                    Else
                        If StrComp(BörjanObjekt(0), "0") <> 0 Then
                            Dim NyBis As VToolsInmatningDataSet.BisObjektRow = VtoolsDataSet.BisObjekt.NewBisObjektRow
                            NyBis.plnr = BörjanObjekt(0)
                            NyBis.typ = BörjanObjekt(1)
                            NyBis.BisID = BörjanObjekt(2)
                            VtoolsDataSet.BisObjekt.Rows.Add(NyBis)
                            VtoolsDataAdaper.BisObjektTableAdapter.Update(VtoolsDataSet.BisObjekt)
                            NyRad.BörjanPunktID = NyBis.ID
                        Else
                            AllPointsExist = False
                            Dim TmpString As String = "Rörelse " & NyRad.Beteckning & " av typ " & NyRad.Typ & " Förkastas pga saknat BisID för börjanpunkt"
                            MakeLogUpdate(FolderPath & "\Bortrensade rörelser vid körning " & System.DateTime.Today & ".txt", TmpString)
                        End If

                    End If



                    FilterString = "plnr='" & SlutObjekt(0) & "' AND Typ='" & SlutObjekt(1) & "' AND BisID='" & SlutObjekt(2) & "'"
                    ResultRows = VtoolsDataSet.BisObjekt.Select(FilterString)
                    If ResultRows.Count > 0 Then

                        FilterString = "plnr='" & SlutObjekt(0) & "' AND BisID='" & ResultRows(0).Item("ID") & "' AND Typ='" & NyRad.Typ & "'"
                        Dim SlutResultat() As DataRow = VtoolsDataSet.Slutpunkter.Select(FilterString)
                        If SlutResultat.Count < 1 Then
                            Dim SlutRad As VToolsInmatningDataSet.SlutpunkterRow = VtoolsDataSet.Slutpunkter.NewSlutpunkterRow
                            SlutRad.plnr = SlutObjekt(0)
                            SlutRad.BisID = ResultRows(0).Item("ID")
                            SlutRad.Typ = NyRad.Typ
                            VtoolsDataSet.Slutpunkter.Rows.Add(SlutRad)
                            VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
                            NyRad.SlutPunktID = SlutRad.ID

                        Else
                            NyRad.SlutPunktID = SlutResultat(0).Item("ID")
                        End If

                    Else
                        If StrComp(SlutObjekt(0), "0") <> 0 Then
                            Dim NyBis As VToolsInmatningDataSet.BisObjektRow = VtoolsDataSet.BisObjekt.NewBisObjektRow
                            NyBis.plnr = SlutObjekt(0)
                            NyBis.typ = SlutObjekt(1)
                            NyBis.BisID = SlutObjekt(2)
                            VtoolsDataSet.BisObjekt.Rows.Add(NyBis)
                            VtoolsDataAdaper.BisObjektTableAdapter.Update(VtoolsDataSet.BisObjekt)

                            Dim SlutRad As VToolsInmatningDataSet.SlutpunkterRow = VtoolsDataSet.Slutpunkter.NewSlutpunkterRow
                            SlutRad.plnr = SlutObjekt(0)
                            SlutRad.BisID = NyBis.ID
                            SlutRad.Typ = NyRad.Typ
                            VtoolsDataSet.Slutpunkter.Rows.Add(SlutRad)
                            VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
                            NyRad.SlutPunktID = SlutRad.ID
                        Else
                            AllPointsExist = False
                            Dim TmpString As String = "Rörelse " & NyRad.Beteckning & " av typ " & NyRad.Typ & " Förkastas pga saknat BisID för slutpunkt"
                            MakeLogUpdate(FolderPath & "\Bortrensade rörelser vid körning " & System.DateTime.Today & ".txt", TmpString)
                        End If
                    End If

                    NyRad.plnr = SlutObjekt(0)
                    If AllPointsExist = True Then
                        FilterString = "Beteckning='" & NyRad.Beteckning & "' AND Typ='" & NyRad.Typ & "'"
                        ResultRows = VtoolsDataSet.Rörelsevägar.Select(FilterString)
                        Dim RörelseID As Integer
                        If ResultRows.Count < 1 Then
                            NyRad.TRV_ID = (SlutObjekt(0) * 10000) + i
                            VtoolsDataSet.Rörelsevägar.Rows.Add(NyRad)
                            VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
                            RörelseID = NyRad.ID
                        Else
                            RörelseID = ResultRows(0).Item("ID")
                        End If

                        FyllRörelsedelarTabell(RörelseDelar, RörelseID, NyRad.Typ, FolderPath)
                    End If
                End If
                i += 1
            Next
        Catch ex As Exception
            MessageBox.Show("RoadsOfRoadParts.csv filen rad: " & i & ": " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
            Throw New ApplicationException()
        End Try

    End Sub

    Private Sub FyllRörelsedelarTabell(Instring As String, RörelseID As String, RörelseTyp As String, FolderPath As String)
        Try
            Dim VägdelsBeteckning As String
            Dim FilterString As String
            Dim ResultRows() As DataRow
            Do Until Instring = ""
                VägdelsBeteckning = FindInString(Instring, ",")
                FilterString = "Beteckning='" & VägdelsBeteckning & "' AND Typ='" & RörelseTyp & "'"
                ResultRows = VtoolsDataSet.Vägdelar.Select(FilterString)
                If ResultRows.Count > 0 Then
                    Dim NyRad As VToolsInmatningDataSet.VägdelariRörelsevägarRow = VtoolsDataSet.VägdelariRörelsevägar.NewVägdelariRörelsevägarRow
                    NyRad.RörelseID = RörelseID
                    NyRad.VägdelsID = ResultRows(0).Item("ID")
                    VtoolsDataSet.VägdelariRörelsevägar.Rows.Add(NyRad)
                Else
                    FilterString = "ID='" & RörelseID & "'"
                    ResultRows = VtoolsDataSet.Rörelsevägar.Select(FilterString)

                    Dim TmpString As String = "Rörelse " & ResultRows(0).Item("Beteckning") & " av typ " & ResultRows(0).Item("Typ") & " Saknar vägdel " & VägdelsBeteckning & " och bör förkastas"
                    MakeLogUpdate(FolderPath & "\Bortrensade rörelser vid körning " & System.DateTime.Today & ".txt", TmpString)
                End If
            Loop
            VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
        Catch ex As Exception
            MessageBox.Show("RoadsOfRoadParts.csv, Kolumn  filen Vägdelar: " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
            Throw New ApplicationException()
        End Try
    End Sub

    Private Sub FyllKonflikterTabell(InList As List(Of String))
        Dim i As Integer = 0
        Dim FilterString As String
        Dim ResultRows() As DataRow

        Try
            For Each Row As String In InList
                If i > 0 Then
                    Dim SlutpunktsLista As List(Of String) = New List(Of String)
                    Dim FientligLista As List(Of String) = New List(Of String)
                    Dim OmEjLista As List(Of String) = New List(Of String)
                    Dim FientligTyp As String
                    Dim BisObjekt As Integer
                    Dim OmEjID As List(Of Integer) = New List(Of Integer)
                    Dim SlutPunktsID As Integer
                    Dim VägTyp As String
                    Dim Avstånd As String
                    Dim KonfliktID As List(Of Integer) = New List(Of Integer)



                    ' onödig beteckning
                    FindInString(Row, ";")
                    ' onödig slutpunkt
                    FindInString(Row, ";")

                    VägTyp = FindInString(Row, ";")
                    ' onödiga grejer
                    For j As Integer = 1 To 8
                        FindInString(Row, ";")
                    Next
                    '
                    FientligTyp = FindInString(Row, ";")
                    ' onödiga grejer
                    FindInString(Row, ";")

                    Avstånd = FindInString(Row, ";")
                    For j As Integer = 1 To 3
                        SlutpunktsLista.Add(FindInString(Row, ","))
                    Next
                    'tabort onödig tecken
                    FindInString(Row, ";")

                    FilterString = "plnr='" & SlutpunktsLista(0) & "' AND typ='" & SlutpunktsLista(1) & "' AND BisID='" & SlutpunktsLista(2) & "'"
                    ResultRows = VtoolsDataSet.BisObjekt.Select(FilterString)
                    If ResultRows.Count > 0 Then
                        FilterString = "BisID='" & ResultRows(0).Item("ID") & "' AND plnr='" & SlutpunktsLista(0) & "' AND Typ='" & VägTyp & "'"
                        ResultRows = VtoolsDataSet.Slutpunkter.Select(FilterString)
                        If ResultRows.Count > 0 Then
                            SlutPunktsID = ResultRows(0).Item("ID")
                        End If
                    End If

                    For j As Integer = 1 To 3
                        FientligLista.Add(FindInString(Row, ","))
                    Next
                    'Riktning
                    FientligLista.Add(FindInString(Row, ";"))

                    ' här kan det väl finnas mer än ett objekt??? eller
                    Dim OmEjSträng As String = FindInString(Row, ";")
                    Dim OmEjRiktingsLista As List(Of String) = New List(Of String)
                    OmEjID = FindBisObjekts(OmEjSträng, OmEjRiktingsLista)



                    FilterString = "plnr='" & FientligLista(0) & "' AND typ='" & FientligLista(1) & "' AND BisID='" & FientligLista(2) & "'"
                    ResultRows = VtoolsDataSet.BisObjekt.Select(FilterString)
                    If ResultRows.Count > 0 Then
                        BisObjekt = ResultRows(0).Item("ID")
                    End If

                    If StrComp(FientligLista(3), "-") = 0 Then
                        ' Har Riktning - så är det en slutpunkt
                        FilterString = "SlutPunktID='" & BisObjekt & "' AND Typ='" & FientligLista(1) & "'"
                        ResultRows = VtoolsDataSet.Vägdelar.Select(FilterString)
                        If ResultRows.Count > 0 Then
                            Dim NyRad As VToolsInmatningDataSet.KonflikterRow = VtoolsDataSet.Konflikter.NewKonflikterRow
                            NyRad.SlutPunktsID = SlutPunktsID
                            NyRad.RörelseID = ResultRows(0).Item("ID")
                            NyRad.Avstånd = Avstånd
                            NyRad.VägTyp = VägTyp
                            VtoolsDataSet.Konflikter.Rows.Add(NyRad)
                            VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
                            KonfliktID.Add(NyRad.ID)
                        End If
                    Else
                        ' 
                        FilterString = "BisID='" & BisObjekt & "' AND Riktning='" & FientligLista(3) & "'"
                        ResultRows = VtoolsDataSet.Låsningar.Select(FilterString)
                        For Each ObRow As DataRow In ResultRows
                            Dim AndraResults() As VToolsInmatningDataSet.VägdelarRow
                            FilterString = "ID='" & ObRow.Item("VägdelsID") & "' AND Typ='" & FientligTyp & "'"
                            AndraResults = VtoolsDataSet.Vägdelar.Select(FilterString)
                            For Each VägRad As VToolsInmatningDataSet.VägdelarRow In AndraResults
                                Dim TredjeResultat() As VToolsInmatningDataSet.VägdelariRörelsevägarRow = VägRad.GetVägdelariRörelsevägarRows
                                For Each VägdelsRad As VToolsInmatningDataSet.VägdelariRörelsevägarRow In TredjeResultat
                                    Dim NyRad As VToolsInmatningDataSet.KonflikterRow = VtoolsDataSet.Konflikter.NewKonflikterRow
                                    NyRad.SlutPunktsID = SlutPunktsID
                                    NyRad.VägTyp = VägTyp
                                    NyRad.RörelseID = VägdelsRad.RörelseID
                                    NyRad.Avstånd = Avstånd
                                    VtoolsDataSet.Konflikter.Rows.Add(NyRad)
                                    VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
                                    KonfliktID.Add(NyRad.ID)
                                Next
                            Next
                        Next

                    End If
                    For Each OmRad As Integer In OmEjID
                        Dim l As Integer = 0
                        For Each k As Integer In KonfliktID
                            Dim NyRad As VToolsInmatningDataSet.KonfliktOmEjRow = VtoolsDataSet.KonfliktOmEj.NewKonfliktOmEjRow
                            NyRad.KonfliktID = k
                            NyRad.BisID = OmRad
                            NyRad.Riktning = OmEjRiktingsLista(l)
                            VtoolsDataSet.KonfliktOmEj.Rows.Add(NyRad)
                        Next
                        VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
                        l += 1
                    Next
                End If
                i += 1
            Next
        Catch ex As Exception
            MessageBox.Show("ConflictingRoutes_xx.csv rad: " & i & ": " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
            Throw New ApplicationException()
        End Try

    End Sub

    Private Function FindBisObjekts(Instring As String, ByRef RiktningsLista As List(Of String)) As List(Of Integer)
        Dim NyLista As List(Of Integer) = New List(Of Integer)
        If Instring.Length > 1 Then
            Do Until Instring = ""
                Dim plnr As String = FindInString(Instring, ",")
                Dim Typ As String = FindInString(Instring, ",")
                Dim BisID As String = FindInString(Instring, ",")
                RiktningsLista.Add(FindInString(Instring, "_"))
                Dim FilterString As String = "plnr='" & plnr & "' AND typ='" & Typ & "' AND BisID='" & BisID & "'"
                Dim ResultRows() As DataRow = VtoolsDataSet.BisObjekt.Select(FilterString)
                If ResultRows.Count > 0 Then
                    NyLista.Add(ResultRows(0).Item("ID"))
                End If
            Loop
        End If
        Return NyLista
    End Function

    Private Sub FyllFrontSkydd(InLista As List(Of String), FolderPath As String)
        Dim i As Integer = 0
        Try
            For Each Row As String In InLista
                If i > 0 Then
                    Dim NyRad As VToolsInmatningDataSet.FrontSkyddRow = VtoolsDataSet.FrontSkydd.NewFrontSkyddRow
                    Dim AllPointsExist As Boolean = True
                    Dim beteckning As String = FindInString(Row, ";")
                    FindInString(Row, ";")
                    NyRad.Typ = FindInString(Row, ";")
                    Dim FrontObjekt As String = FindInString(Row, ";")

                    Dim Om As String
                    Dim OmID As String
                    Dim Låsnignar As String

                    Dim Körningar As String


                    For j As Integer = 1 To 2
                        FindInString(Row, ";")
                    Next
                    Om = FindInString(Row, ";")
                    Låsnignar = FindInString(Row, ";")
                    Körningar = FindInString(Row, ";")
                    Dim SlutTmp As String = FindInString(Row, ";")
                    Dim SlutPunkt As List(Of String) = New List(Of String)
                    For j As Integer = 1 To 3
                        SlutPunkt.Add(FindInString(SlutTmp, ","))
                    Next
                    ' ränsa bort ";" från insträngen
                    '
                    Dim FrontObjektID As String = FindInString(Row, ";")

                    Dim FilterString As String = "plnr='" & SlutPunkt(0) & "' AND typ='" & SlutPunkt(1) & "' AND BisID='" & SlutPunkt(2) & "'"
                    Dim ResultRows() As DataRow = VtoolsDataSet.BisObjekt.Select(FilterString)
                    Dim SlutID As Integer
                    If ResultRows.Count > 0 Then
                        Dim BisID As String = ResultRows(0).Item("ID")

                        FilterString = "BisID='" & BisID & "' AND plnr='" & SlutPunkt(0) & "' AND Typ='" & NyRad.Typ & "'"
                        ResultRows = VtoolsDataSet.Slutpunkter.Select(FilterString)
                        If ResultRows.Count > 0 Then
                            SlutID = ResultRows(0).Item("ID")
                        Else
                            Dim NyttSlut As VToolsInmatningDataSet.SlutpunkterRow = VtoolsDataSet.Slutpunkter.NewSlutpunkterRow
                            NyttSlut.BisID = BisID
                            NyttSlut.plnr = SlutPunkt(0)
                            NyttSlut.Typ = NyRad.Typ
                            VtoolsDataSet.Slutpunkter.Rows.Add(NyttSlut)
                            VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
                            SlutID = NyttSlut.ID

                        End If

                    Else
                        If StrComp(SlutPunkt(0), "0") <> 0 Then
                            Dim NyttBisObjekt As VToolsInmatningDataSet.BisObjektRow = VtoolsDataSet.BisObjekt.NewBisObjektRow
                            NyttBisObjekt.plnr = SlutPunkt(0)
                            NyttBisObjekt.typ = SlutPunkt(1)
                            NyttBisObjekt.BisID = SlutPunkt(2)
                            VtoolsDataSet.BisObjekt.Rows.Add(NyttBisObjekt)
                            VtoolsDataAdaper.BisObjektTableAdapter.Update(VtoolsDataSet.BisObjekt)

                            Dim NySlutpunkt As VToolsInmatningDataSet.SlutpunkterRow = VtoolsDataSet.Slutpunkter.NewSlutpunkterRow
                            NySlutpunkt.BisID = NyttBisObjekt.ID
                            NySlutpunkt.plnr = SlutPunkt(0)
                            NySlutpunkt.Typ = NyRad.Typ
                            VtoolsDataSet.Slutpunkter.Rows.Add(NySlutpunkt)
                            VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
                            SlutID = NySlutpunkt.ID

                        Else
                            AllPointsExist = False
                            Dim TmpString As String = "Frontskydd " & beteckning & " Förkastas pga saknat BisID för slutpunkt"
                            MakeLogUpdate(FolderPath & "\Bortrensade rörelser vid körning " & System.DateTime.Today & ".txt", TmpString)
                        End If
                    End If

                    NyRad.SlutpunktID = SlutID

                    For j As Integer = 1 To 2
                        FindInString(Row, ";")
                    Next

                    If AllPointsExist = True Then
                        OmID = FindInString(Row, ";")

                        Dim LåsningarID As String = FindInString(Row, ";")
                        Dim KörningarID As String = FindInString(Row, ";")


                        NyRad.Skyddssträcka = FindInString(Row, ";")
                        NyRad.Skyddsavstånd_ntv = FindInString(Row, ";")
                        NyRad.Skyddsavstånd_ftv = FindInString(Row, ";")
                        NyRad.Skyddsavstånd_stv = FindInString(Row, ";")
                        NyRad.Skyddsavstånd_vv = FindInString(Row, ";")
                        NyRad.Skyddsavstånd_lo = FindInString(Row, ";")

                        VtoolsDataSet.FrontSkydd.Rows.Add(NyRad)
                        VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
                        FyllUnderTabeller(VtoolsDataSet, FrontObjekt, FrontObjektID, NyRad.ID, "FrontSkyddsObjekt", FolderPath)
                        FyllUnderTabeller(VtoolsDataSet, Om, OmID, NyRad.ID, "OmFront", FolderPath)
                        FyllUnderTabeller(VtoolsDataSet, Låsnignar, LåsningarID, NyRad.ID, "LåsningarFront", FolderPath)
                        FyllUnderTabeller(VtoolsDataSet, Körningar, KörningarID, NyRad.ID, "KörningarFront", FolderPath)
                    End If
                End If
                i += 1
            Next
        Catch ex As Exception
            MessageBox.Show("FrontProtection.csv rad: " & i & ": " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
            Throw New ApplicationException()
        End Try

    End Sub

    Private Sub FyllObjektPerSpl(InLista As List(Of String), FolderPath As String)

        Dim i As Integer = 0
        Try
            For Each Row As String In InLista
                If i > 0 Then
                    Dim AllPointsExists As Boolean = True
                    Dim NyRad As VToolsInmatningDataSet.SplobjektRow = VtoolsDataSet.Splobjekt.NewSplobjektRow
                    ' Objekt Namn
                    Dim objektnamn As String = FindInString(Row, ";")
                    ' Objekt Typ 
                    Dim objekttyp As String = FindInString(Row, ";")
                    ' TC
                    FindInString(Row, ";")
                    ' T1
                    FindInString(Row, ";")
                    Dim BisObjekt As String = FindInString(Row, ";")
                    Dim TC As String = FindInString(Row, ";")
                    Dim T1 As String = FindInString(Row, ";")

                    Dim BisOLista As List(Of String) = New List(Of String)
                    Dim TCLista As List(Of String) = New List(Of String)
                    Dim T1Lista As List(Of String) = New List(Of String)

                    For j As Integer = 1 To 3
                        BisOLista.Add(FindInString(BisObjekt, ","))
                    Next

                    For j As Integer = 1 To 3
                        TCLista.Add(FindInString(TC, ","))
                    Next

                    For j As Integer = 1 To 3
                        T1Lista.Add(FindInString(T1, ","))
                    Next

                    Dim FilterString As String = "plnr='" & BisOLista(0) & "' AND typ='" & BisOLista(1) & "' AND BisID='" & BisOLista(2) & "'"
                    Dim ResultRows() As DataRow = VtoolsDataSet.BisObjekt.Select(FilterString)
                    If ResultRows.Count > 0 Then
                        NyRad.BisID = ResultRows(0).Item("ID")
                    Else
                        If StrComp(BisOLista(0), "0") <> 0 Then
                            Dim NyBis As VToolsInmatningDataSet.BisObjektRow = VtoolsDataSet.BisObjekt.NewBisObjektRow
                            NyBis.plnr = BisOLista(0)
                            NyBis.typ = BisOLista(1)
                            NyBis.BisID = BisOLista(2)
                            VtoolsDataSet.BisObjekt.Rows.Add(NyBis)
                            VtoolsDataAdaper.BisObjektTableAdapter.Update(VtoolsDataSet.BisObjekt)
                            NyRad.BisID = NyBis.ID
                        Else
                            AllPointsExists = False
                            Select Case objekttyp
                                Case "EB", "stb"
                                Case Else
                                    Dim tmp As String = "Objekt " & objekttyp & objektnamn & " saknar BisID"
                                    MakeLogUpdate(FolderPath & "\Bortrensade rörelser vid körning " & System.DateTime.Today & ".txt", tmp)
                            End Select
                        End If
                    End If


                    If (CInt(TCLista(0)) <> 0) = True Then
                        FilterString = "plnr='" & TCLista(0) & "' AND typ='" & TCLista(1) & "' AND BisID='" & TCLista(2) & "'"
                        ResultRows = VtoolsDataSet.BisObjekt.Select(FilterString)
                        If ResultRows.Count > 0 Then
                            NyRad.SplID = ResultRows(0).Item("ID")
                        Else
                            Dim NyBis As VToolsInmatningDataSet.BisObjektRow = VtoolsDataSet.BisObjekt.NewBisObjektRow
                            NyBis.plnr = TCLista(0)
                            NyBis.typ = TCLista(1)
                            NyBis.BisID = TCLista(2)
                            VtoolsDataSet.BisObjekt.Rows.Add(NyBis)
                            VtoolsDataAdaper.BisObjektTableAdapter.Update(VtoolsDataSet.BisObjekt)
                            NyRad.SplID = NyBis.ID
                        End If

                        If (CInt(T1Lista(0) <> 0)) Then
                            FilterString = "plnr='" & T1Lista(0) & "' AND typ='" & T1Lista(1) & "' AND BisID='" & T1Lista(2) & "'"
                            ResultRows = VtoolsDataSet.BisObjekt.Select(FilterString)
                            If ResultRows.Count > 0 Then
                                NyRad.T1 = ResultRows(0).Item("ID")
                            Else
                                Dim NyBis As VToolsInmatningDataSet.BisObjektRow = VtoolsDataSet.BisObjekt.NewBisObjektRow
                                NyBis.plnr = T1Lista(0)
                                NyBis.typ = T1Lista(1)
                                NyBis.BisID = T1Lista(2)
                                VtoolsDataSet.BisObjekt.Rows.Add(NyBis)
                                VtoolsDataAdaper.BisObjektTableAdapter.Update(VtoolsDataSet.BisObjekt)
                                NyRad.T1 = NyBis.ID
                            End If

                        End If

                        If AllPointsExists = True Then
                            FilterString = "BisID='" & NyRad.BisID & "' AND SplID='" & NyRad.SplID & "'"
                            ResultRows = VtoolsDataSet.Splobjekt.Select(FilterString)
                            If ResultRows.Count < 1 Then
                                VtoolsDataSet.Splobjekt.Rows.Add(NyRad)
                            End If
                        End If

                    Else
                        Select Case objekttyp
                            Case "EB", "stb"
                            Case Else
                                Dim tmp As String = "Objekt " & objekttyp & objektnamn & " saknar BisID på TC"
                                MakeLogUpdate(FolderPath & "\Bortrensade rörelser vid körning " & System.DateTime.Today & ".txt", tmp)
                        End Select
                    End If
                End If
                i += 1
            Next
            VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
        Catch ex As Exception
            MessageBox.Show("TC_T1_Mapping.csv: rad: " & i & ": " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
            Throw New ApplicationException()
        End Try

    End Sub

    Private Sub FyllBacktoback(InLista As List(Of String), FolderPath As String)

        Dim i As Integer = 0
        Try
            For Each Row As String In InLista
                If i > 0 Then

                    FindInString(Row, ";")
                    FindInString(Row, ";")
                    Dim Obj1 As String = FindInString(Row, ";")
                    Dim Obj2 As String = FindInString(Row, ";")

                    Dim Obj1List As List(Of String) = New List(Of String)
                    Dim Obj2List As List(Of String) = New List(Of String)
                    Dim MellanLista As List(Of String) = New List(Of String)
                    Do Until Obj1 = ""
                        MellanLista.Add(FindInString(Obj1, "_"))
                    Loop

                    For k As Integer = 0 To MellanLista.Count - 1
                        For j As Integer = 1 To 4
                            Obj1List.Add(FindInString(MellanLista(k), ","))
                        Next
                    Next

                    MellanLista = New List(Of String)

                    Do Until Obj2 = ""
                        MellanLista.Add(FindInString(Obj2, "_"))
                    Loop

                    For k As Integer = 0 To MellanLista.Count - 1
                        For j As Integer = 1 To 4
                            Obj2List.Add(FindInString(MellanLista(k), ","))
                        Next
                    Next

                    Dim q As Integer = 0
                    Do While q < Obj1List.Count
                        Dim NyRad As VToolsInmatningDataSet.BacktoBackRow = VtoolsDataSet.BacktoBack.NewBacktoBackRow
                        NyRad.Obj1BisID = Obj1List(q + 2)
                        NyRad.Obj1typ = Obj1List(q + 1)
                        If Obj2List.Count > 4 Then
                            Dim w As Integer = 4
                            Do While w < Obj2List.Count
                                Dim NyRad2 As VToolsInmatningDataSet.BacktoBackRow = VtoolsDataSet.BacktoBack.NewBacktoBackRow
                                NyRad2.Obj1BisID = NyRad.Obj1BisID
                                NyRad2.Obj1typ = NyRad.Obj1typ
                                NyRad2.Obj2BisID = Obj2List(w + 2)
                                NyRad2.Obj3typ = Obj2List(w + 1)
                                VtoolsDataSet.BacktoBack.Rows.Add(NyRad2)
                                w += 4
                            Loop
                            NyRad.Obj2BisID = Obj2List(2)
                            NyRad.Obj3typ = Obj2List(1)
                        Else
                            NyRad.Obj2BisID = Obj2List(2)
                            NyRad.Obj3typ = Obj2List(1)
                        End If

                        VtoolsDataSet.BacktoBack.Rows.Add(NyRad)
                        q += 4
                    Loop

                End If
                i += 1
            Next
            VtoolsbacktobackDataAdapter.Update(VtoolsDataSet.BacktoBack)
        Catch ex As Exception
            MessageBox.Show("BackToBack_Signals.csv: rad: " & i & ": " & ex.Message, "Felmeddelande", MessageBoxButton.OK, MessageBoxImage.Error)
            Throw New ApplicationException()
        End Try

    End Sub

#Region "Read line i textfile"
    'read line i textfile
    Public Function ReadLine(lineNumber As Integer, lines As List(Of String)) As String
        Return lines(lineNumber - 1)
    End Function
#End Region

#Region "FixEndPointTableSet"
    'Private Function FixEndPointTableSet() As DataSet
    '    Dim _DSet As DataSet = New DataSet
    '    Dim SlutPunkter As DataTable = New DataTable("SlutPunkter")

    '    Dim NamnColumn As DataColumn = New DataColumn("ObjektNamn", System.Type.GetType("System.String"))
    '    Dim TypColumn As DataColumn = New DataColumn("objtypnr", System.Type.GetType("System.String"))
    '    Dim ObjetNr As DataColumn = New DataColumn("obnr", System.Type.GetType("System.String"))
    '    Dim Status As DataColumn = New DataColumn("status", System.Type.GetType("System.String"))
    '    Dim PlatsColumn As DataColumn = New DataColumn("plnr", System.Type.GetType("System.String"))


    '    SlutPunkter.Columns.Add(NamnColumn)
    '    SlutPunkter.Columns.Add(TypColumn)
    '    SlutPunkter.Columns.Add(ObjetNr)
    '    SlutPunkter.Columns.Add(Status)
    '    SlutPunkter.Columns.Add(PlatsColumn)
    '    SlutPunkter.Constraints.Add("PrimaryKey", ObjetNr, True)

    '    Dim FrontSkydd As DataTable = New DataTable("Frontskydd")
    '    Dim IDColumn As DataColumn = New DataColumn("ID", System.Type.GetType("System.String"))
    '    Dim FKID As New DataColumn("FK_ID", System.Type.GetType("System.String"))
    '    Dim TypAvRorelse As DataColumn = New DataColumn("TypAvRorelse", System.Type.GetType("System.String"))
    '    Dim SkyddsAvstandMotNtv As DataColumn = New DataColumn("SkyddsAvstandMotNtv", System.Type.GetType("System.String"))
    '    Dim SkyddsAvstandMotFtv As DataColumn = New DataColumn("SkyddsAvstandMotFtv", System.Type.GetType("System.String"))
    '    Dim SkyddsAvstandMotStv As DataColumn = New DataColumn("SkyddsAvstandMotStv", System.Type.GetType("System.String"))
    '    Dim SkyddsAvstandMotVv As DataColumn = New DataColumn("SkyddsAvstandMotVv", System.Type.GetType("System.String"))
    '    Dim SkyddsAvstandMotLo As DataColumn = New DataColumn("SkyddsAvstandMotLo", System.Type.GetType("System.String"))
    '    Dim SkyddsStracka As DataColumn = New DataColumn("SkyddsStracka", System.Type.GetType("System.String"))

    '    FrontSkydd.Columns.Add(IDColumn)
    '    FrontSkydd.Columns.Add(FKID)
    '    FrontSkydd.Columns.Add(TypAvRorelse)
    '    FrontSkydd.Columns.Add(SkyddsAvstandMotNtv)
    '    FrontSkydd.Columns.Add(SkyddsAvstandMotFtv)
    '    FrontSkydd.Columns.Add(SkyddsAvstandMotStv)
    '    FrontSkydd.Columns.Add(SkyddsAvstandMotVv)
    '    FrontSkydd.Columns.Add(SkyddsAvstandMotLo)
    '    FrontSkydd.Columns.Add(SkyddsStracka)

    '    FrontSkydd.Constraints.Add("PrimaryKey", IDColumn, True)


    '    Dim VillkorKöring As DataTable = New DataTable("VilkorKöring")
    '    IDColumn = New DataColumn("ID", System.Type.GetType("System.String"))
    '    FKID = New DataColumn("FK_ID", System.Type.GetType("System.String"))
    '    Dim Sekvensnummer As DataColumn = New DataColumn("Sekvensnummer", System.Type.GetType("System.String"))
    '    Dim ObjektNamn As DataColumn = New DataColumn("ObjektNamn", System.Type.GetType("System.String"))
    '    Dim plnr As DataColumn = New DataColumn("plnr", System.Type.GetType("System.String"))
    '    Dim objtypnr As DataColumn = New DataColumn("objtypnr", System.Type.GetType("System.String"))
    '    Dim obnr As DataColumn = New DataColumn("obnr", System.Type.GetType("System.String"))
    '    Status = New DataColumn("status", System.Type.GetType("System.String"))

    '    VillkorKöring.Columns.Add(IDColumn)
    '    VillkorKöring.Columns.Add(FKID)
    '    VillkorKöring.Columns.Add(Sekvensnummer)
    '    VillkorKöring.Columns.Add(ObjektNamn)
    '    VillkorKöring.Columns.Add(plnr)
    '    VillkorKöring.Columns.Add(objtypnr)
    '    VillkorKöring.Columns.Add(obnr)
    '    VillkorKöring.Columns.Add(Status)

    '    VillkorKöring.Constraints.Add("PrimaryKey", IDColumn, True)

    '    Dim VillkorLåsning As DataTable = New DataTable("VilkorLåsning")
    '    IDColumn = New DataColumn("ID", System.Type.GetType("System.String"))
    '    FKID = New DataColumn("FK_ID", System.Type.GetType("System.String"))
    '    Sekvensnummer = New DataColumn("Sekvensnummer", System.Type.GetType("System.String"))
    '    ObjektNamn = New DataColumn("ObjektNamn", System.Type.GetType("System.String"))
    '    plnr = New DataColumn("plnr", System.Type.GetType("System.String"))
    '    objtypnr = New DataColumn("objtypnr", System.Type.GetType("System.String"))
    '    obnr = New DataColumn("obnr", System.Type.GetType("System.String"))
    '    Status = New DataColumn("status", System.Type.GetType("System.String"))

    '    VillkorLåsning.Columns.Add(IDColumn)
    '    VillkorLåsning.Columns.Add(FKID)
    '    VillkorLåsning.Columns.Add(Sekvensnummer)
    '    VillkorLåsning.Columns.Add(ObjektNamn)
    '    VillkorLåsning.Columns.Add(plnr)
    '    VillkorLåsning.Columns.Add(objtypnr)
    '    VillkorLåsning.Columns.Add(obnr)
    '    VillkorLåsning.Columns.Add(Status)

    '    VillkorLåsning.Constraints.Add("PrimaryKey", IDColumn, True)

    '    Dim OMVilkor As DataTable = New DataTable("Om")
    '    IDColumn = New DataColumn("ID", System.Type.GetType("System.String"))
    '    FKID = New DataColumn("FK_ID", System.Type.GetType("System.String"))
    '    Sekvensnummer = New DataColumn("Sekvensnummer", System.Type.GetType("System.String"))
    '    ObjektNamn = New DataColumn("ObjektNamn", System.Type.GetType("System.String"))
    '    plnr = New DataColumn("plnr", System.Type.GetType("System.String"))
    '    objtypnr = New DataColumn("objtypnr", System.Type.GetType("System.String"))
    '    obnr = New DataColumn("obnr", System.Type.GetType("System.String"))
    '    Status = New DataColumn("status", System.Type.GetType("System.String"))

    '    OMVilkor.Columns.Add(IDColumn)
    '    OMVilkor.Columns.Add(FKID)
    '    OMVilkor.Columns.Add(Sekvensnummer)
    '    OMVilkor.Columns.Add(ObjektNamn)
    '    OMVilkor.Columns.Add(plnr)
    '    OMVilkor.Columns.Add(objtypnr)
    '    OMVilkor.Columns.Add(obnr)
    '    OMVilkor.Columns.Add(Status)

    '    OMVilkor.Constraints.Add("PrimaryKey", IDColumn, True)

    '    Dim FrontObjekt As DataTable = New DataTable("FrontObjekt")
    '    IDColumn = New DataColumn("ID", System.Type.GetType("System.String"))
    '    FKID = New DataColumn("FK_ID", System.Type.GetType("System.String"))
    '    Sekvensnummer = New DataColumn("Sekvensnummer", System.Type.GetType("System.String"))
    '    ObjektNamn = New DataColumn("ObjektNamn", System.Type.GetType("System.String"))
    '    plnr = New DataColumn("plnr", System.Type.GetType("System.String"))
    '    objtypnr = New DataColumn("objtypnr", System.Type.GetType("System.String"))
    '    obnr = New DataColumn("obnr", System.Type.GetType("System.String"))
    '    Status = New DataColumn("status", System.Type.GetType("System.String"))

    '    FrontObjekt.Columns.Add(IDColumn)
    '    FrontObjekt.Columns.Add(FKID)
    '    FrontObjekt.Columns.Add(Sekvensnummer)
    '    FrontObjekt.Columns.Add(ObjektNamn)
    '    FrontObjekt.Columns.Add(plnr)
    '    FrontObjekt.Columns.Add(objtypnr)
    '    FrontObjekt.Columns.Add(obnr)
    '    FrontObjekt.Columns.Add(Status)

    '    FrontObjekt.Constraints.Add("PrimaryKey", IDColumn, True)

    '    _DSet.Tables.Add(SlutPunkter)
    '    _DSet.Tables.Add(FrontSkydd)
    '    _DSet.Tables.Add(VillkorKöring)
    '    _DSet.Tables.Add(VillkorLåsning)
    '    _DSet.Tables.Add(OMVilkor)
    '    _DSet.Tables.Add(FrontObjekt)

    '    Dim ChildRelation As DataRelation = New DataRelation("Slutpunkt_FrontSkydd", _DSet.Tables("SlutPunkter").Columns("obnr"), _DSet.Tables("FrontSkydd").Columns("FK_ID"), False)
    '    _DSet.Relations.Add(ChildRelation)
    '    ChildRelation = New DataRelation("FrontSkydd_VillkorKöring", _DSet.Tables("FrontSkydd").Columns("ID"), _DSet.Tables("VilkorKöring").Columns("FK_ID"), False)
    '    _DSet.Relations.Add(ChildRelation)

    '    ChildRelation = New DataRelation("FrontSkydd_VillkorLåsning", _DSet.Tables("FrontSkydd").Columns("ID"), _DSet.Tables("VilkorLåsning").Columns("FK_ID"), False)
    '    _DSet.Relations.Add(ChildRelation)

    '    ChildRelation = New DataRelation("FrontSkydd_Om", _DSet.Tables("FrontSkydd").Columns("ID"), _DSet.Tables("Om").Columns("FK_ID"), False)
    '    _DSet.Relations.Add(ChildRelation)

    '    ChildRelation = New DataRelation("FrontSkydd_FrontObjekt", _DSet.Tables("FrontSkydd").Columns("ID"), _DSet.Tables("FrontObjekt").Columns("FK_ID"), False)
    '    _DSet.Relations.Add(ChildRelation)


    '    Return _DSet
    'End Function
#End Region

#Region "MakeLogUpdate"
    Public Sub MakeLogUpdate(FileName As String, TextString As String)
        TextString = TextString & Environment.NewLine
        File.AppendAllText(FileName, TextString)
    End Sub
#End Region

#Region "Nytt Läs i lokala CSV-filer och skapa data i temporär databas"
    Public Sub Inmatnign(FolderPath As String)
        ImportRoadsOfRoadParts_ExportXML(FolderPath)
    End Sub
#End Region

#Region "Skriv ut VtoolsData"
    Public Sub SkrivutVtoolsData(ByVal xmlfilename As String, ByRef DriftPlats As VToolsInmatningDataSet.DriftplatserRow, FolderPath As String)
        Try
            Dim rt As RorelsevagarOchSkyddLib.Signaldata = New RorelsevagarOchSkyddLib.Signaldata
            rt.Trafikplatssignatur = DriftPlats.plsign
            Dim VägRader() As VToolsInmatningDataSet.RörelsevägarRow = DriftPlats.GetRörelsevägarRows
            '--------------------------
            '    Rörelsevägar
            '--------------------------
            For Each VägRad As VToolsInmatningDataSet.RörelsevägarRow In VägRader
                Dim Spliväg As List(Of RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable) = New List(Of RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable)

                Dim RörelseVäg As RorelsevagarOchSkyddLib.RorelsevagA = RörelseBasInfo(VägRad, rt.Trafikplatssignatur)
                '--------------------------
                '    Positioner
                '--------------------------
                PositioneringsObjekt(RörelseVäg, VägRad, Spliväg)

                '--------------------------
                '    Upplåningar
                '--------------------------
                Select Case DriftPlats.stlvtyp
                    Case "stlv95", "cst"
                        UpplåsningspunkterStlv95(RörelseVäg, Spliväg)
                    Case Else
                        ' verkar som att både stlv85 och stlv65 har samma formel för upplåsningspunkter
                        UppåsningsPunkterStlv85(RörelseVäg, VägRad, FolderPath)
                End Select


                rt.Rorelsevagar.Add(RörelseVäg)
            Next

            '--------------------------
            '    SlutPunkt
            '--------------------------
            SlutPunktsInfo(rt, DriftPlats)

            rt.ToXmlFile(xmlfilename)
            'Dim VToolDataSet As VtoolsDatasetClass = New VtoolsDatasetClass()
            'VToolDataSet.XmlRead(InputFilepath)
        Catch ex As Exception
            DisplayError(ex)

        End Try
    End Sub
#End Region

#Region "RörelseBasInfo"
    Public Function RörelseBasInfo(ByRef DataR As VToolsInmatningDataSet.RörelsevägarRow, KortNamn As String) As RorelsevagarOchSkyddLib.RorelsevagA
        Dim Väg As RorelsevagarOchSkyddLib.RorelsevagA = New RorelsevagarOchSkyddLib.RorelsevagA
        Dim ResultRows() As DataRow
        Dim FilterString As String
        Dim slutpunkt As String = DataR.Beteckning
        Dim StartPunkt As String = FindInString(slutpunkt, ".")
        slutpunkt = FindInString(slutpunkt, " ")
        Väg.Id = DataR.TRV_ID

        Väg.Namn = DataR.Beteckning
        Select Case DataR.Item("Typ")
            Case "Tågväg"
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
                'Väg.Namn = "tv-" & StartPunkt & "-" & slutpunkt & " " & KortNamn
            Case "Växlingsväg"
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Växlingsväg
            Case "ftv"
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Förenkladtågväg
            Case "stv"
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Särskildtågväg
            Case "Linje"
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Blocksträcka
            Case "tam"
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.TAMsträcka
            Case "btv"
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Bevakadtågväg
            Case "ntv"
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
            Case Else
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
                MessageBox.Show("Rörelsevägar. Typ av rörelseväg ej implementerad i Vtools. Kontakta programmerare. Rörelsetyp:  " & Rörelsevägstyp)
                'Me.Close()
        End Select

        Väg.Borjanpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
        FilterString = "ID='" & DataR.BörjanPunktID & "'"
        ResultRows = VtoolsDataSet.BisObjekt.Select(FilterString)

        Väg.Borjanpunkt.BisObjektId.Objektnummer = DataR.BisObjektRow.BisID
        Väg.Borjanpunkt.BisObjektId.Objekttypnummer = DataR.BisObjektRow.typ

        FilterString = "ID='" & DataR.SlutPunktID & "'"
        ResultRows = VtoolsDataSet.BisObjekt.Select(FilterString)

        '-- BIS objekt slutpunkt --
        Väg.Slutpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
        'Måste få tillgång till Korrekt Vtools export

        Väg.Slutpunkt.BisObjektId.Objektnummer = DataR.SlutpunkterRow.BisObjektRow.BisID
        Väg.Slutpunkt.BisObjektId.Objekttypnummer = DataR.SlutpunkterRow.BisObjektRow.typ

        Return Väg
    End Function
#End Region

#Region "PositioneringsObjekt"
    Public Sub PositioneringsObjekt(ByRef Rorelsevag As RorelsevagarOchSkyddLib.RorelsevagA, ByVal RoadRow As VToolsInmatningDataSet.RörelsevägarRow,
                                  ByRef SpliVäg As List(Of RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable))
        Dim FilterString As String
        Dim ResultRows() As DataRow
        Try
            Dim VagDelar() As VToolsInmatningDataSet.VägdelariRörelsevägarRow = RoadRow.GetVägdelariRörelsevägarRows

            Dim SekvensNr As Integer = 1
            For Each VagDel As VToolsInmatningDataSet.VägdelariRörelsevägarRow In VagDelar
                Dim VagDelObjs() As VToolsInmatningDataSet.SpårledningarRow
                Dim RoadPart As VToolsInmatningDataSet.VägdelarRow = VagDel.VägdelarRow 'VtoolsDataSet.Vägdelar.Select(FilterString)

                VagDelObjs = RoadPart.GetSpårledningarRows
                For Each _ObjRow As VToolsInmatningDataSet.SpårledningarRow In VagDelObjs
                    Dim NewRoadPart As RorelsevagarOchSkyddLib.PositionA = New RorelsevagarOchSkyddLib.PositionA
                    Dim ObjektIVägRader() As VToolsInmatningDataSet.SplobjektRow = _ObjRow.BisObjektRow.GetSplobjektRows
                    NewRoadPart.Sekvensnummer = SekvensNr
                    NewRoadPart.PositioneringsobjektId = Spl(_ObjRow)
                    If ObjektIVägRader.Count > 0 Then
                        Dim ObjektArray As RorelsevagarOchSkyddLib.ArrayOfObjektMedVillkor = New RorelsevagarOchSkyddLib.ArrayOfObjektMedVillkor
                        For Each ObjektRad As VToolsInmatningDataSet.SplobjektRow In ObjektIVägRader
                            Dim NyttObjekt As RorelsevagarOchSkyddLib.ObjektMedVillkor_Nillable = New RorelsevagarOchSkyddLib.ObjektMedVillkor_Nillable
                            FilterString = "ID='" & ObjektRad.BisID & "'"
                            ResultRows = VtoolsDataSet.BisObjekt.Select(FilterString)

                            If StrComp(ResultRows(0).Item("typ"), "3310") <> 0 Then
                                ' 3310 är stoppbock kan inte vara en del av en rörelseväg
                                NyttObjekt.ObjektId = New RorelsevagarOchSkyddLib.ObjektIdA
                                NyttObjekt.ObjektId.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                                NyttObjekt.ObjektId.BisObjektId.Objektnummer = ResultRows(0).Item("BisID")
                                NyttObjekt.ObjektId.BisObjektId.Objekttypnummer = ResultRows(0).Item("typ")
                                NyttObjekt.ArSkyddsObjekt = False

                                Select Case RoadPart.Typ
                                    Case "Tågväg"
                                        NyttObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
                                    Case "Särskildtågväg"
                                        NyttObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Särskildtågväg
                                    Case "Förenkladtågväg"
                                        NyttObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Förenkladtågväg
                                    Case "Linje"
                                        NyttObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Blocksträcka
                                    Case Else ' växlingsväg
                                        NyttObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Växlingsväg
                                End Select

                                NyttObjekt.TypAvVillkor = RorelsevagarOchSkyddLib.Enumerations.TypAvVillkorA.Väglåsning

                                FilterString = "VägdelsID='" & RoadPart.ID & "' AND BisID='" & ResultRows(0).Item("ID") & "'"
                                ResultRows = VtoolsDataSet.Låsningar.Select(FilterString)
                                Dim Riktning As String
                                If ResultRows.Count > 0 Then
                                    ' objektet fanns bland låsnignar
                                    Riktning = ResultRows(0).Item("Riktning")
                                Else
                                    ' fanns inte i låsningar kolla om det är en motriktad signal
                                    ResultRows = VtoolsDataSet.MotriktadeObjekt.Select(FilterString)
                                    If ResultRows.Count > 0 Then
                                        If StrComp(ResultRows(0).Item("Riktning"), "-") = 0 Then
                                            ' det är en motriktad signal och då skall den stå i stop 
                                            Riktning = "S"
                                        Else
                                            Riktning = ResultRows(0).Item("Riktning")
                                        End If
                                    Else
                                        ' fanns inte som en motriktad signal då är det en medriktad signal som är en mellan punkt i vtools
                                        ' kan vara så att det är ett objekt som inte passeras 
                                        ResultRows = VtoolsDataSet.ObjektiVäg.Select(FilterString)
                                        If ResultRows.Count > 0 Then
                                            Riktning = ResultRows(0).Item("Riktning")
                                        Else

                                            Riktning = ""
                                        End If

                                    End If

                                End If
                                If Riktning.Length > 0 Then
                                    Select Case Riktning
                                        Case "-"
                                            ' medriktad signal bör visa kör
                                            NyttObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Inget
                                        Case "V"
                                            NyttObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Vänster
                                        Case "H"
                                            NyttObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Höger
                                        Case "P"
                                            NyttObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.På
                                        Case "S"
                                            NyttObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Stopp
                                        Case Else
                                            NyttObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Av
                                    End Select

                                    ObjektArray.ObjektMedVillkor.Add(NyttObjekt)
                                End If
                            End If
                        Next
                        NewRoadPart.ObjektMedVillkor = ObjektArray
                    End If

                    SpliVäg.Add(Spl(_ObjRow))
                    Rorelsevag.Positioner.Add(NewRoadPart)
                    SekvensNr += 1
                Next
            Next
        Catch ex As Exception
            'MsgBox("FilterString = " & FilterString & "ResultRow Count: " & ResultRows.Count)
        End Try

    End Sub
#End Region

#Region "Spl"
    Public Function Spl(ByRef TrackRow As VToolsInmatningDataSet.SpårledningarRow) As RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable
        Dim _oSpl As RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable = New RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable

        Dim BisObj As RorelsevagarOchSkyddLib.BisObjektIdA = New RorelsevagarOchSkyddLib.BisObjektIdA
        BisObj.Objektnummer = TrackRow.BisObjektRow.BisID
        BisObj.Objekttypnummer = TrackRow.BisObjektRow.typ
        _oSpl.BisObjektId = BisObj
        Return _oSpl
    End Function
#End Region

#Region "UpplåsningspunkterStlv95"
    Public Sub UpplåsningspunkterStlv95(ByRef RörelseVäg As RorelsevagarOchSkyddLib.RorelsevagA, ByRef SpliVäg As List(Of RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable))
        Dim i As Integer = 0

        For J As Integer = 1 To SpliVäg.Count - 1
            Dim NyUpplåsning As RorelsevagarOchSkyddLib.UpplasningspunktA = New RorelsevagarOchSkyddLib.UpplasningspunktA
            NyUpplåsning.Borjanpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
            NyUpplåsning.Borjanpunkt.BisObjektId.Objektnummer = SpliVäg(i).BisObjektId.Objektnummer
            NyUpplåsning.Borjanpunkt.BisObjektId.Objekttypnummer = SpliVäg(i).BisObjektId.Objekttypnummer

            NyUpplåsning.Slutpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
            NyUpplåsning.Slutpunkt.BisObjektId.Objektnummer = SpliVäg(J).BisObjektId.Objektnummer
            NyUpplåsning.Slutpunkt.BisObjektId.Objekttypnummer = SpliVäg(J).BisObjektId.Objekttypnummer
            'NyUpplåsning.Borjanpunkt = SpliVäg(i)
            'NyUpplåsning.Slutpunkt = SpliVäg(J)



            Select Case SpliVäg(i).BisObjektId.Objekttypnummer
                Case "10019"
                    NyUpplåsning.TypAvUpplasningspunkt = RorelsevagarOchSkyddLib.Enumerations.TypAvUpplasningspunktA.Isolskarv
                Case "6000", "10023"
                    NyUpplåsning.TypAvUpplasningspunkt = RorelsevagarOchSkyddLib.Enumerations.TypAvUpplasningspunktA.Signal
                Case "6006"
                    NyUpplåsning.TypAvUpplasningspunkt = RorelsevagarOchSkyddLib.Enumerations.TypAvUpplasningspunktA.Tavla
            End Select
            NyUpplåsning.Sekvensnummer = J
            i += 1
            RörelseVäg.Upplasningspunkter.Add(NyUpplåsning)
        Next

    End Sub
#End Region

#Region "UppåsningsPunkterStlv85"
    Public Sub UppåsningsPunkterStlv85(ByRef RörelseVäg As RorelsevagarOchSkyddLib.RorelsevagA, Väg As VToolsInmatningDataSet.RörelsevägarRow, FolderPath As String)
        Dim Vägdelar() As VToolsInmatningDataSet.VägdelariRörelsevägarRow = Väg.GetVägdelariRörelsevägarRows
        Dim J As Integer = 1
        Dim i As Integer = 1
        For Each VägDel As VToolsInmatningDataSet.VägdelariRörelsevägarRow In Vägdelar
            Dim FilterString As String = "BisID='" & VägDel.VägdelarRow.SlutPunktID & "' AND VägdelsID='" & VägDel.VägdelsID & "'"

            Dim ObjektRader() As VToolsInmatningDataSet.MotriktadeObjektRow = VtoolsDataSet.MotriktadeObjekt.Select(FilterString)

            For Each ObjektRad As VToolsInmatningDataSet.MotriktadeObjektRow In ObjektRader
                Dim Viableobject As Boolean = True
                ' Specialregler för första vägdelen 
                ' för att hitta rygg i rygg signaler
                If i = 1 Then
                    FilterString = "BisID='" & VägDel.VägdelarRow.BörjanPunktID & "'"
                    Dim bpRows() As VToolsInmatningDataSet.SplobjektRow = VtoolsDataSet.Splobjekt.Select(FilterString)
                    FilterString = "BisID='" & ObjektRad.BisID & "'"

                    Dim spRows() As VToolsInmatningDataSet.SplobjektRow = VtoolsDataSet.Splobjekt.Select(FilterString)
                    If bpRows.Count > 0 And spRows.Count > 0 Then
                        If IsDBNull(bpRows(0).T1) = False Then
                            If spRows(0).SplID = bpRows(0).T1 Then
                                Viableobject = False
                            End If
                        Else
                            Dim tmp As String = "Vägdel " & VägDel.VägdelarRow.Beteckning & " saknar T1 information på börjanpunkt"
                            ImportCVSFile.MakeLogUpdate(FolderPath & "\Bortrensade rörelser vid körning " & System.DateTime.Today & ".txt", tmp)
                        End If


                    Else
                        Viableobject = False
                        'MsgBox("Saknar TC på antingen bärjan punkten eller slutpunkten ")
                        Dim tmp As String = "Vägdel " & VägDel.VägdelarRow.Beteckning & " saknar TC information på början- eller slutpunkt och förkastar därför första möjliga upplåsningspunkten"
                        ImportCVSFile.MakeLogUpdate(FolderPath & "\Bortrensade rörelser vid körning " & System.DateTime.Today & ".txt", tmp)

                    End If

                End If
                If Viableobject = True Then
                    Dim NyUpplåsning As RorelsevagarOchSkyddLib.UpplasningspunktA = New RorelsevagarOchSkyddLib.UpplasningspunktA

                    Dim AllPointsExists As Boolean = True
                    FilterString = "BisID='" & ObjektRad.BisID & "'"
                    Dim SecondFilter As String
                    Dim ResultRows() As DataRow = VtoolsDataSet.Splobjekt.Select(FilterString)

                    If ResultRows.Count > 0 Then
                        FilterString = "ID='" & ResultRows(0).Item("SplID") & "'"
                        If IsDBNull(ResultRows(0).Item("T1")) = False Then
                            SecondFilter = "ID='" & ResultRows(0).Item("T1") & "'"
                        Else
                            AllPointsExists = False
                            SecondFilter = ""
                        End If

                        ResultRows = VtoolsDataSet.BisObjekt.Select(FilterString)
                        If ResultRows.Count > 0 Then
                            NyUpplåsning.Borjanpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                            NyUpplåsning.Borjanpunkt.BisObjektId.Objektnummer = ResultRows(0).Item("BisID")
                            ' det kan bara vara signaler här
                            NyUpplåsning.Borjanpunkt.BisObjektId.Objekttypnummer = ResultRows(0).Item("typ")

                            Select Case ResultRows(0).Item("typ")
                                Case "10019"
                                    NyUpplåsning.TypAvUpplasningspunkt = RorelsevagarOchSkyddLib.Enumerations.TypAvUpplasningspunktA.Isolskarv
                                Case "6000", "10023"
                                    NyUpplåsning.TypAvUpplasningspunkt = RorelsevagarOchSkyddLib.Enumerations.TypAvUpplasningspunktA.Signal
                            End Select
                        End If
                        If AllPointsExists = True Then
                            ResultRows = VtoolsDataSet.BisObjekt.Select(SecondFilter)
                            If ResultRows.Count > 0 Then
                                NyUpplåsning.Slutpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                                NyUpplåsning.Slutpunkt.BisObjektId.Objektnummer = ResultRows(0).Item("BisID")
                                NyUpplåsning.Slutpunkt.BisObjektId.Objekttypnummer = ResultRows(0).Item("typ")
                            End If
                        Else
                            Select Case ResultRows(0).Item("typ")
                                Case "3300", "3010", "3011", "3310, ""10015", "10050"
                                    ' dessa har inte någon T1
                                Case Else
                                    Dim tmp As String = "RörelseVäg " & RörelseVäg.Id & " saknar T1 information på en upplåsnings information"
                                    ImportCVSFile.MakeLogUpdate(FolderPath & "\Bortrensade rörelser vid körning " & System.DateTime.Today & ".txt", tmp)
                            End Select

                        End If

                    End If

                    NyUpplåsning.Sekvensnummer = J
                    RörelseVäg.Upplasningspunkter.Add(NyUpplåsning)
                    J += 1
                End If
            Next
            i += 1
        Next
    End Sub
#End Region

#Region "SlutPunktsInfo"
    Public Sub SlutPunktsInfo(ByRef SignalData As RorelsevagarOchSkyddLib.Signaldata, ByRef DriftPlats As VToolsInmatningDataSet.DriftplatserRow)
        ' Vi skall inte lägga till en rad för varje slutpunkt + typ utan bara för varje Slutpunkt
        ' Alltså fel här
        Dim FilterString As String = "plnr='" & DriftPlats.plnr & "'"
        Dim SortString As String = "BisID ASC"
        Dim LastBisID As Integer = -1
        Dim SlutPunkter() As DataRow = VtoolsDataSet.Slutpunkter.Select(FilterString, SortString)
        Dim KonfliktRader() As VToolsInmatningDataSet.KonflikterRow

        ' Dim ResultRows() As DataRow
        Dim ObjCreated As Boolean = False
        Dim Frontskyddsräknare As Integer = 1

        Dim NyttSlutObjekt As RorelsevagarOchSkyddLib.SlutpunktsobjektA = New RorelsevagarOchSkyddLib.SlutpunktsobjektA

        For Each SlutPunkt As VToolsInmatningDataSet.SlutpunkterRow In SlutPunkter

            If (LastBisID <> SlutPunkt.BisID) = True Then
                LastBisID = SlutPunkt.BisID
                Frontskyddsräknare = 1
                NyttSlutObjekt = New RorelsevagarOchSkyddLib.SlutpunktsobjektA
                NyttSlutObjekt.SlutpunktsobjektId = New RorelsevagarOchSkyddLib.ObjektIdA
                NyttSlutObjekt.SlutpunktsobjektId.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA

                NyttSlutObjekt.SlutpunktsobjektId.BisObjektId.Objektnummer = SlutPunkt.BisObjektRow.BisID
                NyttSlutObjekt.SlutpunktsobjektId.BisObjektId.Objekttypnummer = SlutPunkt.BisObjektRow.typ

                NyttSlutObjekt.Frislappningshastighet = RorelsevagarOchSkyddLib.Enumerations.FrislappningshastighetA.Okänd

                '-- DynamisktFrontskydsomrade --
                NyttSlutObjekt.DynamisktFrontskydsomrade = False
                ' Måste lägga till huvud objektet vid samma tillfälle som det skapas

                SignalData.Slutpunkter.Add(NyttSlutObjekt)
            End If

            Dim NySlutPunkt As RorelsevagarOchSkyddLib.SlutpunktA = New RorelsevagarOchSkyddLib.SlutpunktA
            Select Case SlutPunkt.Typ
                Case "Tågväg"
                    NySlutPunkt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
                Case "Särskildtågväg"
                    NySlutPunkt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Särskildtågväg
                Case "Förenkladtågväg"
                    NySlutPunkt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Förenkladtågväg
                Case "Linje"
                    NySlutPunkt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Blocksträcka
                Case Else ' växlingsväg
                    NySlutPunkt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Växlingsväg
            End Select

            '******** Vi använder oss av FilterString istället för GetChild funktioner eftersom vi nu bara skall lägga till ett Frontskydd
            Dim FrontsRader() As VToolsInmatningDataSet.FrontSkyddRow = SlutPunkt.GetFrontSkyddRows
            For Each FrontRad As VToolsInmatningDataSet.FrontSkyddRow In FrontsRader
                NySlutPunkt.Skyddsavstand = FrontRad.Skyddsavstånd_ntv

                '-- skyddsträcka --
                NySlutPunkt.Skyddstracka = FrontRad.Skyddssträcka

                '-- Signalerad hastighet mot slutpunkt
                NySlutPunkt.SignaleradHastighetMotSlutpunkt = RorelsevagarOchSkyddLib.Enumerations.SignalbeskedMotSlutpunktA.Okänd
                Dim NyttFrontskydd As RorelsevagarOchSkyddLib.FrontskyddA = New RorelsevagarOchSkyddLib.FrontskyddA
                NyttFrontskydd.Id = Frontskyddsräknare
                Frontskyddsräknare += 1

                NySlutPunkt.Frontskydd.Add(NyttFrontskydd)
                FrontSkydden(NyttFrontskydd, FrontRad)
            Next

            'FilterString = "SlutpunktID='" & SlutPunkt.ID & "'"
            'ResultRows = VtoolsDataSet.FrontSkydd.Select(FilterString)
            'If ResultRows.Count > 0 Then
            '
            '           End If

            KonfliktRader = SlutPunkt.GetKonflikterRows
            For Each KonfliktRad As VToolsInmatningDataSet.KonflikterRow In KonfliktRader
                Konflikter(NySlutPunkt, KonfliktRad)
            Next

            NyttSlutObjekt.Slutpunkter.Add(NySlutPunkt)
        Next

    End Sub
#End Region

#Region "FrontSkydden"
    Public Sub FrontSkydden(ByRef FrontSkydd As RorelsevagarOchSkyddLib.FrontskyddA, AktivSlutPunkt As VToolsInmatningDataSet.FrontSkyddRow)
        Dim FrontObjects() As VToolsInmatningDataSet.FrontObjektRow
        Dim Räknare As Integer = 1
        FrontObjects = AktivSlutPunkt.GetFrontObjektRows
        For Each Row As DataRow In FrontObjects
            FrontSkydd.ObjektMedVillkor.Add(FrontSkyddsObjekt(Row, AktivSlutPunkt.Typ, True, Räknare))
        Next
        Dim ResultRows() As VToolsInmatningDataSet.FrontSkyddOmRow = AktivSlutPunkt.GetFrontSkyddOmRows
        For Each Row As DataRow In ResultRows
            FrontSkydd.ObjektMedVillkor.Add(FrontSkyddsObjekt(Row, AktivSlutPunkt.Typ, False, Räknare))
        Next
    End Sub
#End Region

#Region "FrontSkyddsObjekt"
    Public Function FrontSkyddsObjekt(AktivtFrontskydd As DataRow, tågvägstyp As String, FrontObjekt As Boolean, ByRef Räknare As Integer) As RorelsevagarOchSkyddLib.ObjektMedVillkorA
        Dim OmvilkorsObjekt As RorelsevagarOchSkyddLib.ObjektMedVillkorA = New RorelsevagarOchSkyddLib.ObjektMedVillkorA
        Dim FilterString As String = "ID='" & AktivtFrontskydd.Item("BisID") & "'"
        Dim ResultRows() As DataRow = VtoolsDataSet.BisObjekt.Select(FilterString)

        OmvilkorsObjekt.ObjektId = New RorelsevagarOchSkyddLib.ObjektIdA
        OmvilkorsObjekt.ObjektId.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
        OmvilkorsObjekt.ObjektId.BisObjektId.Objektnummer = ResultRows(0).Item("BisID")
        OmvilkorsObjekt.ObjektId.BisObjektId.Objekttypnummer = ResultRows(0).Item("typ")



        If FrontObjekt = True Then
            OmvilkorsObjekt.ArSkyddsObjekt = True
        Else
            OmvilkorsObjekt.ArSkyddsObjekt = False
        End If

        OmvilkorsObjekt.TypAvVillkor = RorelsevagarOchSkyddLib.Enumerations.TypAvVillkorA.Väglåsning

        Select Case tågvägstyp
            Case "Tågväg"
                OmvilkorsObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
            Case "Särskildtågväg"
                OmvilkorsObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Särskildtågväg
            Case "Förenkladtågväg"
                OmvilkorsObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Förenkladtågväg
            Case "Linje"
                OmvilkorsObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Blocksträcka
            Case Else ' växlingsväg
                OmvilkorsObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Växlingsväg
        End Select

        Select Case ResultRows(0).Item("typ")
            Case "6000", "10022", "10023", "6006"
                OmvilkorsObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Stopp
                'OmvilkorsObjekt.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Signal
            Case "10015", "10050"
                'OmvilkorsObjekt.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Spårspärr
                If StrComp(AktivtFrontskydd.Item("Riktning"), "P") = 0 Then
                    OmvilkorsObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.På
                    Console.WriteLine("Villkor Spårspärr 'På' ")
                Else
                    OmvilkorsObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Av
                    Console.WriteLine("Villkor Spårspärr 'Av' ")
                End If
            Case "3300", "3010", "3011"
                'OmvilkorsObjekt.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Växel
                Console.WriteLine("Objekttyp växel")


                'växelläge
                If StrComp(AktivtFrontskydd.Item("Riktning"), "H") = 0 Then
                    OmvilkorsObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Höger
                    Console.WriteLine("Kontrolläge höger.")
                Else
                    OmvilkorsObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Vänster
                    Console.WriteLine("Kontrolläge vänster.")
                End If
            Case "3310"
                'OmvilkorsObjekt.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Stoppbock
                OmvilkorsObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.På

            Case Else
                Console.WriteLine("----- Frontskydd. Objekttyp finns inte i programmet. Kontakta programmerare. Objettyp: " & ResultRows(0).Item("typ"))
                MessageBox.Show("Frontskydd. Objekttyp finns inte i programmet. Kontakta programmerare. Objettyp: " & ResultRows(0).Item("typ"))
        End Select

        Return OmvilkorsObjekt
    End Function
#End Region

#Region "Konflikter"
    Public Sub Konflikter(ByRef SlutPunkt As RorelsevagarOchSkyddLib.SlutpunktA, AktivKonflikt As VToolsInmatningDataSet.KonflikterRow)
        Dim NyFientligRörelse As RorelsevagarOchSkyddLib.FientligrorelseA = New RorelsevagarOchSkyddLib.FientligrorelseA


        NyFientligRörelse.RorelsevagId = AktivKonflikt.RörelsevägarRow.TRV_ID
        ' hårdkodas just nu kan komma att ändras senare
        NyFientligRörelse.Konflikt = RorelsevagarOchSkyddLib.Enumerations.TypAvKonfliktA.Gemensamtobjekt
        NyFientligRörelse.Rorelse = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelseA.Rörelseväg

        Dim OmEjRader() As VToolsInmatningDataSet.KonfliktOmEjRow = AktivKonflikt.GetKonfliktOmEjRows
        For Each OmEj As VToolsInmatningDataSet.KonfliktOmEjRow In OmEjRader
            NyFientligRörelse.OmEjVillkor.Add(AdderaOmEj(OmEj))
        Next
        SlutPunkt.Fientligarorelser.Add(NyFientligRörelse)
    End Sub
#End Region

#Region "AdderaOmEj"
    Public Function AdderaOmEj(ByRef AktivOmEj As VToolsInmatningDataSet.KonfliktOmEjRow) As RorelsevagarOchSkyddLib.OmEjVillkorA
        Dim NyOmEj As RorelsevagarOchSkyddLib.OmEjVillkorA = New RorelsevagarOchSkyddLib.OmEjVillkorA

        NyOmEj.ObjektId = New RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable
        NyOmEj.ObjektId.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
        NyOmEj.ObjektId.BisObjektId.Objektnummer = AktivOmEj.BisObjektRow.BisID


        NyOmEj.ObjektId.BisObjektId.Objekttypnummer = AktivOmEj.BisObjektRow.typ



        Select Case AktivOmEj.BisObjektRow.typ
            Case "6000", "10022", "10023", "6006"
                NyOmEj.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Stopp
                'OmvilkorsObjekt.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Signal
            Case "10015", "10050"
                'OmvilkorsObjekt.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Spårspärr
                If StrComp(AktivOmEj.Riktning, "P") = 0 Then
                    NyOmEj.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.På
                    Console.WriteLine("Villkor Spårspärr 'På' ")
                Else
                    NyOmEj.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Av
                    Console.WriteLine("Villkor Spårspärr 'Av' ")
                End If
            Case "3300", "3010", "3011"
                'OmvilkorsObjekt.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Växel
                Console.WriteLine("Objekttyp växel")

                'växelläge
                If StrComp(AktivOmEj.Riktning, "H") = 0 Then
                    NyOmEj.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Höger
                    Console.WriteLine("Kontrolläge höger.")
                Else
                    NyOmEj.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Vänster
                    Console.WriteLine("Kontrolläge vänster.")
                End If
            Case "3010"
                'OmvilkorsObjekt.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Stoppbock
                NyOmEj.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.På

            Case Else
                Console.WriteLine("----- KonfliktOmEj. Objekttyp finns inte i programmet. Kontakta programmerare. Objettyp: " & AktivOmEj.BisObjektRow.typ)
                MessageBox.Show("KonfliktOmEj. Objekttyp finns inte i programmet. Kontakta programmerare. Objettyp: " & AktivOmEj.BisObjektRow.typ)
        End Select

        Return NyOmEj
    End Function
#End Region

#Region "Error Handler"
    Private Sub DisplayError(ByVal ex As Exception)
        Dim errText As String = "Error - " & vbCrLf
        Do While (Not ex Is Nothing)
            errText = errText & ex.Message & vbCrLf
            ex = ex.InnerException
        Loop
        MessageBox.Show(errText, "An Error occurred.", MessageBoxButton.OK, MessageBoxImage.Error)
    End Sub
#End Region

#End Region
End Module





