﻿Public Class DeliveryXMLToTrv
    Private Sub btClose_Click(sender As Object, e As RoutedEventArgs)

        '---------------------------------------
        ' Microsoft Outlook 2013
        '--------------------------------------

        'bifoga leveransbrev format i Pfd
        If chbOutlookDeliveryLetter.IsChecked = True Then
            OutlookDeliveryLetterPDF = True
        Else
            OutlookDeliveryLetterPDF = False
        End If

        'bifoga XML filer i en zip fil
        If chbOutlookXMLFilesZip.IsChecked = True Then
            OutlookXMLDeliveryFilesZIP = True
        Else
            OutlookXMLDeliveryFilesZIP = False
        End If

        'bifoga XML filer som enskilda filer
        If chbOutlookXMLFiles.IsChecked = True Then
            OutlookXMLFiles = True
        Else
            OutlookXMLFiles = False
        End If

        'Skicka e-post direkt utan öppna Outlook
        If chbOutlookNewSend.IsChecked = True Then
            OutlookNewSend = True
        Else
            OutlookNewSend = False
        End If


        '---------------------------------------
        ' Sweco Portal mot externa leverans
        '--------------------------------------

        'bifoga leveransbrev format i Pfd
        If chbSwecoPortalDeliveryLetter.IsChecked = True Then
            SwecoPortalDeliveryLetterPDF = True
        Else
            SwecoPortalDeliveryLetterPDF = False
        End If

        'bifoga till Sweco Portal XML filer i format (zip)
        If chbSwecoPortalDeliveryFiles.IsChecked = True Then
            SwecoPortalDeliveryFilesZIP = True
        Else
            SwecoPortalDeliveryFilesZIP = False
        End If

        'stäng dialogruta
        Close()
    End Sub

    Private Sub Window_Loaded(sender As Object, e As RoutedEventArgs)
        grbSwecoPortalDeliveryLetter.Visibility = Visibility.Hidden
        chbOutlookNewSend.Visibility = Visibility.Hidden
    End Sub
End Class
