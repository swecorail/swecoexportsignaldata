﻿Imports System.IO
Imports Microsoft.Win32
Imports Microsoft.Office.Interop
Imports System.Data
Imports System.Data.OleDb
Imports Microsoft.Office.Interop.Excel
Imports System.Runtime.InteropServices
Imports System.IO.Compression
'Imports Microsoft.Office.Interop.Excel


Public Class ProjectExportBIS

#Region "Private data"
    Private AndaDSet As Object
    Private DriftplatsnummerExport As String
    'Private updatePbDelegate As New UpdateProgressBarDelegate(AddressOf ProgressBar1.SetValue)
    Dim valueProgressbarXMLExport As Double = 0 'värde i ProgressBar1

    Private barvalue As Double = 0 'stegökning i ProgressBar1
    Private Val_BandelDriftplats As String = "" 'val bandel,driftplatsnummer
    Private AvbrytXMLGenering As Boolean = False 'Avbryt direkt XML generering av leverans
    Private SwDataSet As SwTmovesDataSet
#End Region

#Region "Instans"

    'Create a Delegate that matches the Signature of the ProgressBar's SetValue method
    Private Delegate Sub UpdateProgressBarDelegate(ByVal dp As DependencyProperty,
                                                   ByVal value As Object)
#End Region

#Region "Skapa projektmappar"
    Private Sub CreateAllMapps()

        'create Defult project catalog ANDA leverans
        If Not ProjectBISCatalog_Root(ProjectDirectory & "\" & OnlyFileNameProject) = True Then
            Exit Sub
        End If

        'skapa mapp för leverans av XML-filer
        If Not ProjectBISCatalog_Root(ProjectDirectory & "\" & OnlyFileNameProject & "\" & Folder_XMLdelivery) = True Then
            Exit Sub
        End If

        'create Defult project catalog Design Basis
        If Not ProjectBISCatalog_Root(ProjectDirectory & "\" & OnlyFileNameProject & "\Projekteringsunderlag") = True Then
            Exit Sub
        End If

        'create Defult project catalog Quality Control
        If Not ProjectBISCatalog_Root(ProjectDirectory & "\" & OnlyFileNameProject & "\Kvalitetskontroll") = True Then
            Exit Sub
        End If

        'create Defult project catalog Summary Signal Data
        If Not ProjectBISCatalog_Root(ProjectDirectory & "\" & OnlyFileNameProject & "\Sammanställning SignalData") = True Then
            Exit Sub
        End If

    End Sub
#End Region

#Region "Startvariabler bandel"
    Private Sub StartvariableBandel()

        'Stations
        SwTmovesArrayStationsPost = 0
        SwTmovesPeekArrayStations = 0

        'Movements
        SwTmovesArrayMovementsPost = 0
        SwTmovesPeekArrayMovements = 0

        'EndObj
        SwTmovesArrayEndObjPost = 0 'antal poster
        SwTmovesPeekArrayEndObj = 0 'använder post nr

        'typ av rörelseväg
        SwTmovesArrayTypAvRorelsevag = 0 'antal poster
        SwTmovesPeekArrayTypAvRorelsevag = 0 'använder post nr

        'frontobjekt
        SwTmovesArrayTypAvFrontobjekt = 0 'antal poster
        SwTmovesPeekArrayTypAvFrontobjekt = 0 'använder post nr

        'Fientliga rörelser mot slutpunkt
        SwTmovesArrayFientligaRorelser = 0 'antal poster

        'Fientliga rörelser mot slutpunkt om ej växel
        SwTmovesArrayFientligaRorelserOmEj = 0 'antal poster

        'startrad i exceldomunent Kvalitetskontroll
        QualityControl_Counter = 1

    End Sub
#End Region

    Public Sub SkrivutVtoolsData(ByVal xmlfilename As String, ByRef DriftPlats As VToolsInmatningDataSet.DriftplatserRow, FolderPath As String)
        Try
            Dim rt As RorelsevagarOchSkyddLib.Signaldata = New RorelsevagarOchSkyddLib.Signaldata
            rt.Trafikplatssignatur = DriftPlats.plsign
            Dim VägRader() As VToolsInmatningDataSet.RörelsevägarRow = DriftPlats.GetRörelsevägarRows
            '--------------------------
            '    Rörelsevägar
            '--------------------------
            For Each VägRad As VToolsInmatningDataSet.RörelsevägarRow In VägRader
                Dim Spliväg As List(Of RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable) = New List(Of RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable)

                Dim RörelseVäg As RorelsevagarOchSkyddLib.RorelsevagA = RörelseBasInfo(VägRad, rt.Trafikplatssignatur)
                '--------------------------
                '    Positioner
                '--------------------------
                PositioneringsObjekt(RörelseVäg, VägRad, Spliväg)

                '--------------------------
                '    Upplåningar
                '--------------------------
                Select Case DriftPlats.stlvtyp
                    Case "stlv95", "cst", "stlv11"
                        UpplåsningspunkterStlv95(RörelseVäg, Spliväg)
                    Case Else
                        ' verkar som att både stlv85 och stlv65 har samma formel för upplåsningspunkter
                        UppåsningsPunkterStlv85(RörelseVäg, VägRad, FolderPath)
                End Select


                rt.Rorelsevagar.Add(RörelseVäg)
            Next

            '--------------------------
            '    SlutPunkt
            '--------------------------
            SlutPunktsInfo(rt, DriftPlats)

            rt.ToXmlFile(xmlfilename)
            'Dim VToolDataSet As VtoolsDatasetClass = New VtoolsDatasetClass()
            'VToolDataSet.XmlRead(InputFilepath)
        Catch ex As Exception
            DisplayError(ex, "Skriv ut XML-filer för vald bandel.")

        End Try
    End Sub

    Private Sub SlutPunktsInfo(ByRef SignalData As RorelsevagarOchSkyddLib.Signaldata, ByRef DriftPlats As VToolsInmatningDataSet.DriftplatserRow)
        ' Vi skall inte lägga till en rad för varje slutpunkt + typ utan bara för varje Slutpunkt
        ' Alltså fel här
        Dim FilterString As String = "plnr='" & DriftPlats.plnr & "'"
        Dim SortString As String = "BisID ASC"
        Dim LastBisID As Integer = -1
        Dim SlutPunkter() As DataRow = VtoolsDataSet.Slutpunkter.Select(FilterString, SortString)
        Dim KonfliktRader() As VToolsInmatningDataSet.KonflikterRow

        ' Dim ResultRows() As DataRow
        Dim ObjCreated As Boolean = False
        Dim Frontskyddsräknare As Integer = 1

        Dim NyttSlutObjekt As RorelsevagarOchSkyddLib.SlutpunktsobjektA = New RorelsevagarOchSkyddLib.SlutpunktsobjektA

        For Each SlutPunkt As VToolsInmatningDataSet.SlutpunkterRow In SlutPunkter

            If (LastBisID <> SlutPunkt.BisID) = True Then
                LastBisID = SlutPunkt.BisID
                Frontskyddsräknare = 1
                NyttSlutObjekt = New RorelsevagarOchSkyddLib.SlutpunktsobjektA
                NyttSlutObjekt.SlutpunktsobjektId = New RorelsevagarOchSkyddLib.ObjektIdA
                NyttSlutObjekt.SlutpunktsobjektId.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA

                NyttSlutObjekt.SlutpunktsobjektId.BisObjektId.Objektnummer = SlutPunkt.BisObjektRow.BisID
                NyttSlutObjekt.SlutpunktsobjektId.BisObjektId.Objekttypnummer = SlutPunkt.BisObjektRow.typ

                NyttSlutObjekt.Frislappningshastighet = RorelsevagarOchSkyddLib.Enumerations.FrislappningshastighetA.Okänd

                '-- DynamisktFrontskydsomrade --
                NyttSlutObjekt.DynamisktFrontskydsomrade = False
                ' Måste lägga till huvud objektet vid samma tillfälle som det skapas

                SignalData.Slutpunkter.Add(NyttSlutObjekt)
            End If

            Dim NySlutPunkt As RorelsevagarOchSkyddLib.SlutpunktA = New RorelsevagarOchSkyddLib.SlutpunktA
            Select Case SlutPunkt.Typ
                Case "Tågväg"
                    NySlutPunkt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
                Case "Särskildtågväg"
                    NySlutPunkt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Särskildtågväg
                Case "Förenkladtågväg"
                    NySlutPunkt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Förenkladtågväg
                Case "Linje"
                    NySlutPunkt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Blocksträcka
                Case Else ' växlingsväg
                    NySlutPunkt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Växlingsväg
            End Select

            '******** Vi använder oss av FilterString istället för GetChild funktioner eftersom vi nu bara skall lägga till ett Frontskydd
            Dim FrontsRader() As VToolsInmatningDataSet.FrontSkyddRow = SlutPunkt.GetFrontSkyddRows
            For Each FrontRad As VToolsInmatningDataSet.FrontSkyddRow In FrontsRader
                NySlutPunkt.Skyddsavstand = FrontRad.Skyddsavstånd_ntv

                '-- skyddsträcka --
                NySlutPunkt.Skyddstracka = FrontRad.Skyddssträcka

                '-- Signalerad hastighet mot slutpunkt
                NySlutPunkt.SignaleradHastighetMotSlutpunkt = RorelsevagarOchSkyddLib.Enumerations.SignalbeskedMotSlutpunktA.Okänd
                Dim NyttFrontskydd As RorelsevagarOchSkyddLib.FrontskyddA = New RorelsevagarOchSkyddLib.FrontskyddA
                NyttFrontskydd.Id = Frontskyddsräknare
                Frontskyddsräknare += 1

                NySlutPunkt.Frontskydd.Add(NyttFrontskydd)
                FrontSkydden(NyttFrontskydd, FrontRad)
            Next

            'FilterString = "SlutpunktID='" & SlutPunkt.ID & "'"
            'ResultRows = VtoolsDataSet.FrontSkydd.Select(FilterString)
            'If ResultRows.Count > 0 Then
            '
            '           End If

            KonfliktRader = SlutPunkt.GetKonflikterRows
            For Each KonfliktRad As VToolsInmatningDataSet.KonflikterRow In KonfliktRader
                Konflikter(NySlutPunkt, KonfliktRad)
            Next

            NyttSlutObjekt.Slutpunkter.Add(NySlutPunkt)
        Next

    End Sub

    Private Sub FrontSkydden(ByRef FrontSkydd As RorelsevagarOchSkyddLib.FrontskyddA, AktivSlutPunkt As VToolsInmatningDataSet.FrontSkyddRow)
        Dim FrontObjects() As VToolsInmatningDataSet.FrontObjektRow
        Dim Räknare As Integer = 1
        FrontObjects = AktivSlutPunkt.GetFrontObjektRows
        For Each Row As DataRow In FrontObjects
            FrontSkydd.ObjektMedVillkor.Add(FrontSkyddsObjekt(Row, AktivSlutPunkt.Typ, True, Räknare))
        Next
        Dim ResultRows() As VToolsInmatningDataSet.FrontSkyddOmRow = AktivSlutPunkt.GetFrontSkyddOmRows
        For Each Row As DataRow In ResultRows
            FrontSkydd.ObjektMedVillkor.Add(FrontSkyddsObjekt(Row, AktivSlutPunkt.Typ, False, Räknare))
        Next
    End Sub

    Private Function FrontSkyddsObjekt(AktivtFrontskydd As DataRow, tågvägstyp As String, FrontObjekt As Boolean, ByRef Räknare As Integer) As RorelsevagarOchSkyddLib.ObjektMedVillkorA
        Dim OmvilkorsObjekt As RorelsevagarOchSkyddLib.ObjektMedVillkorA = New RorelsevagarOchSkyddLib.ObjektMedVillkorA
        Dim FilterString As String = "ID='" & AktivtFrontskydd.Item("BisID") & "'"
        Dim ResultRows() As DataRow = VtoolsDataSet.BisObjekt.Select(FilterString)

        OmvilkorsObjekt.ObjektId = New RorelsevagarOchSkyddLib.ObjektIdA
        OmvilkorsObjekt.ObjektId.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
        OmvilkorsObjekt.ObjektId.BisObjektId.Objektnummer = ResultRows(0).Item("BisID")
        OmvilkorsObjekt.ObjektId.BisObjektId.Objekttypnummer = ResultRows(0).Item("typ")



        If FrontObjekt = True Then
            OmvilkorsObjekt.ArSkyddsObjekt = True
        Else
            OmvilkorsObjekt.ArSkyddsObjekt = False
        End If

        OmvilkorsObjekt.TypAvVillkor = RorelsevagarOchSkyddLib.Enumerations.TypAvVillkorA.Väglåsning

        Select Case tågvägstyp
            Case "Tågväg"
                OmvilkorsObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
            Case "Särskildtågväg"
                OmvilkorsObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Särskildtågväg
            Case "Förenkladtågväg"
                OmvilkorsObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Förenkladtågväg
            Case "Linje"
                OmvilkorsObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Blocksträcka
            Case Else ' växlingsväg
                OmvilkorsObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Växlingsväg
        End Select

        Select Case ResultRows(0).Item("typ")
            Case "6000", "10022", "10023", "6006"
                OmvilkorsObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Stopp
                'OmvilkorsObjekt.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Signal
            Case "10015", "10050"
                'OmvilkorsObjekt.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Spårspärr
                'If StrComp(AktivtFrontskydd.Item("Riktning"), "P") = 0 Then
                OmvilkorsObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.På
                Console.WriteLine("Villkor Spårspärr 'På' ")
                'Else
                'OmvilkorsObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Av
                'Console.WriteLine("Villkor Spårspärr 'Av' ")
                'End If
            Case "3300", "3010", "3011"
                'OmvilkorsObjekt.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Växel
                Console.WriteLine("Objekttyp växel")


                'växelläge
                If StrComp(AktivtFrontskydd.Item("Riktning"), "H") = 0 Then
                    OmvilkorsObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Höger
                    Console.WriteLine("Kontrolläge höger.")
                Else
                    OmvilkorsObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Vänster
                    Console.WriteLine("Kontrolläge vänster.")
                End If
            Case "3310"
                'OmvilkorsObjekt.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Stoppbock
                OmvilkorsObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.På

            Case Else
                Console.WriteLine("----- Frontskydd. Objekttyp finns inte i programmet. Kontakta programmerare. Objettyp: " & ResultRows(0).Item("typ"))
                MessageBox.Show("Frontskydd. Objekttyp finns inte i programmet. Kontakta programmerare. Objettyp: " & ResultRows(0).Item("typ"))
        End Select

        Return OmvilkorsObjekt
    End Function

    Private Sub UpplåsningspunkterStlv95(ByRef RörelseVäg As RorelsevagarOchSkyddLib.RorelsevagA, ByRef SpliVäg As List(Of RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable))
        Dim i As Integer = 0

        For J As Integer = 1 To SpliVäg.Count - 1
            Dim NyUpplåsning As RorelsevagarOchSkyddLib.UpplasningspunktA = New RorelsevagarOchSkyddLib.UpplasningspunktA
            NyUpplåsning.Borjanpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
            NyUpplåsning.Borjanpunkt.BisObjektId.Objektnummer = SpliVäg(i).BisObjektId.Objektnummer
            NyUpplåsning.Borjanpunkt.BisObjektId.Objekttypnummer = SpliVäg(i).BisObjektId.Objekttypnummer

            NyUpplåsning.Slutpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
            NyUpplåsning.Slutpunkt.BisObjektId.Objektnummer = SpliVäg(J).BisObjektId.Objektnummer
            NyUpplåsning.Slutpunkt.BisObjektId.Objekttypnummer = SpliVäg(J).BisObjektId.Objekttypnummer
            'NyUpplåsning.Borjanpunkt = SpliVäg(i)
            'NyUpplåsning.Slutpunkt = SpliVäg(J)



            Select Case SpliVäg(i).BisObjektId.Objekttypnummer
                Case "10019"
                    NyUpplåsning.TypAvUpplasningspunkt = RorelsevagarOchSkyddLib.Enumerations.TypAvUpplasningspunktA.Isolskarv
                Case "6000", "10023"
                    NyUpplåsning.TypAvUpplasningspunkt = RorelsevagarOchSkyddLib.Enumerations.TypAvUpplasningspunktA.Signal
                Case "6006"
                    NyUpplåsning.TypAvUpplasningspunkt = RorelsevagarOchSkyddLib.Enumerations.TypAvUpplasningspunktA.Tavla
            End Select
            NyUpplåsning.Sekvensnummer = J
            i += 1
            RörelseVäg.Upplasningspunkter.Add(NyUpplåsning)
        Next

    End Sub

    Private Sub UppåsningsPunkterStlv85(ByRef RörelseVäg As RorelsevagarOchSkyddLib.RorelsevagA, Väg As VToolsInmatningDataSet.RörelsevägarRow, FolderPath As String)
        Dim Vägdelar() As VToolsInmatningDataSet.VägdelariRörelsevägarRow = Väg.GetVägdelariRörelsevägarRows
        Dim J As Integer = 1
        Dim i As Integer = 1
        For Each VägDel As VToolsInmatningDataSet.VägdelariRörelsevägarRow In Vägdelar
            Dim FilterString As String = "BisID='" & VägDel.VägdelarRow.SlutPunktID & "' AND VägdelsID='" & VägDel.VägdelsID & "'"

            Dim ObjektRader() As VToolsInmatningDataSet.MotriktadeObjektRow = VtoolsDataSet.MotriktadeObjekt.Select(FilterString)

            For Each ObjektRad As VToolsInmatningDataSet.MotriktadeObjektRow In ObjektRader
                Dim Viableobject As Boolean = True
                ' Specialregler för första vägdelen 
                ' för att hitta rygg i rygg signaler
                FilterString = "ID='" & VägDel.VägdelarRow.BörjanPunktID & "'"
                Dim pBRader() As VToolsInmatningDataSet.BisObjektRow = VtoolsDataSet.BisObjekt.Select(FilterString)
                If i = 1 Then
                    FilterString = "Obj1BisID='" & ObjektRad.BisObjektRow.BisID & "'"
                    Dim ResultRows() As DataRow = VtoolsDataSet.BacktoBack.Select(FilterString)
                    ' Om den finns med som rygg i rygg så är det troligt att jag skall hoppa över att lägga till den 
                    If ResultRows.Count > 0 Then
                        ' kolla om det är startpunkten som är dess rygg-i-rygg
                        For Each Row As DataRow In ResultRows
                            If StrComp(Row.Item("Obj2BisID"), pBRader(0).BisID) = 0 Then
                                Viableobject = False
                            End If
                        Next


                    Else
                        ' Objektet fanns inte med i Obj1 prova Obj2
                        FilterString = "Obj2BisID='" & ObjektRad.BisObjektRow.BisID & "'"
                        ResultRows = VtoolsDataSet.BacktoBack.Select(FilterString)
                        If ResultRows.Count > 0 Then
                            For Each Row As DataRow In ResultRows
                                If StrComp(Row.Item("Obj1BisID"), pBRader(0).BisID) = 0 Then
                                    Viableobject = False
                                End If
                            Next
                        End If
                    End If
                End If

                If Viableobject = True Then
                    Dim NyUpplåsning As RorelsevagarOchSkyddLib.UpplasningspunktA = New RorelsevagarOchSkyddLib.UpplasningspunktA

                    Dim AllPointsExists As Boolean = True
                    FilterString = "BisID='" & ObjektRad.BisID & "'"
                    Dim SecondFilter As String
                    Dim ResultRows() As DataRow = VtoolsDataSet.Splobjekt.Select(FilterString)

                    If ResultRows.Count > 0 Then
                        FilterString = "ID='" & ResultRows(0).Item("SplID") & "'"
                        If IsDBNull(ResultRows(0).Item("T1")) = False Then
                            SecondFilter = "ID='" & ResultRows(0).Item("T1") & "'"
                        Else
                            AllPointsExists = False
                            SecondFilter = ""
                        End If

                        ResultRows = VtoolsDataSet.BisObjekt.Select(FilterString)
                        If ResultRows.Count > 0 Then
                            NyUpplåsning.Borjanpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                            NyUpplåsning.Borjanpunkt.BisObjektId.Objektnummer = ResultRows(0).Item("BisID")
                            ' det kan bara vara signaler här
                            NyUpplåsning.Borjanpunkt.BisObjektId.Objekttypnummer = ResultRows(0).Item("typ")

                            Select Case ResultRows(0).Item("typ")
                                Case "10019"
                                    NyUpplåsning.TypAvUpplasningspunkt = RorelsevagarOchSkyddLib.Enumerations.TypAvUpplasningspunktA.Isolskarv
                                Case "6000", "10023"
                                    NyUpplåsning.TypAvUpplasningspunkt = RorelsevagarOchSkyddLib.Enumerations.TypAvUpplasningspunktA.Signal
                            End Select
                        End If
                        If AllPointsExists = True Then
                            ResultRows = VtoolsDataSet.BisObjekt.Select(SecondFilter)
                            If ResultRows.Count > 0 Then
                                NyUpplåsning.Slutpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                                NyUpplåsning.Slutpunkt.BisObjektId.Objektnummer = ResultRows(0).Item("BisID")
                                NyUpplåsning.Slutpunkt.BisObjektId.Objekttypnummer = ResultRows(0).Item("typ")
                            End If
                        Else
                            Select Case ResultRows(0).Item("typ")
                                Case "3300", "3010", "3011", "3310, ""10015", "10050"
                                    ' dessa har inte någon T1
                                Case Else
                                    Dim tmp As String = "RörelseVäg " & RörelseVäg.Id & " saknar T1 information på objekt: typ " & ResultRows(0).Item("typ") & ", BisID " & ResultRows(0).Item("BisID") & " i upplåsnings information"
                                    ImportCVSFile.MakeLogUpdate(FolderPath & "\Bortrensade rörelser vid körning " & System.DateTime.Today & ".txt", tmp)
                            End Select

                        End If

                    End If

                    NyUpplåsning.Sekvensnummer = J
                    RörelseVäg.Upplasningspunkter.Add(NyUpplåsning)
                    J += 1
                End If
            Next
            i += 1
        Next
    End Sub

    Private Function RörelseBasInfo(ByRef DataR As VToolsInmatningDataSet.RörelsevägarRow, KortNamn As String) As RorelsevagarOchSkyddLib.RorelsevagA
        Dim Väg As RorelsevagarOchSkyddLib.RorelsevagA = New RorelsevagarOchSkyddLib.RorelsevagA
        Dim ResultRows() As DataRow
        Dim FilterString As String
        Dim slutpunkt As String = DataR.Beteckning
        Dim StartPunkt As String = FindInString(slutpunkt, ".")
        slutpunkt = FindInString(slutpunkt, " ")
        Väg.Id = DataR.TRV_ID


        Dim TmpString As String = DataR.Beteckning
        Dim StationName As String = ""
        If InStr(TmpString, "@") > 0 Then
            Dim Firstpart As String = FindInString(TmpString, " ")
            StationName = TmpString
            TmpString = Firstpart
            While InStr(TmpString, "@") > 0
                Firstpart = FindInString(TmpString, "@")
                Dim Secondpart As String = FindInString(TmpString, "-")
                If TmpString.Length > 1 Then
                    TmpString = Firstpart + "-" + TmpString
                Else
                    TmpString = Firstpart
                End If
            End While

            TmpString = TmpString + " " + StationName
        End If


        Väg.Namn = TmpString
        Select Case DataR.Item("Typ")
            Case "Tågväg"
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
                'Väg.Namn = "tv-" & StartPunkt & "-" & slutpunkt & " " & KortNamn
            Case "Växlingsväg"
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Växlingsväg
            Case "ftv"
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Förenkladtågväg
            Case "stv"
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Särskildtågväg
            Case "Linje"
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Blocksträcka
            Case "tam"
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.TAMsträcka
            Case "btv"
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Bevakadtågväg
            Case "ntv"
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
            Case Else
                Väg.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
                MessageBox.Show("Rörelsevägar. Typ av rörelseväg ej implementerad i Vtools. Kontakta programmerare. Rörelsetyp:  " & Rörelsevägstyp)
                Me.Close()
        End Select

        Väg.Borjanpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
        FilterString = "ID='" & DataR.BörjanPunktID & "'"
        ResultRows = VtoolsDataSet.BisObjekt.Select(FilterString)

        Väg.Borjanpunkt.BisObjektId.Objektnummer = DataR.BisObjektRow.BisID
        Väg.Borjanpunkt.BisObjektId.Objekttypnummer = DataR.BisObjektRow.typ

        FilterString = "ID='" & DataR.SlutPunktID & "'"
        ResultRows = VtoolsDataSet.BisObjekt.Select(FilterString)

        '-- BIS objekt slutpunkt --
        Väg.Slutpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
        'Måste få tillgång till Korrekt Vtools export

        Väg.Slutpunkt.BisObjektId.Objektnummer = DataR.SlutpunkterRow.BisObjektRow.BisID
        Väg.Slutpunkt.BisObjektId.Objekttypnummer = DataR.SlutpunkterRow.BisObjektRow.typ

        Return Väg
    End Function

    Private Sub PositioneringsObjekt(ByRef Rorelsevag As RorelsevagarOchSkyddLib.RorelsevagA, ByVal RoadRow As VToolsInmatningDataSet.RörelsevägarRow,
                                     ByRef SpliVäg As List(Of RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable))
        Dim FilterString As String
        Dim ResultRows() As DataRow
        Try
            Dim VagDelar() As VToolsInmatningDataSet.VägdelariRörelsevägarRow = RoadRow.GetVägdelariRörelsevägarRows

            Dim SekvensNr As Integer = 1
            For Each VagDel As VToolsInmatningDataSet.VägdelariRörelsevägarRow In VagDelar
                Dim VagDelObjs() As VToolsInmatningDataSet.SpårledningarRow
                Dim RoadPart As VToolsInmatningDataSet.VägdelarRow = VagDel.VägdelarRow 'VtoolsDataSet.Vägdelar.Select(FilterString)

                VagDelObjs = RoadPart.GetSpårledningarRows
                For Each _ObjRow As VToolsInmatningDataSet.SpårledningarRow In VagDelObjs
                    Dim NewRoadPart As RorelsevagarOchSkyddLib.PositionA = New RorelsevagarOchSkyddLib.PositionA
                    Dim ObjektIVägRader() As VToolsInmatningDataSet.SplobjektRow = _ObjRow.BisObjektRow.GetSplobjektRows
                    NewRoadPart.Sekvensnummer = SekvensNr
                    NewRoadPart.PositioneringsobjektId = Spl(_ObjRow)
                    If ObjektIVägRader.Count > 0 Then
                        Dim ObjektArray As RorelsevagarOchSkyddLib.ArrayOfObjektMedVillkor = New RorelsevagarOchSkyddLib.ArrayOfObjektMedVillkor
                        For Each ObjektRad As VToolsInmatningDataSet.SplobjektRow In ObjektIVägRader
                            Dim NyttObjekt As RorelsevagarOchSkyddLib.ObjektMedVillkor_Nillable = New RorelsevagarOchSkyddLib.ObjektMedVillkor_Nillable
                            FilterString = "ID='" & ObjektRad.BisID & "'"
                            ResultRows = VtoolsDataSet.BisObjekt.Select(FilterString)

                            If StrComp(ResultRows(0).Item("typ"), "3310") <> 0 Then
                                ' 3310 är stoppbock kan inte vara en del av en rörelseväg
                                NyttObjekt.ObjektId = New RorelsevagarOchSkyddLib.ObjektIdA
                                NyttObjekt.ObjektId.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                                NyttObjekt.ObjektId.BisObjektId.Objektnummer = ResultRows(0).Item("BisID")
                                NyttObjekt.ObjektId.BisObjektId.Objekttypnummer = ResultRows(0).Item("typ")
                                NyttObjekt.ArSkyddsObjekt = False

                                Select Case RoadPart.Typ
                                    Case "Tågväg"
                                        NyttObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
                                    Case "Särskildtågväg"
                                        NyttObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Särskildtågväg
                                    Case "Förenkladtågväg"
                                        NyttObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Förenkladtågväg
                                    Case "Linje"
                                        NyttObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Blocksträcka
                                    Case Else ' växlingsväg
                                        NyttObjekt.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Växlingsväg
                                End Select

                                NyttObjekt.TypAvVillkor = RorelsevagarOchSkyddLib.Enumerations.TypAvVillkorA.Väglåsning

                                FilterString = "VägdelsID='" & RoadPart.ID & "' AND BisID='" & ResultRows(0).Item("ID") & "'"
                                ResultRows = VtoolsDataSet.Låsningar.Select(FilterString)
                                Dim Riktning As String
                                If ResultRows.Count > 0 Then
                                    ' objektet fanns bland låsnignar
                                    Riktning = ResultRows(0).Item("Riktning")
                                Else
                                    ' fanns inte i låsningar kolla om det är en motriktad signal
                                    ResultRows = VtoolsDataSet.MotriktadeObjekt.Select(FilterString)
                                    If ResultRows.Count > 0 Then
                                        If StrComp(ResultRows(0).Item("Riktning"), "-") = 0 Then
                                            ' det är en motriktad signal och då skall den stå i stop 
                                            Riktning = "S"
                                        Else
                                            Riktning = ResultRows(0).Item("Riktning")
                                        End If
                                    Else
                                        ' fanns inte som en motriktad signal då är det en medriktad signal som är en mellan punkt i vtools
                                        ' kan vara så att det är ett objekt som inte passeras 
                                        ResultRows = VtoolsDataSet.ObjektiVäg.Select(FilterString)
                                        If ResultRows.Count > 0 Then
                                            Riktning = ResultRows(0).Item("Riktning")
                                        Else

                                            Riktning = ""
                                        End If

                                    End If

                                End If
                                If Riktning.Length > 0 Then
                                    Select Case Riktning
                                        Case "-"
                                            ' medriktad signal bör visa kör
                                            NyttObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Inget
                                        Case "V"
                                            NyttObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Vänster
                                        Case "H"
                                            NyttObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Höger
                                        Case "P"
                                            NyttObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.På
                                        Case "S"
                                            NyttObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Stopp
                                        Case Else
                                            NyttObjekt.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Av
                                    End Select

                                    ObjektArray.ObjektMedVillkor.Add(NyttObjekt)
                                End If
                            End If
                        Next
                        NewRoadPart.ObjektMedVillkor = ObjektArray
                    End If

                    SpliVäg.Add(Spl(_ObjRow))
                    Rorelsevag.Positioner.Add(NewRoadPart)
                    SekvensNr += 1
                Next
            Next
        Catch ex As Exception
            'MsgBox("FilterString = " & FilterString & "ResultRow Count: " & ResultRows.Count)
        End Try

    End Sub

    Private Function Spl(ByRef TrackRow As VToolsInmatningDataSet.SpårledningarRow) As RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable
        Dim _oSpl As RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable = New RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable

        Dim BisObj As RorelsevagarOchSkyddLib.BisObjektIdA = New RorelsevagarOchSkyddLib.BisObjektIdA
        BisObj.Objektnummer = TrackRow.BisObjektRow.BisID
        BisObj.Objekttypnummer = TrackRow.BisObjektRow.typ
        _oSpl.BisObjektId = BisObj
        Return _oSpl
    End Function

    Private Sub Konflikter(ByRef SlutPunkt As RorelsevagarOchSkyddLib.SlutpunktA, AktivKonflikt As VToolsInmatningDataSet.KonflikterRow)
        Dim NyFientligRörelse As RorelsevagarOchSkyddLib.FientligrorelseA = New RorelsevagarOchSkyddLib.FientligrorelseA


        NyFientligRörelse.RorelsevagId = AktivKonflikt.RörelsevägarRow.TRV_ID
        ' hårdkodas just nu kan komma att ändras senare
        NyFientligRörelse.Konflikt = RorelsevagarOchSkyddLib.Enumerations.TypAvKonfliktA.Gemensamtobjekt
        NyFientligRörelse.Rorelse = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelseA.Rörelseväg

        Dim OmEjRader() As VToolsInmatningDataSet.KonfliktOmEjRow = AktivKonflikt.GetKonfliktOmEjRows
        For Each OmEj As VToolsInmatningDataSet.KonfliktOmEjRow In OmEjRader
            NyFientligRörelse.OmEjVillkor.Add(AdderaOmEj(OmEj))
        Next
        SlutPunkt.Fientligarorelser.Add(NyFientligRörelse)
    End Sub

    Private Function AdderaOmEj(ByRef AktivOmEj As VToolsInmatningDataSet.KonfliktOmEjRow) As RorelsevagarOchSkyddLib.OmEjVillkorA
        Dim NyOmEj As RorelsevagarOchSkyddLib.OmEjVillkorA = New RorelsevagarOchSkyddLib.OmEjVillkorA

        NyOmEj.ObjektId = New RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable
        NyOmEj.ObjektId.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
        NyOmEj.ObjektId.BisObjektId.Objektnummer = AktivOmEj.BisObjektRow.BisID


        NyOmEj.ObjektId.BisObjektId.Objekttypnummer = AktivOmEj.BisObjektRow.typ

        Select Case AktivOmEj.BisObjektRow.typ
            Case "6000", "10022", "10023", "6006"
                NyOmEj.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Stopp
                'OmvilkorsObjekt.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Signal
            Case "10015", "10050"
                'OmvilkorsObjekt.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Spårspärr
                If StrComp(AktivOmEj.Riktning, "P") = 0 Then
                    NyOmEj.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.På
                    Console.WriteLine("Villkor Spårspärr 'På' ")
                Else
                    NyOmEj.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Av
                    Console.WriteLine("Villkor Spårspärr 'Av' ")
                End If
            Case "3300", "3010", "3011"
                'OmvilkorsObjekt.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Växel
                Console.WriteLine("Objekttyp växel")

                'växelläge
                If StrComp(AktivOmEj.Riktning, "H") = 0 Then
                    NyOmEj.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Höger
                    Console.WriteLine("Kontrolläge höger.")
                Else
                    NyOmEj.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Vänster
                    Console.WriteLine("Kontrolläge vänster.")
                End If
            Case "3010"
                'OmvilkorsObjekt.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Stoppbock
                NyOmEj.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.På

            Case Else
                Console.WriteLine("----- KonfliktOmEj. Objekttyp finns inte i programmet. Kontakta programmerare. Objettyp: " & AktivOmEj.BisObjektRow.typ)
                MessageBox.Show("KonfliktOmEj. Objekttyp finns inte i programmet. Kontakta programmerare. Objettyp: " & AktivOmEj.BisObjektRow.typ)
        End Select

        Return NyOmEj
    End Function

#Region "XML-fil från SwTmoves till BIS. Enligt ANDA 1.0"
    Public Sub SkrivutSwTmoves(ByVal xmlfilename As String, ByVal arraynr As Integer, ByVal driftplatsnummer As Integer)
        Dim textinfo As String = ""
        Dim felmedelande As String = ""

        SwDataSet = New SwTmovesDataSet

        Try
            FillSwTmovesDataSet(SwDataSet, driftplatsnummer)


            SwTmovesPeekArrayMovements = 0
            StationIDFiktiv = driftplatsnummer

            Console.WriteLine("")
            Console.WriteLine("----  Driftplatsnummer som ska utvärderas (" & StationIDFiktiv & ") ----")
            Console.WriteLine("")

            '---------------------
            '  Signaldata 
            '--------------------
            Dim rt As RorelsevagarOchSkyddLib.SignaldataA = New RorelsevagarOchSkyddLib.SignaldataA

            'signaldata
            rt.Trafikplatssignatur = Trafikplatssignatur

            '-------------------------------
            '   Rörelseväg
            '-------------------------------

            '-- Grupp Rörelsevägar --
            For counterMovements = 1 To SwTmovesArrayMovementsPost

                'Typ av upplåsningspunkt för att hitta isolskarv
                Dim signal As Boolean = False 'signal
                Dim upplasningspunktSekvensnummer As Integer = 1

                'create array rörelseväg börjanpunkt/slutpunkt/upplåsning börjanpunkt. 
                ObjektIDBpSpSwTmoves(SwTmovesPeekArrayMovements) 'Börjapunkt, slutpunkt, upplåsningspunkt
                StationFKStObjStIDSwTmoves(SwTmovesPeekArrayMovements) 'Fiktiv stations ID

                'BIS data börjanpunktsobjekt
                ReadSignalObjectsBPSwTmoves(ProjectSwTmoves_Database) 'börjanpunkt

                'BIS data slutpunktsobjekt
                ReadSignalObjectsSPSwTmoves(ProjectSwTmoves_Database) 'slutpunkt

                '---------------------------
                '     Rörelsevägar
                '---------------------------
                Dim rorelsevagA As RorelsevagarOchSkyddLib.RorelsevagA = New RorelsevagarOchSkyddLib.RorelsevagA
                rt.Rorelsevagar.Add(rorelsevagA)

                '-- id  --
                rorelsevagA.Id = IDMoments(SwTmovesPeekArrayMovements) 'Unik ID rörelseväg



                'signal
                Dim LSignal As String
                LSignal = BpBisObjektNamnRv

                'kontroll om det är en blocksträcka
                Dim blockstracka As Char
                blockstracka = LSignal.Substring(0, 1)

                'skapa fiktiv Rörelsevägstyp för att hantera projekterad rörelse på linje
                'system H
                Dim FiktivRörelsevägstyp As String = Rörelsevägstyp

                If blockstracka = "L" Or blockstracka = "N" Or blockstracka = "U" Then

                    Select Case FiktivRörelsevägstyp
                        Case "vv", "ftv", "stv", "lib", "tam", "btv"
                            Exit Select

                        Case Else
                            If Not BorjanpunktBisObjectType = "ERTMSTavla" Then
                                FiktivRörelsevägstyp = "lib"
                            End If
                    End Select
                End If

                '-- Typ --
                Select Case FiktivRörelsevägstyp
                    Case "tv"
                        rorelsevagA.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
                    Case "vv"
                        rorelsevagA.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Växlingsväg
                    Case "ftv"
                        rorelsevagA.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Förenkladtågväg
                    Case "stv"
                        rorelsevagA.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Särskildtågväg
                    Case "lib"
                        rorelsevagA.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Blocksträcka
                    Case "tam"
                        rorelsevagA.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.TAMsträcka
                    Case "btv"
                        rorelsevagA.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Bevakadtågväg
                    Case "ntv"
                        rorelsevagA.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
                    Case Else
                        rorelsevagA.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
                        MessageBox.Show("Rörelsevägar. Typ av rörelseväg ej implementerad i SwTmoves. Kontakta programmerare. Rörelsetyp: " & Rörelsevägstyp)
                        Me.Close()
                End Select

                Dim FilterString As String
                Dim ResultRows() As DataRow
                Dim bp_orgNamn As String
                Dim sp_orgNamn As String

                '-- BIS objekt börjanpunkt --
                rorelsevagA.Borjanpunkt = New RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable
                rorelsevagA.Borjanpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                rorelsevagA.Borjanpunkt.BisObjektId.Objektnummer = BpBisObjektObjektnummerRv
                rorelsevagA.Borjanpunkt.BisObjektId.Objekttypnummer = BpBisObjekttypnummerRv
                rorelsevagA.Borjanpunkt.IsNil = False

                FilterString = "BisID='" & BpBisObjektObjektnummerRv & "' AND O_typ='" & BpBisObjekttypnummerRv & "'"
                ResultRows = OrginalNametable.Select(FilterString)
                If ResultRows.Count > 0 Then
                    bp_orgNamn = ResultRows(0).Item("O_Name")
                Else
                    bp_orgNamn = BpBisObjektNamnRv

                    Select Case MsgBox("BisID för si " & BpBisObjektNamnRv.ToString & " saknas i Orginalnamntabellen,  " &
                                       " Vill du fortsätta genereringen ?", MsgBoxStyle.YesNo)

                        Case MsgBoxResult.Yes
                            'empty

                        Case MsgBoxResult.No
                            AvbrytXMLGenering = True
                            Exit Sub
                    End Select
                End If

                '-- BIS objekt slutpunkt --
                rorelsevagA.Slutpunkt = New RorelsevagarOchSkyddLib.PositioneringsobjektId_Nillable
                rorelsevagA.Slutpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                rorelsevagA.Slutpunkt.BisObjektId.Objektnummer = SpBisObjektObjektnummerRv
                rorelsevagA.Slutpunkt.BisObjektId.Objekttypnummer = SpBisObjekttypnummerRv
                rorelsevagA.Slutpunkt.IsNil = False

                FilterString = "BisID='" & SpBisObjektObjektnummerRv & "' AND O_typ='" & SpBisObjekttypnummerRv & "'"
                ResultRows = OrginalNametable.Select(FilterString)
                If ResultRows.Count > 0 Then
                    sp_orgNamn = ResultRows(0).Item("O_Name")
                Else
                    sp_orgNamn = SpBisObjektNamnRv

                    Select Case MsgBox("BisID för si " & SpBisObjektNamnRv.ToString & " saknas i Orginalnamntabellen,  " &
                                       " Vill du fortsätta genereringen ?", MsgBoxStyle.YesNo)

                        Case MsgBoxResult.Yes
                            'empty

                        Case MsgBoxResult.No
                            AvbrytXMLGenering = True
                            Exit Sub
                    End Select
                End If

                '-- namn --
                rorelsevagA.Namn = Rörelsevägstyp & "-" & bp_orgNamn & "-" & sp_orgNamn & " " & Trafikplatssignatur

                'slutpunkt stoppbock, förväntas vara stopplykta
                If SpBisObjektObjektnummerRv = "SB" Then
                    textinfo = "Driftplatsnummer: " & StationID & " Objektnamn: " & SpBisObjektObjektnummerRv & " Objekttyp: " & SpBisObjektObjektnummerRv &
                                ". Slutpunkt i rörelseväg förväntas vara en stopplykta, är projekterad som stoppbock. "

                    WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "SP rörelse", 1, textinfo, QualityControl_Counter)
                    QualityControl_Counter += 1
                End If


                '--------------------------
                '    Positioner
                '--------------------------
                ' Hanteras av BIS, behöver endast ange börjanpunkt och slutpunkt för rörelseväg. 
                ' Om det finns fler rörelsevägar från samma BP-SP så måste positioner anges.
                ' Funktion saknas i SwTmoves.

                '--------------------------
                '    Upplåsningspunkter
                '--------------------------

                'BIS data upplåsningspunkt
                ReadSignalObjectsBPUplSwTmoves(ProjectSwTmoves_Database) 'objektnummer, signal eller spårledning

                'typ av objekt. Signal (främst ATC)/Signal (ej ATC)
                If BpBisObjekttypnummerUpl = "6000" Or "10022" Or "10023" Or "10020" Then
                    signal = True
                Else
                    signal = False
                End If

                'typ av objekt signal för upplåsningspunkt
                If signal = True Then

                    '-- Upplåsningspunkter --
                    Dim upplasningspunktA As RorelsevagarOchSkyddLib.UpplasningspunktA = New RorelsevagarOchSkyddLib.UpplasningspunktA

                    '-- upplåsningspunkt --
                    rorelsevagA.Upplasningspunkter.Add(upplasningspunktA)
                    upplasningspunktA.Sekvensnummer = upplasningspunktSekvensnummer

                    '-- typ av upplåsningspunkt --
                    upplasningspunktA.TypAvUpplasningspunkt = RorelsevagarOchSkyddLib.Enumerations.TypAvUpplasningspunktA.Isolskarv

                    '-- BIS objekt börjanpunkt --
                    upplasningspunktA.Borjanpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                    upplasningspunktA.Borjanpunkt.BisObjektId.Objektnummer = BpBisObjektObjektnummerUpl
                    upplasningspunktA.Borjanpunkt.BisObjektId.Objekttypnummer = BpBisObjekttypnummerUpl

                    '-- BIS objekt slutpunkt--
                    upplasningspunktA.Slutpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                    upplasningspunktA.Slutpunkt.BisObjektId.Objektnummer = SpBisObjektObjektnummerUpl
                    upplasningspunktA.Slutpunkt.BisObjektId.Objekttypnummer = SpBisObjektObjekttypnummerUpl


                    'typ av objekt spårledning för upplåsningspunkt
                Else

                    '-- Upplåsningspunkter --
                    Dim upplasningspunktA As RorelsevagarOchSkyddLib.UpplasningspunktA = New RorelsevagarOchSkyddLib.UpplasningspunktA

                    '-- upplåsningspunkt --
                    rorelsevagA.Upplasningspunkter.Add(upplasningspunktA)
                    upplasningspunktA.Sekvensnummer = upplasningspunktSekvensnummer

                    '-- typ av upplåsningspunkt --
                    upplasningspunktA.TypAvUpplasningspunkt = RorelsevagarOchSkyddLib.Enumerations.TypAvUpplasningspunktA.Isolskarv

                    '-- BIS objekt börjanpunkt--
                    upplasningspunktA.Borjanpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                    upplasningspunktA.Borjanpunkt.BisObjektId.Objektnummer = BpBisObjektObjektnummerUpl
                    upplasningspunktA.Borjanpunkt.BisObjektId.Objekttypnummer = BpBisObjekttypnummerUpl

                    '-- BIS objekt slutpunkt--
                    upplasningspunktA.Slutpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                    upplasningspunktA.Slutpunkt.BisObjektId.Objektnummer = SpBisObjektObjektnummerUpl
                    upplasningspunktA.Slutpunkt.BisObjektId.Objekttypnummer = SpBisObjektObjekttypnummerUpl

                    textinfo = "Driftplatsnummer: " & BpBisStationsIDUpl & " Objektnamn: " & BpBisObjektNamnUpl & " Objekttyp: " & BpBisObjekttypnummerUpl &
                    ". Upplåsningspunkt med objekt spårledningar ska ändras till att ange objekt signal. Ingen genrering av upplåsningspunkt: "

                    WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "BP rörelse upplåsningspunkt spårledning", 1, textinfo, QualityControl_Counter)
                    QualityControl_Counter += 1

                    MessageBox.Show(textinfo)
                End If

                'loop
                SwTmovesPeekArrayMovements += 1
            Next

            '-------------------------
            '  Slutpunkter Alla
            '-------------------------
            Dim StationRow As SwTmovesDataSet.StationsRow = SwDataSet.Stations.FindByID(driftplatsnummer)
            Dim FiltString As String = "FKSiObjStID='" & StationRow.ID & "'"
            Dim SortString As String = "FKSiObjName ASC"
            Dim ResRows() As SwTmovesDataSet.EndObjRow = SwDataSet.EndObj.Select(FiltString, SortString)
            Dim LastSiName As String = ""
            Dim slutpunktsobjektA As RorelsevagarOchSkyddLib.SlutpunktsobjektA
            For Each EndRow As SwTmovesDataSet.EndObjRow In ResRows
                If StrComp(EndRow.FKSiObjName, LastSiName) <> 0 Then
                    FiltString = "BisID='" & EndRow.SignalObjectsRowParent.ObjectNR & "' AND O_Name='" & EndRow.SignalObjectsRowParent.ObjectName & "'"
                    Dim ObRows() As SwTmovesDataSet.OrginalNameRow = SwDataSet.OrginalName.Select(FiltString)
                    slutpunktsobjektA = New RorelsevagarOchSkyddLib.SlutpunktsobjektA
                    slutpunktsobjektA.SlutpunktsobjektId = New RorelsevagarOchSkyddLib.ObjektIdA
                    slutpunktsobjektA.SlutpunktsobjektId.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                    slutpunktsobjektA.SlutpunktsobjektId.BisObjektId.Objektnummer = EndRow.SignalObjectsRowParent.ObjectNR
                    slutpunktsobjektA.SlutpunktsobjektId.BisObjektId.Objekttypnummer = ObRows(0).O_typ

                    slutpunktsobjektA.Frislappningshastighet = RorelsevagarOchSkyddLib.Enumerations.FrislappningshastighetA.Okänd
                    slutpunktsobjektA.DynamisktFrontskydsomrade = False

                    rt.Slutpunkter.Add(slutpunktsobjektA)
                End If

                Dim slutpunktA As RorelsevagarOchSkyddLib.SlutpunktA = New RorelsevagarOchSkyddLib.SlutpunktA

                Select Case EndRow.MoveType
                    Case "tv"
                        slutpunktA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
                    Case "vv"
                        slutpunktA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Växlingsväg
                    Case "ftv"
                        slutpunktA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Förenkladtågväg
                    Case "stv"
                        slutpunktA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Särskildtågväg
                    Case "lib"
                        slutpunktA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Blocksträcka
                    Case "tam"
                        slutpunktA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.TAMsträcka
                    Case "btv" ' 
                        slutpunktA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Bevakadtågväg
                    Case Else
                        slutpunktA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
                        MessageBox.Show("Typ av rörelseväg för slutpunk ej implementerad i SwTmoves. Kontakta programmerare.")
                        Me.Close()
                End Select
                FiltString = "FKEObjName='" & EndRow.FKSiObjName & "' AND FKEObjType='" & EndRow.FKSiObjType & "' AND MovType='" & EndRow.MoveType & "'"
                Dim MovRows() As SwTmovesDataSet.MovementsRow = SwDataSet.Movements.Select(FiltString)
                If MovRows.Count > 0 Then
                    slutpunktA.Skyddsavstand = MovRows(0).Protectleght
                    slutpunktA.Skyddstracka = MovRows(0).ProtectDistance
                Else
                    slutpunktA.Skyddsavstand = -1
                    slutpunktA.Skyddstracka = -1
                End If
                '************************ FRONT SKYDD
                slutpunktA.SignaleradHastighetMotSlutpunkt = RorelsevagarOchSkyddLib.Enumerations.SignalbeskedMotSlutpunktA.Okänd
                If EndRow.GetFrontObjectsRows.Count > 0 Then
                    Dim frontskyddA As RorelsevagarOchSkyddLib.FrontskyddA = New RorelsevagarOchSkyddLib.FrontskyddA
                    frontskyddA.Id = 1

                    For Each FrontRow As SwTmovesDataSet.FrontObjectsRow In EndRow.GetFrontObjectsRows


                        frontskyddA.ObjektMedVillkor.Add(CreateFrontObjekt(FrontRow))

                        For Each IFRow As SwTmovesDataSet.FrontIfPointRow In FrontRow.GetFrontIfPointRows
                            frontskyddA.ObjektMedVillkor.Add(CreateIFFrontObjekt(IFRow))
                        Next

                    Next
                    slutpunktA.Frontskydd.Add(frontskyddA)
                End If

                '************************ fientliga rörelsevägar 
                For Each MovRow As SwTmovesDataSet.HostileMovementsRow In EndRow.GetHostileMovementsRows
                    Dim fientligrorelseA As RorelsevagarOchSkyddLib.FientligrorelseA = New RorelsevagarOchSkyddLib.FientligrorelseA
                    fientligrorelseA.RorelsevagId = MovRow.ID
                    fientligrorelseA.Rorelse = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelseA.Rörelseväg

                    fientligrorelseA.Konflikt = RorelsevagarOchSkyddLib.Enumerations.TypAvKonfliktA.Skyddsavstånd
                    slutpunktA.Fientligarorelser.Add(fientligrorelseA)

                    For Each IFpointRow As SwTmovesDataSet.IFPointHostMovRow In MovRow.GetIFPointHostMovRows
                        Dim omEjVillkorA As RorelsevagarOchSkyddLib.OmEjVillkorA = New RorelsevagarOchSkyddLib.OmEjVillkorA

                        omEjVillkorA.ObjektId.VerksamhetsId = New RorelsevagarOchSkyddLib.VerksamhetsIdA
                        omEjVillkorA.ObjektId.VerksamhetsId.Signatur = IFpointRow.SignalObjectsRowParent.ObjectName
                        omEjVillkorA.ObjektId.VerksamhetsId.Platssignatur = IFpointRow.SignalObjectsRowParent.StationsRow.ShortName

                        '--- objekttyp --
                        omEjVillkorA.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Växel

                        '-- Objektvillkor om ej --
                        If IFpointRow.Leg = False Then
                            omEjVillkorA.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Höger
                        Else
                            omEjVillkorA.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Vänster
                        End If
                        fientligrorelseA.OmEjVillkor.Add(omEjVillkorA)
                    Next
                Next

#Disable Warning BC42104 ' Variable is used before it has been assigned a value
                slutpunktsobjektA.Slutpunkter.Add(slutpunktA)
#Enable Warning BC42104 ' Variable is used before it has been assigned a value
            Next
            'write to XML-file
            rt.ToXmlFile(xmlfilename)

            'redovisa i leveransbrev
            PrintTMoves = True

        Catch ex As Exception
            DisplayError(ex, "felmedelande.")

        End Try
    End Sub

    Private Function CreateFrontObjekt(FrontRow As SwTmovesDataSet.FrontObjectsRow) As RorelsevagarOchSkyddLib.ObjektMedVillkorA
        Dim objektMedVillkorA As RorelsevagarOchSkyddLib.ObjektMedVillkorA = New RorelsevagarOchSkyddLib.ObjektMedVillkorA

        objektMedVillkorA.ArSkyddsObjekt = True
        objektMedVillkorA.TypAvVillkor = RorelsevagarOchSkyddLib.Enumerations.TypAvVillkorA.Väglåsning
        objektMedVillkorA.ObjektId = New RorelsevagarOchSkyddLib.ObjektIdA()

        Select Case FrontRow.FKFronProType
            Case "CV", "SB"

                objektMedVillkorA.ObjektId.VerksamhetsId = New RorelsevagarOchSkyddLib.VerksamhetsIdA
                objektMedVillkorA.ObjektId.VerksamhetsId.Signatur = FrontRow.FKFronProName
                objektMedVillkorA.ObjektId.VerksamhetsId.Platssignatur = FrontRow.SignalObjectsRowParent.StationsRow.ShortName
            Case Else
                Dim FiltString As String = "BisID='" & FrontRow.SignalObjectsRowParent.ObjectNR & "' AND O_Name='" & FrontRow.SignalObjectsRowParent.ObjectName & "'"
                Dim ObRows() As SwTmovesDataSet.OrginalNameRow = SwDataSet.OrginalName.Select(FiltString)

                objektMedVillkorA.ObjektId.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                objektMedVillkorA.ObjektId.BisObjektId.Objektnummer = FrontRow.SignalObjectsRowParent.ObjectNR
                objektMedVillkorA.ObjektId.BisObjektId.Objekttypnummer = ObRows(0).O_typ
        End Select

        Select Case FrontRow.FKFronProType
            Case "CV"
                objektMedVillkorA.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Växel
                'om inte växelläge
                If FrontRow.FrontProLeg = False Then
                    objektMedVillkorA.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Vänster
                Else
                    objektMedVillkorA.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Höger
                End If
            Case "SB"
                objektMedVillkorA.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Stoppbock
                Console.WriteLine("Objekttyp stoppbock")
                'om inte läge
                objektMedVillkorA.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.På
                Console.WriteLine("Kontrolläge på.")

            Case Else
                Dim FiltString As String = "BisID='" & FrontRow.SignalObjectsRowParent.ObjectNR & "' AND O_Name='" & FrontRow.SignalObjectsRowParent.ObjectName & "'"
                Dim ObRows() As SwTmovesDataSet.OrginalNameRow = SwDataSet.OrginalName.Select(FiltString)
                Select Case ObRows(0).O_typ
                    Case "6000", "6006", "10020", "10022", "10023"
                        objektMedVillkorA.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Stopp
                    Case "10015", "10050"
                        objektMedVillkorA.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.På
                End Select
        End Select

        Select Case FrontRow.EndObjRowParent.MoveType
            Case "tv"
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
            Case "btv"
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Bevakadtågväg
            Case "vv"
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Växlingsväg
            Case "ftv"
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Förenkladtågväg
            Case "stv"
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Särskildtågväg
            Case "lib"
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Blocksträcka
            Case "tam"
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.TAMsträcka
            Case "ntv"
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
            Case Else
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
                MessageBox.Show("Felaktig angiven rörelsetyp för frontskydd. Projektera om eller kontakta programmerar för ändring. " &
                                                        "Angivet värde: " & FrontskyddBisObjektObjectRorelse)
        End Select
        Return objektMedVillkorA
    End Function

    Private Function CreateIFFrontObjekt(FrontIfRow As SwTmovesDataSet.FrontIfPointRow) As RorelsevagarOchSkyddLib.ObjektMedVillkorA
        Dim objektMedVillkorA As RorelsevagarOchSkyddLib.ObjektMedVillkorA = New RorelsevagarOchSkyddLib.ObjektMedVillkorA

        objektMedVillkorA.ArSkyddsObjekt = False
        objektMedVillkorA.TypAvVillkor = RorelsevagarOchSkyddLib.Enumerations.TypAvVillkorA.Väglåsning
        objektMedVillkorA.ObjektId = New RorelsevagarOchSkyddLib.ObjektIdA()

        Select Case FrontIfRow.FKSiObjType
            Case "CV", "SB"

                objektMedVillkorA.ObjektId.VerksamhetsId = New RorelsevagarOchSkyddLib.VerksamhetsIdA
                objektMedVillkorA.ObjektId.VerksamhetsId.Signatur = FrontIfRow.FKSiObjName
                objektMedVillkorA.ObjektId.VerksamhetsId.Platssignatur = FrontIfRow.SignalObjectsRowParent.StationsRow.ShortName
            Case Else

                Dim FiltString As String = "BisID='" & FrontIfRow.SignalObjectsRowParent.ObjectNR & "' AND O_Name='" & FrontIfRow.SignalObjectsRowParent.ObjectName & "'"
                Dim ObRows() As SwTmovesDataSet.OrginalNameRow = SwDataSet.OrginalName.Select(FiltString)


                objektMedVillkorA.ObjektId.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                objektMedVillkorA.ObjektId.BisObjektId.Objektnummer = FrontIfRow.SignalObjectsRowParent.ObjectNR
                objektMedVillkorA.ObjektId.BisObjektId.Objekttypnummer = ObRows(0).O_typ
        End Select

        Select Case FrontIfRow.FKSiObjType
            Case "CV"
                objektMedVillkorA.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Växel
                'om inte växelläge
                If FrontIfRow.Leg = False Then
                    objektMedVillkorA.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Vänster
                Else
                    objektMedVillkorA.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Höger
                End If
            Case "SB"
                objektMedVillkorA.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Stoppbock
                Console.WriteLine("Objekttyp stoppbock")
                'om inte läge
                objektMedVillkorA.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.På
                Console.WriteLine("Kontrolläge på.")

            Case Else
                Dim FiltString As String = "BisID='" & FrontIfRow.SignalObjectsRowParent.ObjectNR & "' AND O_Name='" & FrontIfRow.SignalObjectsRowParent.ObjectName & "'"
                Dim ObRows() As SwTmovesDataSet.OrginalNameRow = SwDataSet.OrginalName.Select(FiltString)
                Select Case ObRows(0).O_typ
                    Case "6000", "6006", "10020", "10022", "10023"
                        objektMedVillkorA.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Stopp
                    Case "10015", "10050"
                        objektMedVillkorA.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.På
                End Select
        End Select

        Select Case FrontIfRow.FrontObjectsRow.EndObjRowParent.MoveType
            Case "tv"
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
            Case "btv"
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Bevakadtågväg
            Case "vv"
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Växlingsväg
            Case "ftv"
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Förenkladtågväg
            Case "stv"
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Särskildtågväg
            Case "lib"
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Blocksträcka
            Case "tam"
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.TAMsträcka
            Case "ntv"
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
            Case Else
                objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
                MessageBox.Show("Felaktig angiven rörelsetyp för frontskydd. Projektera om eller kontakta programmerar för ändring. " &
                                                        "Angivet värde: " & FrontskyddBisObjektObjectRorelse)
        End Select
        Return objektMedVillkorA
    End Function

    Private Sub FillSwTmovesDataSet(ByRef DSet As SwTmovesDataSet, ByVal driftplatsnr As Integer)
        Dim masterTA As SwTmovesDataSetTableAdapters.TableAdapterManager = New SwTmovesDataSetTableAdapters.TableAdapterManager

        masterTA.StationsTableAdapter = New SwTmovesDataSetTableAdapters.StationsTableAdapter
        masterTA.SignalObjectsTableAdapter = New SwTmovesDataSetTableAdapters.SignalObjectsTableAdapter
        masterTA.EndObjTableAdapter = New SwTmovesDataSetTableAdapters.EndObjTableAdapter
        masterTA.FrontObjectsTableAdapter = New SwTmovesDataSetTableAdapters.FrontObjectsTableAdapter
        masterTA.FrontIfPointTableAdapter = New SwTmovesDataSetTableAdapters.FrontIfPointTableAdapter
        masterTA.MovementsTableAdapter = New SwTmovesDataSetTableAdapters.MovementsTableAdapter
        masterTA.HostileMovementsTableAdapter = New SwTmovesDataSetTableAdapters.HostileMovementsTableAdapter
        masterTA.IFPointHostMovTableAdapter = New SwTmovesDataSetTableAdapters.IFPointHostMovTableAdapter
        masterTA.OrginalNameTableAdapter = New SwTmovesDataSetTableAdapters.OrginalNameTableAdapter

        Dim Connection As OleDb.OleDbConnection = New OleDb.OleDbConnection
        Connection.ConnectionString = ProjectSwTmoves_Database
        masterTA.StationsTableAdapter.Connection = Connection
        masterTA.SignalObjectsTableAdapter.Connection = Connection
        masterTA.EndObjTableAdapter.Connection = Connection
        masterTA.FrontObjectsTableAdapter.Connection = Connection
        masterTA.FrontIfPointTableAdapter.Connection = Connection
        masterTA.MovementsTableAdapter.Connection = Connection
        masterTA.HostileMovementsTableAdapter.Connection = Connection
        masterTA.IFPointHostMovTableAdapter.Connection = Connection
        masterTA.OrginalNameTableAdapter.Connection = Connection

        masterTA.StationsTableAdapter.FillByStationID(DSet.Stations, driftplatsnr)
        masterTA.SignalObjectsTableAdapter.FillByStationID(DSet.SignalObjects, driftplatsnr)
        masterTA.EndObjTableAdapter.FillByStationID(DSet.EndObj, driftplatsnr)
        masterTA.FrontObjectsTableAdapter.FillByStationID(DSet.FrontObjects, driftplatsnr)
        masterTA.FrontIfPointTableAdapter.FillByStationID(DSet.FrontIfPoint, driftplatsnr)
        masterTA.MovementsTableAdapter.FillByStationID(DSet.Movements, driftplatsnr)
        masterTA.HostileMovementsTableAdapter.FillByStationID(DSet.HostileMovements, driftplatsnr)
        masterTA.IFPointHostMovTableAdapter.FillByStationID(DSet.IFPointHostMov, driftplatsnr)
        masterTA.OrginalNameTableAdapter.Fill(DSet.OrginalName)

    End Sub
#End Region

#Region "Error Handler"
    Private Sub DisplayError(ByVal ex As Exception, ByVal Errortext As String)
        Dim errText As String = "Error - " & vbCrLf
        Do While (Not ex Is Nothing)
            errText = "Funktion " & Errortext & errText & ex.Message & vbCrLf
            ex = ex.InnerException
        Loop
        MessageBox.Show(Me, errText, "An Error occurred.", MessageBoxButton.OK, MessageBoxImage.Error)
    End Sub
#End Region

#Region "Mall XML-leverans till BIS. Underlag enligt ANDA 1.0"
    'Mall för XML-leverans ANDA 1.0
    Public Sub Skrivut(ByVal xmlfilename As String)

        Dim indexrorelsevag As Integer 'test klass rörelseväg
        Dim indexposition As Integer  'test klass positioner

        Try
            '-- Signaldata --
            Dim rt As RorelsevagarOchSkyddLib.Signaldata = New RorelsevagarOchSkyddLib.Signaldata

            'signaldata
            rt.StallverksId = "123" 'Option
            rt.Trafikplatssignatur = "Blg" 'option

            'test för att skapa flera rörelsevägar. Data från verktygen hanterar lopen
            indexrorelsevag = 0
            Do While indexrorelsevag <= 2

                '-- Rörelsevägar --
                Dim rorelsevagA As RorelsevagarOchSkyddLib.RorelsevagA = New RorelsevagarOchSkyddLib.RorelsevagA
                rt.Rorelsevagar.Add(rorelsevagA)
                rorelsevagA.Id = "1" 'option
                rorelsevagA.Namn = "TRH_21_31" 'option

                '-- Typ --
                rorelsevagA.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg
                'rorelsevagA.Typ = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Växlingsväg 'om det är växlingsväg

                '-- BISobjekt börjanpunkt --
                rorelsevagA.Borjanpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                rorelsevagA.Borjanpunkt.BisObjektId.Objektnummer = 21
                rorelsevagA.Borjanpunkt.BisObjektId.Objekttypnummer = 6000 'Signal främst ATC

                '-- BISobjekt slutpunkt --
                rorelsevagA.Slutpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                rorelsevagA.Slutpunkt.BisObjektId.Objektnummer = 31
                rorelsevagA.Slutpunkt.BisObjektId.Objekttypnummer = 6000 'Signal främst ATC

                'test för att skapa flera positioner. Data från verktygen hanterar lopen
                indexposition = 0
                Do While indexposition <= 3

                    '-- Positioner --
                    Dim positionA As RorelsevagarOchSkyddLib.PositionA = New RorelsevagarOchSkyddLib.PositionA

                    '-- Id
                    rorelsevagA.Positioner.Add(positionA)
                    positionA.Sekvensnummer = indexposition 'kronologisk ordning

                    '-- BISobjekt --
                    positionA.PositioneringsobjektId.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                    positionA.PositioneringsobjektId.BisObjektId.Objektnummer = 131 + indexposition
                    positionA.PositioneringsobjektId.BisObjektId.Objekttypnummer = 131 + indexposition

                    indexposition += 1
                Loop

                'test för att skapa flera Upplåsningspunkter. Data från verktygen hanterar lopen
                indexposition = 0
                Do While indexposition <= 2

                    '-- Upplåsningspunkter --
                    Dim upplasningspunktA As RorelsevagarOchSkyddLib.UpplasningspunktA = New RorelsevagarOchSkyddLib.UpplasningspunktA

                    '-- upplåsningspunkt --
                    rorelsevagA.Upplasningspunkter.Add(upplasningspunktA)
                    upplasningspunktA.Sekvensnummer = 1

                    '-- typ av upplåsningspunkt --
                    upplasningspunktA.TypAvUpplasningspunkt = RorelsevagarOchSkyddLib.Enumerations.TypAvUpplasningspunktA.Isolskarv

                    '-- BISobjekt --
                    upplasningspunktA.Borjanpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                    upplasningspunktA.Borjanpunkt.BisObjektId.Objektnummer = 200 + indexposition
                    upplasningspunktA.Borjanpunkt.BisObjektId.Objekttypnummer = 6000

                    '-- BISobjekt --
                    upplasningspunktA.Slutpunkt.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                    upplasningspunktA.Slutpunkt.BisObjektId.Objektnummer = 230 + indexposition
                    upplasningspunktA.Slutpunkt.BisObjektId.Objekttypnummer = 6000

                    indexposition += 1
                Loop

                indexrorelsevag += 1
            Loop

            'test för att skapa flera Slutpunktsobjekt. Data från verktygen hanterar lopen
            indexrorelsevag = 0
            Do While indexrorelsevag <= 2

                '-- Slutpunktsobjekt --
                Dim slutpunktsobjektA As RorelsevagarOchSkyddLib.SlutpunktsobjektA = New RorelsevagarOchSkyddLib.SlutpunktsobjektA
                rt.Slutpunkter.Add(slutpunktsobjektA)

                '-- SlutpunktsobjektId
                slutpunktsobjektA.SlutpunktsobjektId = New RorelsevagarOchSkyddLib.ObjektIdA

                '-- BISobjekt --
                slutpunktsobjektA.SlutpunktsobjektId.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                slutpunktsobjektA.SlutpunktsobjektId.BisObjektId.Objektnummer = 31
                slutpunktsobjektA.SlutpunktsobjektId.BisObjektId.Objekttypnummer = 6000

                '-- frisläppningshastighet --
                slutpunktsobjektA.Frislappningshastighet = RorelsevagarOchSkyddLib.Enumerations.FrislappningshastighetA._10Övervakning

                '-- DynamisktFrontskydsomrade --
                slutpunktsobjektA.DynamisktFrontskydsomrade = False

                'test för att hantera slutpunkter beroende på rörelseväg. Data från verktygen hanterar lopen
                indexposition = 0
                Do While indexposition <= 2

                    '-- Slutpunkt --
                    Dim slutpunktA As RorelsevagarOchSkyddLib.SlutpunktA = New RorelsevagarOchSkyddLib.SlutpunktA
                    slutpunktsobjektA.Slutpunkter.Add(slutpunktA)

                    '-- typ av rörelseväg --
                    slutpunktA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg

                    '-- skyddsavstånd --
                    slutpunktA.Skyddsavstand = 200

                    '-- skyddsträcka --
                    slutpunktA.Skyddstracka = 100


                    '-----------------
                    ' Frontskydd 
                    '-----------------
                    Dim frontskyddA As RorelsevagarOchSkyddLib.FrontskyddA = New RorelsevagarOchSkyddLib.FrontskyddA
                    slutpunktA.Frontskydd.Add(frontskyddA)

                    frontskyddA.Id = "1"

                    'objekt med villkor
                    Dim objektMedVillkorA As RorelsevagarOchSkyddLib.ObjektMedVillkorA = New RorelsevagarOchSkyddLib.ObjektMedVillkorA
                    frontskyddA.ObjektMedVillkor.Add(objektMedVillkorA)

                    '-- objekt BIS/ANDA/Verksamhet  --
                    objektMedVillkorA.ObjektId = New RorelsevagarOchSkyddLib.ObjektIdA()

                    '-- BIS objekt --
                    objektMedVillkorA.ObjektId.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                    objektMedVillkorA.ObjektId.BisObjektId.Objektnummer = 21
                    objektMedVillkorA.ObjektId.BisObjektId.Objekttypnummer = 6000

                    'är skyddsobjekt
                    objektMedVillkorA.ArSkyddsObjekt = False

                    'Typ av rörelse
                    objektMedVillkorA.TypAvRorelsevag = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelsevagA.Tågväg

                    'typ av villkor
                    objektMedVillkorA.TypAvVillkor = RorelsevagarOchSkyddLib.Enumerations.TypAvVillkorA.Väglåsning

                    'objekt ska ligga i läge
                    objektMedVillkorA.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Höger

                    '---------------------------
                    ' Slutpunktsupplåsning
                    '---------------------------

                    Dim slutpunktsupplasningA As RorelsevagarOchSkyddLib.SlutpunktsupplasningA = New RorelsevagarOchSkyddLib.SlutpunktsupplasningA
                    slutpunktA.Slutpunktsupplasning.Add(slutpunktsupplasningA)

                    '-- Initeras av objekt
                    slutpunktsupplasningA.IntierasAv = New RorelsevagarOchSkyddLib.ObjektIdA

                    '-- BISobjekt --
                    slutpunktsupplasningA.IntierasAv.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                    slutpunktsupplasningA.IntierasAv.BisObjektId.Objektnummer = 23
                    slutpunktsupplasningA.IntierasAv.BisObjektId.Objekttypnummer = 6000

                    '-- tidsfördröjning vid upplåsning
                    slutpunktsupplasningA.Tid = 90

                    '-- Signalerad hastighet mot slutpunkt
                    slutpunktA.SignaleradHastighetMotSlutpunkt = RorelsevagarOchSkyddLib.Enumerations.SignalbeskedMotSlutpunktA.Kör40Med10Övervakning


                    '-------------------------------------
                    ' Fientliga rörelser mot slutpunkt
                    '-------------------------------------

                    Dim fientligrorelseA As RorelsevagarOchSkyddLib.FientligrorelseA = New RorelsevagarOchSkyddLib.FientligrorelseA
                    slutpunktA.Fientligarorelser.Add(fientligrorelseA)

                    '-- Typ av rörelse --
                    fientligrorelseA.Rorelse = RorelsevagarOchSkyddLib.Enumerations.TypAvRorelseA.Rörelseväg

                    '-- Om ej --
                    Dim omEjVillkorA As RorelsevagarOchSkyddLib.OmEjVillkorA = New RorelsevagarOchSkyddLib.OmEjVillkorA
                    fientligrorelseA.OmEjVillkor.Add(omEjVillkorA)

                    '-- Läge objekt --
                    omEjVillkorA.Villkor = RorelsevagarOchSkyddLib.Enumerations.ObjektvillkorA.Vänster

                    '-- BISobjekt --
                    omEjVillkorA.ObjektId.BisObjektId = New RorelsevagarOchSkyddLib.BisObjektIdA
                    omEjVillkorA.ObjektId.BisObjektId.Objektnummer = 41
                    omEjVillkorA.ObjektId.BisObjektId.Objekttypnummer = -6001

                    '-- Verksamhetsid --
                    omEjVillkorA.ObjektId.VerksamhetsId = New RorelsevagarOchSkyddLib.VerksamhetsIdA
                    omEjVillkorA.ObjektId.VerksamhetsId.Signatur = "S21"
                    omEjVillkorA.ObjektId.VerksamhetsId.Platssignatur = "BLG"
                    omEjVillkorA.ObjektId.VerksamhetsId.Objekttyp = RorelsevagarOchSkyddLib.Enumerations.ObjekttyperA.Växel

                    '-- Konflikt ---
                    fientligrorelseA.Konflikt = RorelsevagarOchSkyddLib.Enumerations.TypAvKonfliktA.Skyddsavstånd

                    indexposition += 1
                Loop

                indexrorelsevag += 1
            Loop

            'write to XML-file
            rt.ToXmlFile(xmlfilename)

        Catch ex As Exception
            DisplayError(ex, "Skriv ut XML-filer för vald bandel.")

        End Try
    End Sub
#End Region

#Region "Window_Loaded"
    Private Sub Window_Loaded(sender As Object, e As RoutedEventArgs)

        'Create ErrorRepor directory
        CreateErrorReportdirectory()

        'ProgressBar1
        txtXMLFileCreate.Text = ""
        ProgressBar1.Visibility = Visibility.Hidden

        'Knapp Avbryt Generering
        btnCancelGenerating.Visibility = Visibility.Hidden

        'Knapp Avbryt Generering
        btnVTools.Visibility = Visibility.Hidden
        VtoolsDataSet = New VToolsInmatningDataSet

        VtoolsDataAdaper = New VToolsInmatningDataSetTableAdapters.TableAdapterManager

        VtoolsDataAdaper.DriftplatserTableAdapter = New VToolsInmatningDataSetTableAdapters.DriftplatserTableAdapter

        VtoolsDataAdaper.RörelsevägarTableAdapter = New VToolsInmatningDataSetTableAdapters.RörelsevägarTableAdapter
        VtoolsDataAdaper.VägdelariRörelsevägarTableAdapter = New VToolsInmatningDataSetTableAdapters.VägdelariRörelsevägarTableAdapter
        VtoolsDataAdaper.KonflikterTableAdapter = New VToolsInmatningDataSetTableAdapters.KonflikterTableAdapter
        VtoolsDataAdaper.SlutpunkterTableAdapter = New VToolsInmatningDataSetTableAdapters.SlutpunkterTableAdapter

        VtoolsDataAdaper.VägdelarTableAdapter = New VToolsInmatningDataSetTableAdapters.VägdelarTableAdapter
        VtoolsDataAdaper.LåsningarTableAdapter = New VToolsInmatningDataSetTableAdapters.LåsningarTableAdapter
        VtoolsDataAdaper.KörningarTableAdapter = New VToolsInmatningDataSetTableAdapters.KörningarTableAdapter
        VtoolsDataAdaper.ViaPunkterTableAdapter = New VToolsInmatningDataSetTableAdapters.ViaPunkterTableAdapter
        VtoolsDataAdaper.MotriktadeObjektTableAdapter = New VToolsInmatningDataSetTableAdapters.MotriktadeObjektTableAdapter
        VtoolsDataAdaper.SpårledningarTableAdapter = New VToolsInmatningDataSetTableAdapters.SpårledningarTableAdapter
        VtoolsDataAdaper.ObjektiVägTableAdapter = New VToolsInmatningDataSetTableAdapters.ObjektiVägTableAdapter

        VtoolsbacktobackDataAdapter = New VToolsInmatningDataSetTableAdapters.BacktoBackTableAdapter


        VtoolsDataAdaper.BisObjektTableAdapter = New VToolsInmatningDataSetTableAdapters.BisObjektTableAdapter
        VtoolsDataAdaper.FrontSkyddTableAdapter = New VToolsInmatningDataSetTableAdapters.FrontSkyddTableAdapter
        VtoolsDataAdaper.KörningFrontTableAdapter = New VToolsInmatningDataSetTableAdapters.KörningFrontTableAdapter
        VtoolsDataAdaper.LåsningarFrontTableAdapter = New VToolsInmatningDataSetTableAdapters.LåsningarFrontTableAdapter
        VtoolsDataAdaper.FrontObjektTableAdapter = New VToolsInmatningDataSetTableAdapters.FrontObjektTableAdapter
        VtoolsDataAdaper.FrontSkyddOmTableAdapter = New VToolsInmatningDataSetTableAdapters.FrontSkyddOmTableAdapter
        VtoolsDataAdaper.KonfliktOmEjTableAdapter = New VToolsInmatningDataSetTableAdapters.KonfliktOmEjTableAdapter

        VtoolsDataAdaper.SplobjektTableAdapter = New VToolsInmatningDataSetTableAdapters.SplobjektTableAdapter



        VtoolsDataAdaper.DriftplatserTableAdapter.Fill(VtoolsDataSet.Driftplatser)
        VtoolsDataAdaper.BisObjektTableAdapter.Fill(VtoolsDataSet.BisObjekt)
        VtoolsDataAdaper.RörelsevägarTableAdapter.Fill(VtoolsDataSet.Rörelsevägar)
        VtoolsDataAdaper.SlutpunkterTableAdapter.Fill(VtoolsDataSet.Slutpunkter)
        VtoolsDataAdaper.VägdelarTableAdapter.Fill(VtoolsDataSet.Vägdelar)
        VtoolsDataAdaper.VägdelariRörelsevägarTableAdapter.Fill(VtoolsDataSet.VägdelariRörelsevägar)
        VtoolsDataAdaper.LåsningarTableAdapter.Fill(VtoolsDataSet.Låsningar)
        VtoolsDataAdaper.KörningarTableAdapter.Fill(VtoolsDataSet.Körningar)
        VtoolsDataAdaper.ViaPunkterTableAdapter.Fill(VtoolsDataSet.ViaPunkter)
        VtoolsDataAdaper.MotriktadeObjektTableAdapter.Fill(VtoolsDataSet.MotriktadeObjekt)
        VtoolsDataAdaper.ObjektiVägTableAdapter.Fill(VtoolsDataSet.ObjektiVäg)
        VtoolsDataAdaper.SpårledningarTableAdapter.Fill(VtoolsDataSet.Spårledningar)
        VtoolsDataAdaper.FrontSkyddTableAdapter.Fill(VtoolsDataSet.FrontSkydd)
        VtoolsDataAdaper.FrontObjektTableAdapter.Fill(VtoolsDataSet.FrontObjekt)
        VtoolsDataAdaper.KörningFrontTableAdapter.Fill(VtoolsDataSet.KörningFront)
        VtoolsDataAdaper.LåsningarFrontTableAdapter.Fill(VtoolsDataSet.LåsningarFront)
        VtoolsDataAdaper.KonflikterTableAdapter.Fill(VtoolsDataSet.Konflikter)
        VtoolsDataAdaper.KonfliktOmEjTableAdapter.Fill(VtoolsDataSet.KonfliktOmEj)
        VtoolsDataAdaper.FrontSkyddOmTableAdapter.Fill(VtoolsDataSet.FrontSkyddOm)
        VtoolsDataAdaper.SplobjektTableAdapter.Fill(VtoolsDataSet.Splobjekt)

        VtoolsbacktobackDataAdapter.Fill(VtoolsDataSet.BacktoBack)


        AddHandler VtoolsDataAdaper.VägdelarTableAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleVägDelarRowUpdated)

        AddHandler VtoolsDataAdaper.RörelsevägarTableAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleRörelserRowUpdated)
        AddHandler VtoolsDataAdaper.BisObjektTableAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleBisIDRowUpdated)

        AddHandler VtoolsDataAdaper.KonflikterTableAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleKonfliktIDRowUpdated)

        AddHandler VtoolsDataAdaper.FrontSkyddTableAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleFrontskyddIDRowUpdated)
        AddHandler VtoolsDataAdaper.SlutpunkterTableAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleSlutpunkterRowUpdated)
        AddHandler VtoolsbacktobackDataAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleBacktobackRowUpdated)

    End Sub
#End Region

#Region "<!--Knapp Skapa Bandel-->"
    Private Sub btnCreateMappProjectBIS_Click(sender As Object, e As RoutedEventArgs)

        'Rensa Databasen Innan Ny Linje Skapas
        RensaDatabasenInnanNyLinjeSkapas()

        'kontroll om någon bandel är vald
        If txtTrackSection.Text = "" Then
            MessageBox.Show("Välj bandel.", "Felmedelande", MessageBoxButton.OK, MessageBoxImage.Warning)
            txtTrackSection.Focus()
            Exit Sub
        End If

        'val
        Val_BandelDriftplats = "Bandel"

        '-- info user --
        Select Case Val_BandelDriftplats

            Case "Bandel"
                txtXMLFileCreate.Text = "Bandel vald."
                Console.WriteLine("Bandel vald som alternativ för skapande av XML-utskrift.")

            Case Else
                txtXMLFileCreate.Text = "Inget val utfört av Bandel eller Driftplats."
                txtTrackSection.Focus()
                Exit Sub
        End Select

        'Kontroll att någon leverans är vald
        'ANDA 1.0
        If rdbdeliveryANDA1_0.IsChecked = True Then

            'ANDA 2.0
        ElseIf rdbdeliveryANDA2_0.IsChecked = True Then

            'Inget valt
        Else
            MessageBox.Show("Välj vilken leverans som ska utföras.", "Felmedelande", MessageBoxButton.OK, MessageBoxImage.Warning)
            rdbdeliveryANDA1_0.Focus()
            Exit Sub
        End If

        'Skapa grunder för XML leverans ANDA
        CreateXMLBISData()
    End Sub

    Private Sub RensaDatabasenInnanNyLinjeSkapas()
        ' Rensa databasen innan ny linje skapas 
        For Each Dp As VToolsInmatningDataSet.DriftplatserRow In VtoolsDataSet.Driftplatser.Rows
            Dp.Delete()
        Next

        'DataAdaper
        VtoolsDataAdaper.UpdateAll(VtoolsDataSet)

        VtoolsDataAdaper.DriftplatserTableAdapter.Fill(VtoolsDataSet.Driftplatser)
        VtoolsDataAdaper.BisObjektTableAdapter.Fill(VtoolsDataSet.BisObjekt)
        VtoolsDataAdaper.RörelsevägarTableAdapter.Fill(VtoolsDataSet.Rörelsevägar)
        VtoolsDataAdaper.SlutpunkterTableAdapter.Fill(VtoolsDataSet.Slutpunkter)
        VtoolsDataAdaper.VägdelarTableAdapter.Fill(VtoolsDataSet.Vägdelar)
        VtoolsDataAdaper.VägdelariRörelsevägarTableAdapter.Fill(VtoolsDataSet.VägdelariRörelsevägar)
        VtoolsDataAdaper.LåsningarTableAdapter.Fill(VtoolsDataSet.Låsningar)
        VtoolsDataAdaper.KörningarTableAdapter.Fill(VtoolsDataSet.Körningar)
        VtoolsDataAdaper.ViaPunkterTableAdapter.Fill(VtoolsDataSet.ViaPunkter)
        VtoolsDataAdaper.MotriktadeObjektTableAdapter.Fill(VtoolsDataSet.MotriktadeObjekt)
        VtoolsDataAdaper.ObjektiVägTableAdapter.Fill(VtoolsDataSet.ObjektiVäg)
        VtoolsDataAdaper.SpårledningarTableAdapter.Fill(VtoolsDataSet.Spårledningar)
        VtoolsDataAdaper.FrontSkyddTableAdapter.Fill(VtoolsDataSet.FrontSkydd)
        VtoolsDataAdaper.FrontObjektTableAdapter.Fill(VtoolsDataSet.FrontObjekt)
        VtoolsDataAdaper.KörningFrontTableAdapter.Fill(VtoolsDataSet.KörningFront)
        VtoolsDataAdaper.LåsningarFrontTableAdapter.Fill(VtoolsDataSet.LåsningarFront)
        VtoolsDataAdaper.KonflikterTableAdapter.Fill(VtoolsDataSet.Konflikter)
        VtoolsDataAdaper.KonfliktOmEjTableAdapter.Fill(VtoolsDataSet.KonfliktOmEj)
        VtoolsDataAdaper.FrontSkyddOmTableAdapter.Fill(VtoolsDataSet.FrontSkyddOm)
        VtoolsDataAdaper.SplobjektTableAdapter.Fill(VtoolsDataSet.Splobjekt)

        For Each row As VToolsInmatningDataSet.BacktoBackRow In VtoolsDataSet.BacktoBack.Rows
            row.Delete()
        Next

        VtoolsbacktobackDataAdapter.Update(VtoolsDataSet.BacktoBack)

    End Sub
#End Region

#Region "ExcelSaveQuit"
    Public Sub ExcelSaveQuit()
        'om angiven fil leveransbrev existerar
        If System.IO.File.Exists(newfileProofOfDelivery) = True Then
            Console.WriteLine("Spara excelfil leveransbrev och stäng. ProofOfDelivery_xlWorkBook")
            ProofOfDelivery_xlWorkBook.Save()
            ProofOfDelivery_xlWorkBook.Close()
        Else
            Console.WriteLine("Angiven excelfil leveransbrev existerar inte för att kunna spara och stänga. " & newfileProofOfDelivery)
        End If

        'om angiven fil Kvalitetskontroll existerar
        If System.IO.File.Exists(QualityControl_ExcelFile) = True Then
            Console.WriteLine("Spara excelfil Kvalitetskontroll och stäng. QualityControl_xlWorkBook")
            QualityControl_xlWorkBook.Save()
            QualityControl_xlWorkBook.Close()
        Else
            Console.WriteLine("Angiven excelfil Kvalitetskontroll existerar inte för att kunna spara och stänga. " & QualityControl_ExcelFile)
        End If

        'om angiven fil platser bandel VTools existerar
        If System.IO.File.Exists(NewListOperationSite) = True Then
            Console.WriteLine("Stäng excelfil platser bandel VTools. ListOperationSite_xlWorkBook")
            ListOperationSite_xlWorkBook.Close()
        Else
            Console.WriteLine("Angiven excelfil Kvalitetskontroll existerar inte för att kunna stänga. " & NewListOperationSite)
        End If

        'stäng applikation Excel
        xlApp.Quit()

    End Sub

    Public Sub ExcelSaveQuitControl()
        Console.WriteLine("Spara och stäng anslutningar till Excelfiler om den är öppen")


    End Sub

#End Region

#Region "<!--Knapp Skapa Driftplatsnummer-->"
    Private Sub btnCreateMappStationId_Click(sender As Object, e As RoutedEventArgs)

        'kontroll om någon driftplats är vald
        If txtStationId.Text = "" Then
            MessageBox.Show("Välj driftplatsnummer.", "Felmedelande", MessageBoxButton.OK, MessageBoxImage.Warning)
            txtStationId.Focus()
            Exit Sub
        End If

        'kontroll att endast ett val är utfört
        If ck_SwTmoves.IsChecked = True And ck_VToolsOnly.IsChecked = True Then
            MessageBox.Show("Välj endast ett av valen SwTmoves eller VTools.", "Driftplats", MessageBoxButton.OK, MessageBoxImage.Warning)
            gb_SelectPrinting.Focus()
            Exit Sub
        End If

        'val
        Val_BandelDriftplats = "Driftplats"

        '-- info user --
        Select Case Val_BandelDriftplats

            Case "Driftplats"
                txtXMLFileCreate.Text = "Driftplats vald."
                Console.WriteLine("Driftplats vald som alternativ för skapande av XML-utskrift.")

            Case Else
                txtXMLFileCreate.Text = "Inget val utfört av Bandel eller Driftplats."
                Exit Sub
        End Select

        'Kontroll att någon leverans är vald
        'ANDA 1.0
        If rdbdeliveryANDA1_0.IsChecked = True Then

            'ANDA 2.0
        ElseIf rdbdeliveryANDA2_0.IsChecked = True Then

            'Inget valt
        Else
            MessageBox.Show("Välj vilken leverans som ska utföras.", "Felmedelande", MessageBoxButton.OK, MessageBoxImage.Warning)
            rdbdeliveryANDA1_0.Focus()
            Exit Sub
        End If

        If ck_VToolsOnly.IsChecked = True Then
            RensaDatabasenInnanNyLinjeSkapas()
        End If

        'Skapa grunder för XML leverans ANDA
        CreateXMLBISData()
    End Sub
#End Region

#Region "Start variabler knappar"
    Private Sub Startvariablerknappar()

        'knappar
        btnCreateMappProjectBIS.IsEnabled = False
        btnCreateMappStationId.IsEnabled = False
    End Sub
#End Region

#Region "Grundvärde/normaltillstånd knappar"
    Private Sub Stopvariablerknappar()

        'knappar
        btnCreateMappProjectBIS.IsEnabled = True
        btnCreateMappStationId.IsEnabled = True
    End Sub
#End Region

#Region "<!--Knapp Avbryt Generering-->"
    Private Sub btnCancelGenerating_Click(sender As Object, e As RoutedEventArgs)

        'slutrad verifiering
        WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, "000000", "END", 10000, "Verifiering klart",
                                                QualityControl_Counter)

        'SwTmoves databas close ANDA
        CloseDatabasSwTmoves(ProjectSwTmoves_Database)

        'Save and close Excel
        ExcelSaveQuit()

        'skapa instans ProgressBar1
        Dim updatePbDelegate As New UpdateProgressBarDelegate(AddressOf ProgressBar1.SetValue)

        'ProgressBar1
        valueProgressbarXMLExport = 0

        'updatera ProgressBar1
        Dispatcher.Invoke(updatePbDelegate,
                      System.Windows.Threading.DispatcherPriority.Background,
                      New Object() {ProgressBar.ValueProperty, valueProgressbarXMLExport})

        'info user
        txtXMLFileCreate.Text = "Genrering klart"

        MessageBox.Show("Export ANDA avbryten men delar av leveransen är eventuellt genererad. ", "Generering XML", MessageBoxButton.OK, MessageBoxImage.Information)
    End Sub
#End Region

#Region "<!--Knapp VTools test XML-->"
    Private Sub btnVTools_Click(sender As Object, e As RoutedEventArgs)
        For Each Dp As VToolsInmatningDataSet.DriftplatserRow In VtoolsDataSet.Driftplatser.Rows
            Dp.Delete()
        Next
        VtoolsDataAdaper.UpdateAll(VtoolsDataSet)
        Inmatnign("C:\temp\ANDA\CSV Filer\Skavstaby\")
        Dim FilterString As String = "plnr='950'"
        Dim ResultRows() As VToolsInmatningDataSet.DriftplatserRow = VtoolsDataSet.Driftplatser.Select(FilterString)


        For Each Driftplats As VToolsInmatningDataSet.DriftplatserRow In VtoolsDataSet.Driftplatser
            Dim filePath As String = "C:\Users\SEDHAL\Documents\ANDA-leverans\Test_" & Driftplats.plnamn & ".xml"
            SkrivutVtoolsData(filePath, Driftplats, "C:\temp\ANDA\CSV Filer\Skavstaby\")
            Driftplats.GetRörelsevägarRows()
        Next

    End Sub
#End Region

#Region "Skapa grunder för XML leverans ANDA"
    Private Sub CreateXMLBISData()
        Dim TextBoxProjectDir As String
        Dim postleveransbrev As Integer = 1 'startvärde för post

        'info user
        txtXMLFileCreate.Text = ""

        Try
            ' Configure save file dialog box 
            Dim exportsignaldata As New SaveFileDialog()
            exportsignaldata.Title = "Skapa Projektmapp för exportfil till BIS"
            exportsignaldata.CheckPathExists = True
            exportsignaldata.InitialDirectory = "\\segvxfs001\PROJEKT\7656\7501425\000\3. Genomförande (E)\3.8 Leverans\Generering"

            Select Case Val_BandelDriftplats

                Case "Bandel"
                    exportsignaldata.FileName = "Bandel " & txtTrackSection.Text & " " & Date.Today ' Default file name

                Case "Driftplats"
                    exportsignaldata.FileName = "Driftplatsnummer " & txtStationId.Text & " " & Date.Today ' Default file name
            End Select

            ' Open dialog box and retrieve dialog result when ShowDialog returns
            Dim result As Boolean = exportsignaldata.ShowDialog()

            If result = True Then

                'välj hur leveransen ska distribueras till Trafikverket
                Dim DeliveryXMLToTrv1 As New DeliveryXMLToTrv()
                DeliveryXMLToTrv1.ShowDialog()

                'Konfiguera ProgressBar
                ProgressBar1.Minimum = 0
                ProgressBar1.Maximum = 32000
                ProgressBar1.Value = 0
                valueProgressbarXMLExport = 0

                'skapa instans ProgressBar1
                Dim updatePbDelegate As New UpdateProgressBarDelegate(AddressOf ProgressBar1.SetValue)

                'info user
                txtXMLFileCreate.Text = "Skapande av Temporär Mapp för huvudprojekt pågår."

                'updatera ProgressBar1
                Dispatcher.Invoke(updatePbDelegate,
                              System.Windows.Threading.DispatcherPriority.Background,
                              New Object() {ProgressBar.ValueProperty, valueProgressbarXMLExport})

                ProgressBar1.Visibility = Visibility.Visible

                'grund knappar osv.
                Startvariablerknappar()

                'grundvärde vid start av generering
                StartvariableBandel()

                'vald bandel/driftplats
                TextBoxProjectDir = exportsignaldata.FileName
                exportsignaldata.InitialDirectory = exportsignaldata.FileName

                'bestäm huvudkatalog projekt
                Dim fileinfo As New FileInfo(TextBoxProjectDir)
                ProjectDirectory = fileinfo.DirectoryName
                OnlyFileNameProject = exportsignaldata.SafeFileName

                'skapa tillfälligt mapp på C-disk.
                'slutlig flytt av temp till valt ställe
                Dim OrginalProjectDirectory As String = ProjectDirectory

                'ny temp sökväg projektkatalog
                ProjectDirectory = FolderReceiverProject
                'ProjectDirectory = OrginalProjectDirectory

                'skapa alla grundmappar för projekt
                CreateAllMapps()

                'Kopiera Databas TMoves
                If ck_SwTmoves.IsChecked = True Then

                    'info user
                    txtXMLFileCreate.Text = "kopiera databas ANDA för SwTmoves."

                    'updatera ProgressBar1
                    Dispatcher.Invoke(updatePbDelegate,
                              System.Windows.Threading.DispatcherPriority.Background,
                              New Object() {ProgressBar.ValueProperty, valueProgressbarXMLExport})

                    'kopiera databaser till projekt
                    CopyDatabasFileProject()

                    'info user
                    txtXMLFileCreate.Text = "Fyll databas ANDA för SwTmoves med BIS namn på signaler."

                    'updatera ProgressBar1
                    Dispatcher.Invoke(updatePbDelegate,
                              System.Windows.Threading.DispatcherPriority.Background,
                              New Object() {ProgressBar.ValueProperty, valueProgressbarXMLExport})

                    ' Skapa fyll en tabell som man sedan kan slå i för att hitta rätt namn på signaler
                    initDataAdatpter(ProjectSwTmoves_Database)
                    OrginalAdapter.Fill(OrginalNametable)
                End If

                'info user
                txtXMLFileCreate.Text = "Kopiera excelfiler och skapa anslutningar till blad."

                'updatera ProgressBar1
                Dispatcher.Invoke(updatePbDelegate,
                              System.Windows.Threading.DispatcherPriority.Background,
                              New Object() {ProgressBar.ValueProperty, valueProgressbarXMLExport})

                'Copy/open Excel for project
                CopyExcelFileProject()

                'projekterad i SwTmoves. Läs in alla stationer/linjer som ska skrivas ut i XML
                If ck_SwTmoves.IsChecked = True Then

                    'info user
                    txtXMLFileCreate.Text = "Kontroll vilka driftplatser/Linjer projekterade i SwTmoves."

                    'updatera ProgressBar1
                    Dispatcher.Invoke(updatePbDelegate,
                              System.Windows.Threading.DispatcherPriority.Background,
                              New Object() {ProgressBar.ValueProperty, valueProgressbarXMLExport})

                    'kontroll antal
                    ReadAllStationToArray_SwTmoves()
                End If

                'projekterad i VTools. Läs in alla stationer/linjer som ska skrivas ut i XML
                If ck_VToolsOnly.IsChecked = True Then

                    'info user
                    txtXMLFileCreate.Text = "Kontroll vilka driftplatser/Linjer projekterade i VTools."

                    'updatera ProgressBar1
                    Dispatcher.Invoke(updatePbDelegate,
                              System.Windows.Threading.DispatcherPriority.Background,
                              New Object() {ProgressBar.ValueProperty, valueProgressbarXMLExport})

                    'kontroll antal
                    ReadAllStationToArray_VTools()
                End If

                'ProgressBar1 spread
                Select Case Val_BandelDriftplats

                    Case "Bandel"
                        If SwTmovesArrayStationsPost <= 0 Then
                            barvalue = 32000
                        Else
                            'kräver mera vid VTools generering
                            barvalue = 32000 / SwTmovesArrayStationsPost + AntalProjekteringarVTool
                        End If

                    Case "Driftplats"
                        barvalue = 27000
                        valueProgressbarXMLExport = 5000
                End Select

                'Skriv till Excelflik Leverans, WriteSignalDatumDataExcel Rubrik Bandel
                xlWorksheet = ProofOfDelivery_xlWorkBook.Worksheets("Leverans") 'flik Leverans
                WriteswDateDataExcel(xlWorksheet, Date.Today) 'DATUM
                WriteswAuthorDataExcel(xlWorksheet, GetUserName) 'UPPRÄTTAD AV
                WriteSignalTrackPortionDataExcel(xlWorksheet, txtTrackSection.Text, 8) 'Bandel

                'startvärde pekare
                DriftplatserXML = 0

                '---------------------------------------------------------
                ' Skapa en XML fil per driftplatsdel utifrån vald bandel
                '---------------------------------------------------------

                Do While DriftplatserXML < SwTmovesArrayStationsPost

                    Console.WriteLine("Driftplats eller linje av totalt antal:  " & DriftplatserXML & "/" & SwTmovesArrayStationsPost)

                    Dim StartRowPost As Integer = 10 'första startrad för rubrik Post
                    Dim FilestringXML As String = "" 'del av sökväg XML-fil som ska leveras

                    'default print till leveransbekräftelse
                    PrintTMoves = False
                    PrintVTool = False

                    '*******************************************************************
                    'Identitetsnummer driftplats eller linje = ArrayStations(DriftplatserXML, 0)
                    'Driftplatsnamn = ArrayStations(DriftplatserXML, 1)
                    'Driftplatssignatur = ArrayStations(DriftplatserXML, 4)
                    'Projektering utför i verktyg = ArrayStations(DriftplatserXML, 6)
                    '************************************************************************

                    'skapa del av sökväg XML-fil leverans
                    FilestringXML = OnlyFileNameProject &
                                    " " & ArrayStations(DriftplatserXML, 1) &
                                    " IDnr-" & ArrayStations(DriftplatserXML, 0) & ".xml"

                    'Projektering utförd i TMoves eller VTool
                    Select Case ArrayStations(DriftplatserXML, 6)

                        Case "SwTmoves"

                            'läs driftplats
                            ArrayStation(DriftplatserXML)

                            'read tablename Movements
                            ReadMovementsdataSwTmooves(ProjectSwTmoves_Database, StationIDBandel)

                            'spara xml i leveransmapp och med filnamn
                            DriftplatsnummerExport = ProjectDirectory &
                                        "\" & OnlyFileNameProject &
                                        "\" & Folder_XMLdelivery &
                                        "\" & FilestringXML

                        Case "VTool"

                            Dim Driftplatsnummer = DriftplatserXML

                            For iXML = Driftplatsnummer To SwTmovesArrayStationsPost - 1

                                Console.WriteLine("-- Skriv ut Driftplatsnummer -- " & iXML)

                                'läs driftplats
                                ArrayStation(iXML)

                                'info user
                                txtXMLFileCreate.Text = "Kopiera in CSV-filer och skapa temporär databas. Projekt: " & ArrayStations(iXML, 1)

                                'stega upp processbar1
                                valueProgressbarXMLExport += barvalue

                                'updatera ProgressBar1
                                Dispatcher.Invoke(updatePbDelegate,
                              System.Windows.Threading.DispatcherPriority.Background,
                              New Object() {ProgressBar.ValueProperty, valueProgressbarXMLExport})

                                'skapa mapp projektering driftplats och kopiera CSV-filer
                                CreateVtoolsMapps(iXML)

                                'avbryt filer saknades
                                If AvbrytXMLGenering = True Then
                                    Exit Do
                                End If

                                'info user
                                txtXMLFileCreate.Text = "Läs i lokala CSV-filer och skapa data i temporär databas: "

                                'updatera ProgressBar1
                                Dispatcher.Invoke(updatePbDelegate,
                              System.Windows.Threading.DispatcherPriority.Background,
                              New Object() {ProgressBar.ValueProperty, valueProgressbarXMLExport})

                                'läs i lokala CSV-filer och skapa data i temporär databas
                                Inmatnign(folder & "\")

                            Next

                            'spara xml i mapp och med filnamn
                            DriftplatsnummerExport = ProjectDirectory &
                                        "\" & OnlyFileNameProject &
                                        "\" & Folder_XMLdelivery

                        Case Else
                            MessageBox.Show("Hittade inget projektering av driftplats/linje som är utför i TMoves eller VTool. Verktyg : " &
                                              ArrayStations(DriftplatserXML, 6), "Projekteringsverktyg", MessageBoxButton.OK, MessageBoxImage.Error)
                    End Select

                    'Kortnamn på Trafikplatssignatur och StationsID
                    Select Case Val_BandelDriftplats

                        Case "Bandel"
                            TrafikplatssignaturSwTmoves(DriftplatserXML) 'Kortnamn
                            StationsIDSwTmoves(DriftplatserXML) 'ID 

                        Case "Driftplats"
                            TrafikplatssignaturSwTmoves(DriftplatserXML) 'Kortnamn
                            StationsIDSwTmoves(DriftplatserXML) 'ID 
                    End Select

                    '---------------------------------------------------------
                    ' Skriv ut ANDA version 1.0 eller 2.0
                    '---------------------------------------------------------
                    Console.WriteLine("-- Skriv ut ANDA version 1.0 eller 2.0 -- ")

                    '*** Skriv ut ANDA version 1.0  ***
                    If rdbdeliveryANDA1_0.IsChecked = True Then

                        Select Case ArrayStations(DriftplatserXML, 6)
                            Case "SwTmoves"

                                'skapa XML-fil. Inga rörelse projekterad utförd på annan driftplats
                                If SwTmovesArrayMovementsPost >= 1 Then

                                    'info user
                                    txtXMLFileCreate.Text = "SwTmoves. Skapande av XML-fil driftplats " & ArrayStations(DriftplatserXML, 1) & " pågår."

                                    'updatera ProgressBar1
                                    ProgressBar1.Visibility = Visibility.Visible
                                    Dispatcher.Invoke(updatePbDelegate,
                              System.Windows.Threading.DispatcherPriority.Background,
                              New Object() {ProgressBar.ValueProperty, valueProgressbarXMLExport})

                                    'skriv ut XML- fil
                                    SkrivutSwTmoves(DriftplatsnummerExport, DriftplatserXML, StationID)

                                    'användare begärt avsluta exporten av XML-filer
                                    If AvbrytXMLGenering = True Then
                                        Exit Do
                                    End If

                                End If

                                'Skriv till Leveransbrev
                                If PrintTMoves = True Then
                                    Console.WriteLine("Skriv till leveransbrev levernas XML-fil SwTmoves. Driftplats :" & ArrayStations(DriftplatserXML, 1))

                                    xlWorksheet = ProofOfDelivery_xlWorkBook.Worksheets("Leverans")
                                    WriteSignalPosDataExcel(xlWorksheet, postleveransbrev, StartRowPost + postleveransbrev)
                                    WriteSignalStationDataExcel(xlWorksheet, ArrayStations(DriftplatserXML, 0), StartRowPost + postleveransbrev)
                                    WriteSignalFileXMLDataExcel(xlWorksheet, FilestringXML, StartRowPost + postleveransbrev)
                                    ProofOfDelivery_xlWorkBook.Save()
                                    postleveransbrev += 1

                                    'spara data i Kvalitetskontroll
                                    QualityControl_xlWorkBook.Save()
                                End If


                            Case "VTool"

                                'skriv ut valt ställverk och dess olika driftplatser/linjer
                                For Each Driftplats As VToolsInmatningDataSet.DriftplatserRow In VtoolsDataSet.Driftplatser

                                    Dim filenameVTool As String = Date.Today & " " &
                                            Driftplats.plnamn &
                                            " IDnr-" & Driftplats.plnr & ".xml"

                                    Dim filePathVTools As String = DriftplatsnummerExport & "\" &
                                            filenameVTool

                                    'Skriv till Leveransbrev om det är en ställverkstyp definerad för driftplats
                                    If Driftplats.stlvtyp <> "-" Then

                                        'info user
                                        txtXMLFileCreate.Text = "VTool. Skapande av XML-fil driftplats " & Driftplats.plnamn & " pågår."

                                        'updatera ProgressBar1
                                        Dispatcher.Invoke(updatePbDelegate,
                                        System.Windows.Threading.DispatcherPriority.Background,
                                        New Object() {ProgressBar.ValueProperty, valueProgressbarXMLExport})

                                        'skriv ut XML fil
                                        SkrivutVtoolsData(filePathVTools, Driftplats, folder)

                                        Console.WriteLine("Skriv till leveransbrev levernas XML-fil VTools. Driftplatsnummer :" & Driftplats.plnr &
                                                          " Driftplatsnamn: " & Driftplats.plnamn)

                                        xlWorksheet = ProofOfDelivery_xlWorkBook.Worksheets("Leverans")
                                        WriteSignalPosDataExcel(xlWorksheet, postleveransbrev, StartRowPost + postleveransbrev)
                                        WriteSignalStationDataExcel(xlWorksheet, Driftplats.plnr, StartRowPost + postleveransbrev)
                                        WriteSignalFileXMLDataExcel(xlWorksheet, filenameVTool, StartRowPost + postleveransbrev)
                                        WriteSignalTypeOfProjectDataExcel(xlWorksheet, Driftplats.stlvtyp & "." & Driftplats.pltyp, StartRowPost + postleveransbrev)
                                        ProofOfDelivery_xlWorkBook.Save()
                                        postleveransbrev += 1

                                        'återställ VTools redovisning leveransbrev
                                        PrintVTool = False

                                        'rörelsevägar
                                        Driftplats.GetRörelsevägarRows()

                                    End If

                                Next

                                'samtliga XML-filer skapade. avbryt fortsatt inläsning/skapande
                                Console.WriteLine("-- samtliga XML-filer skapade. avbryt fortsatt inläsning/skapande -- ")
                                Exit Do

                            Case Else
                                MessageBox.Show("Hittade inget verktyg för utförd projektering. Verktyg : " & ArrayStations(DriftplatserXML, 6),
                                                    "Generering XML", MessageBoxButton.OK, MessageBoxImage.Error)
                        End Select


                        '*** Skriv ut ANDA version 2.0  ***
                    ElseIf rdbdeliveryANDA2_0.IsChecked = True Then
                        MessageBox.Show("Vald leverans kan inte utföras, under utveckling.", "Generering XML", MessageBoxButton.OK, MessageBoxImage.Warning)
                    End If

                    'ProgressBar1
                    valueProgressbarXMLExport += barvalue

                    'loop
                    DriftplatserXML += 1

                    Console.WriteLine(" ----- Loop nr :" & DriftplatserXML)
                Loop

                '----------------------------------------------
                '  Alla driftplatser och linjer är behandlade
                '----------------------------------------------

                'skriv data till Kvalitetskontroll. slutrad verifiering
                If ck_SwTmoves.IsChecked = True Then
                    WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, "000000", "", 10000, "Slutrad verifiering",
                                                        QualityControl_Counter)
                End If

                'SwTmoves databas close ANDA
                If ck_SwTmoves.IsChecked = True Then
                    Console.WriteLine("SwTmoves databas close ANDA.")
                    CloseDatabasSwTmoves(ProjectSwTmoves_Database)
                End If

                'spara leveransbrev i pdf format
                SaveExcel2013ToPDF(ProjectDirectory & "\" & OnlyFileNameProject, "Leveransbrev " & OnlyFileNameProject)

                'Save and close Excel. 
                ExcelSaveQuit()

                'mapp XML Leverans görs till en Zip-fil
                ZipFile.CreateFromDirectory(ProjectDirectory & "\" & OnlyFileNameProject & "\" & Folder_XMLdelivery,
                                                          ProjectDirectory & "\" & OnlyFileNameProject & "\" & Folder_XMLdelivery &
                                                               " " & OnlyFileNameProject & ".zip")

                'skicka till Microsoft Outlook 2013 ANDA 1.0
                If rdbdeliveryANDA1_0.IsChecked = True Then

                    If OutlookDeliveryLetterPDF = True Or OutlookXMLFiles = True Or OutlookXMLDeliveryFilesZIP = True Then
                        SendMailToMicrosoftOutlook2013("Leverans av XML-filer enligt ANDA 1.0  " &
                                                       vbCrLf & OnlyFileNameProject & vbCrLf & vbCrLf & vbCrLf,
                                                      ProjectDirectory & "\" & OnlyFileNameProject & "\",
                                                       ProjectDirectory & "\" & OnlyFileNameProject & "\" & Folder_XMLdelivery & "\")
                    End If
                End If

                'updatera ProgressBar1
                Dispatcher.Invoke(updatePbDelegate,
                              System.Windows.Threading.DispatcherPriority.Background,
                              New Object() {ProgressBar.ValueProperty, valueProgressbarXMLExport})


                'flytta temp till vald mapp
                MoveDirectory(ProjectDirectory & "\" & OnlyFileNameProject,
                              OrginalProjectDirectory & "\" & OnlyFileNameProject, True)

                'info user
                txtXMLFileCreate.Text = "Genrering klart"

                'uppdatera grafiska knappar
                Stopvariablerknappar()

                Console.WriteLine("--------- Skapande av leverans klart. Väntar på användare ska välja en ny körning. ----------")
                Console.WriteLine("")

                MessageBox.Show("Export ANDA klart och sparat i filer med signaltur driftplatsdel och datum. " &
                               vbCrLf & vbCrLf & "Projektplats :" & vbCrLf &
                                OrginalProjectDirectory & "\" & OnlyFileNameProject,
                                "Generering XML", MessageBoxButton.OK, MessageBoxImage.Information)

                ProgressBar1.Visibility = Visibility.Hidden
            End If

        Catch ex As Exception
            DisplayError(ex, "Skriv ut XML-filer för vald bandel.")

            'progressbar1
            ProgressBar1.Visibility = Visibility.Hidden
        End Try
    End Sub
#End Region

#Region "Progressbar 1 och uppdateringar + text"
    Public Sub ProgressBarDropFileUpdateTxtXML(ByVal valueProgressbarXMLExport As Double, ByVal txtXML As String)

        'updatera text
        txtXMLFileCreate.Text = txtXML

        Dim updatePbDelegate As New UpdateProgressBarDelegate(AddressOf ProgressBar1.SetValue)

        'updatera ProgressBar1
        Dispatcher.Invoke(updatePbDelegate,
                      Threading.DispatcherPriority.Background,
                      New Object() {ProgressBar.ValueProperty, valueProgressbarXMLExport})
    End Sub

    Public Sub ProgressBarDropFileUpdate(ByVal valueProgressbarXMLExport As Double)

        Dim updatePbDelegate As New UpdateProgressBarDelegate(AddressOf ProgressBar1.SetValue)

        'updatera ProgressBar1
        Dispatcher.Invoke(updatePbDelegate,
                      Threading.DispatcherPriority.Background,
                      New Object() {ProgressBar.ValueProperty, valueProgressbarXMLExport})
    End Sub
#End Region

    'spara excelfil till PDF
    Private Sub SaveExcel2013ToPDF(ByVal SaveToMapp As String, ByVal filenamnePFD As String)

        Try
            ProofOfDelivery_xlWorkBook.ExportAsFixedFormat(XlFixedFormatType.xlTypePDF, SaveToMapp & "\" & filenamnePFD & ".pdf", XlFixedFormatQuality.xlQualityStandard)

        Catch ex As Exception
            DisplayError(ex, "Skriv ut Leveransbrev till PDF avbryten. ")
        End Try
    End Sub

    'sänd mail till Microsoft Outlook 2013
    Private Sub SendMailToMicrosoftOutlook2013(ByVal Message As String, ByVal DirectoryPfd As String, ByVal DirectoryXML As String)

        Dim objMissing As Object = Type.Missing
        Dim objOutlook As Outlook.Application = Nothing
        Dim objNS As Outlook.NameSpace = Nothing
        Dim objMail As Outlook.MailItem = Nothing

        Dim SwecoAB As String = "Sweco Rail AB" & vbCrLf &
            "Humanistgatan 2 B" & vbCrLf &
            "Box 874" & vbCrLf &
            "SE-781 28  Borlänge" & vbCrLf &
            "Telefon  +46 8 695 60 00" & vbCrLf &
            "www.sweco.se"

        Console.WriteLine(" Sänd till Microsoft Outlook 2013 pdf-fil " & DirectoryPfd)
        Console.WriteLine(" Sänd till Microsoft Outlook 2013 XML-filer " & DirectoryXML)


        Try
            ' Start Microsoft Outlook and log on with your profile.
            ' Create an Outlook application.
            objOutlook = New Outlook.Application()

            ' Get the namespace
            objNS = objOutlook.GetNamespace("MAPI")

            ' Log on by using a dialog box to choose the profile.
            objNS.Logon(objMissing, objMissing, False, False)

            ' create an email message
            objMail = CType(objOutlook.CreateItem(Outlook.OlItemType.olMailItem), Outlook.MailItem)

            ' Set the properties of the email.
            With objMail
                .Subject = "Leverans XML-filer ANDA 1.0 " & OnlyFileNameProject
                .To = "jens.rigtorp@trafikverket.se; mats.matsson@trafikverket.se;"
                '.To = "thomas.west@sweco.se; jens.rigtorp@trafikverket.se; mats.matsson@trafikverket.se"
                '.CC = "kristina.karlsson@sweco.se"

                'bifoga leveransfil i pdf
                If OutlookDeliveryLetterPDF = True Then

                    'sök pdf filer i mapp
                    For Each foundFilePfd As String In My.Computer.FileSystem.GetFiles(
                        DirectoryPfd,
                        FileIO.SearchOption.SearchTopLevelOnly, "*.pdf")
                        .Attachments.Add(foundFilePfd)
                    Next
                End If

                'bifoga XML filer
                If OutlookXMLFiles = True Then

                    'sök xlm filer i mapp
                    For Each foundFileXml As String In My.Computer.FileSystem.GetFiles(
                        DirectoryXML,
                        FileIO.SearchOption.SearchTopLevelOnly, "*.xml")
                        .Attachments.Add(foundFileXml)
                    Next
                End If

                'bifoga Zip fil innehållande XML-filer
                If OutlookXMLDeliveryFilesZIP = True Then

                    'sök zip filer i mapp
                    For Each foundFileXmlZip As String In My.Computer.FileSystem.GetFiles(
                        DirectoryPfd,
                        FileIO.SearchOption.SearchTopLevelOnly, "*.zip")
                        .Attachments.Add(foundFileXmlZip)
                    Next
                End If

                .Body = Message & SwecoAB

                'Skicka e-post direkt utan öppna Outlook
                If OutlookNewSend = True Then
                    .Send()
                Else
                    .Display(True)
                End If

            End With

        Catch ex As Exception

            MessageBox.Show(ex.Message,
                    "Ett undantag är uppstod i koden för tillägget.")

            'manuellt städa upp efter COM
        Finally

            If Not objMail Is Nothing Then
                Marshal.FinalReleaseComObject(objMail)
                objMail = Nothing
            End If

            If Not objNS Is Nothing Then
                Marshal.FinalReleaseComObject(objNS)
                objNS = Nothing
            End If

            If Not objOutlook Is Nothing Then
                Marshal.FinalReleaseComObject(objOutlook)
                objOutlook = Nothing
            End If

        End Try
    End Sub


#Region "Skapa projektmapp VTools och kopier alla projektfiler"
    Private Sub CreateVtoolsMapps(ByVal StationsPost As Integer)

        'ArrayStations(StationsPost, 1) = Driftplatsnamn
        'ArrayStations(StationsPost, 4) = Signatur driftplatsnamn
        'ArrayStations(StationsPost, 7) = Sökväg till Projekt VTools projektering

        Dim VToolsProjectMappStation As String = ArrayStations(StationsPost, 7) 'Projektmapp VTools projektering
        Dim DatabaseVTools As String = ArrayStations(StationsPost, 4) & ".mdb" 'Databas VTools
        Dim DMIVTools As String = ArrayStations(StationsPost, 1) & ".txt" 'DMI grafisk uppritning

        'projektmapp leverans
        folder = ProjectDirectory & "\" & OnlyFileNameProject & "\Projekteringsunderlag\" & ArrayStations(StationsPost, 1)

        'create Defult project catalog Project VTools
        If Not ProjectBISCatalog_Root(folder & "\") = True Then
            AvbrytXMLGenering = True
            Exit Sub
        End If

        'Copy FrontProtection
        If Not ProofOfDelivery_ToCopyAlways(VToolsProjectMappStation & "\" & importFrontProtection, folder & "\" & importFrontProtection) = True Then
            AvbrytXMLGenering = True
            Exit Sub
        End If

        'Copy RoadParts
        If Not ProofOfDelivery_ToCopyAlways(VToolsProjectMappStation & "\" & importRoadParts, folder & "\" & importRoadParts) = True Then
            AvbrytXMLGenering = True
            Exit Sub
        End If

        'Copy RoadsOfRoadParts
        If Not ProofOfDelivery_ToCopyAlways(VToolsProjectMappStation & "\" & importRoadsOfRoadParts, folder & "\" & importRoadsOfRoadParts) = True Then
            AvbrytXMLGenering = True
            Exit Sub
        End If

        'Copy ConflictingRoutes_tv
        If Not ProofOfDelivery_ToCopyAlways(VToolsProjectMappStation & "\" & importConflictingRoutes_tv, folder & "\" & importConflictingRoutes_tv) = True Then
            AvbrytXMLGenering = True
            Exit Sub
        End If

        'Copy ConflictingRoutes_vv. Ej aktuell för ANDA version 1.0
        'If Not ProofOfDelivery_ToCopySelected(VToolsProjectMappStation & "\" & importConflictingRoutes_vv, folder & "\" & importConflictingRoutes_vv) = True Then
        '    AvbrytXMLGenering = True
        '    Exit Sub
        'End If

        'Copy TC_T1_Mapping
        If Not ProofOfDelivery_ToCopyAlways(VToolsProjectMappStation & "\" & importTC_T1_Mapping, folder & "\" & importTC_T1_Mapping) = True Then
            AvbrytXMLGenering = True
            Exit Sub
        End If


        If System.IO.File.Exists(VToolsProjectMappStation & "\" & importBacktoBack) = True Then
            If Not ProofOfDelivery_ToCopyAlways(VToolsProjectMappStation & "\" & importBacktoBack, folder & "\" & importBacktoBack) = True Then
                AvbrytXMLGenering = True
                Exit Sub
            End If
        End If 'Copy BacktoBack


        'Copy BIS_pl_config
        If Not ProofOfDelivery_ToCopyAlways(VToolsProjectMappStation & "\" & importBIS_pl_config, folder & "\" & importBIS_pl_config) = True Then
            AvbrytXMLGenering = True
            Exit Sub
        End If

        'Copy Database Vtools
        If Not ProofOfDelivery_ToCopyAlways(VToolsProjectMappStation & "\" & DatabaseVTools, folder & "\" & DatabaseVTools) = True Then
            AvbrytXMLGenering = True
            Exit Sub
        End If

        'Copy DMI Vtools
        If Not ProofOfDelivery_ToCopyAlways(VToolsProjectMappStation & "\" & DMIVTools, folder & "\" & DMIVTools) = True Then
            AvbrytXMLGenering = True
            Exit Sub
        End If
    End Sub
#End Region

#Region "Läs in alla stationer/linjer i array som sedan ska skrivas ut i XML"
    Private Sub ReadAllStationToArray_SwTmoves()

        '-- läs av bandel/driftplats --
        Select Case Val_BandelDriftplats

            Case "Bandel"
                txtXMLFileCreate.Text = "Läser av vilka driftplatser/linjer som ingår i vald bandel."

                'projekterad i SwTmoves
                ReadStationsdataSwTmoves(ProjectSwTmoves_Database, txtTrackSection.Text)



                'finns inga driftplatser på bandel
                If SwTmovesArrayStationsPost <= 0 Then
                    MessageBox.Show("Finns inga driftplatser på vald bandel " & txtTrackSection.Text, "Bandel", MessageBoxButton.OK)
                    Exit Sub
                End If

            Case "Driftplats"
                txtXMLFileCreate.Text = "Läser av vald driftplatsnummer."

                'projekterad i SwTmoves
                ReadOnlyStationsdataSwTmoves(ProjectSwTmoves_Database, txtStationId.Text)

                'finns inga driftplatsnummer
                If SwTmovesArrayStationsPost <= 0 Then
                    MessageBox.Show("Vald driftplatsnummer kan ej hittas " & txtStationId.Text, "Driftplatsnummer", MessageBoxButton.OK)
                    Exit Sub
                End If

                'TrackSection first
                txtTrackSection.Text = ArrayStations(0, 5)
        End Select
    End Sub

    Private Sub ReadAllStationToArray_VTools()

        '-- läs av bandel/driftplats --
        Select Case Val_BandelDriftplats

            Case "Bandel"

                'projekterad i VTools
                ReadStationsdataVTools(txtTrackSection.Text)

                'finns inga driftplatser på bandel
                If SwTmovesArrayStationsPost <= 0 Then
                    MessageBox.Show("Finns inga driftplatser på vald bandel " & txtTrackSection.Text, "Bandel", MessageBoxButton.OK)
                    Exit Sub
                End If

            Case "Driftplats"

                'projekterad i VTools
                ReadOnlyStationsdataVTools(txtStationId.Text)

                'finns inga driftplatsnummer
                If SwTmovesArrayStationsPost <= 0 Then
                    MessageBox.Show("Vald driftplatsnummer kan ej hittas " & txtStationId.Text, "Driftplatsnummer", MessageBoxButton.OK)
                    Exit Sub
                End If

                'TrackSection first
                txtTrackSection.Text = ArrayStations(0, 5)
        End Select
    End Sub
#End Region


#Region "Kopiera databaser till Projekt"
    Private Sub CopyDatabasFileProject()

        ' utskrift från verktyg SwTmoves
        If ck_SwTmoves.IsChecked = True Then

            'kopiera databas ANDA för SwTmoves
            newdatabasSwTmoves = ProjectDirectory & "\" & OnlyFileNameProject & "\Projekteringsunderlag" & "\Anda.accdb"
            If Not ProofOfDelivery_ToCopyAlways(CopyDatabasSwTmoves, newdatabasSwTmoves) = True Then
                AvbrytXMLGenering = True
                ck_SwTmoves.IsChecked = False
                Exit Sub
            End If

            'Data Source databas ANDA
            ProjectSwTmoves_Database = OleDbConnection_SwTmovesDatabase & newdatabasSwTmoves
        End If
    End Sub
#End Region

#Region "Kopiera Excel filer till projekt"
    Private Sub CopyExcelFileProject()

        'sökväg leveransbrev BIS
        newfileProofOfDelivery = ProjectDirectory & "\" & OnlyFileNameProject & "\Leveransbrev " & OnlyFileNameProject & ".xlsx"

        'sökväg Kvalitetskontroll
        QualityControl_ExcelFile = ProjectDirectory & "\" & OnlyFileNameProject & "\Kvalitetskontroll\Kvalitetskontroll " & OnlyFileNameProject & ".xlsx"

        'sökväg platser bandel VTools
        NewListOperationSite = ProjectDirectory & "\" & OnlyFileNameProject & "\Projekteringsunderlag" & "\Platser bandel VTools.xlsx"


        '-- Utskrift från verktyg VTool och TMoves  --
        If ck_VToolsOnly.IsChecked = True And ck_SwTmoves.IsChecked = True Then

            'leveransbrev BIS
            If Not ProofOfDelivery_ToCopyAlways(ProofOfDelivery, newfileProofOfDelivery) = True Then
                Exit Sub

            Else
                'öppna excel leveransbrev BIS
                ProofOfDelivery_xlWorkBook = xlApp.Workbooks.Open(newfileProofOfDelivery)
            End If

            'Kvalitetskontroll
            If Not ProofOfDelivery_ToCopyAlways(QualityControl_ExcelFileTemplate, QualityControl_ExcelFile) = True Then
                Exit Sub

            Else
                'öppna excel Kvalitetskontroll och flik Verifiering
                QualityControl_xlWorkBook = xlApp.Workbooks.Open(QualityControl_ExcelFile)
                QualityControl_xlWorksheet = QualityControl_xlWorkBook.Worksheets("Verifiering")
            End If

            'platser bandel VTools
            If Not ProofOfDelivery_ToCopyAlways(CopyListOperationSite, NewListOperationSite) = True Then
                Exit Sub

            Else
                'öppna platser bandel VTools
                ListOperationSite_xlWorkBook = xlApp.Workbooks.Open(NewListOperationSite)
                ListOperationSite_xlWorksheet = ListOperationSite_xlWorkBook.Worksheets("Resultat")
            End If
        End If

        '-- Utskrift endast från verktyg TMoves --
        If ck_SwTmoves.IsChecked = True And ck_VToolsOnly.IsChecked = False Then

            'leveransbrev BIS
            If Not ProofOfDelivery_ToCopyAlways(ProofOfDelivery, newfileProofOfDelivery) = True Then
                Exit Sub

            Else
                'öppna excel leveransbrev BIS
                ProofOfDelivery_xlWorkBook = xlApp.Workbooks.Open(newfileProofOfDelivery)
            End If

            'Kvalitetskontroll
            If Not ProofOfDelivery_ToCopyAlways(QualityControl_ExcelFileTemplate, QualityControl_ExcelFile) = True Then
                Exit Sub

            Else
                'öppna excel Kvalitetskontroll och flik Verifiering
                QualityControl_xlWorkBook = xlApp.Workbooks.Open(QualityControl_ExcelFile)
                QualityControl_xlWorksheet = QualityControl_xlWorkBook.Worksheets("Verifiering")
            End If
        End If

        '-- Utskrift endast från verktyg VTool --
        If ck_VToolsOnly.IsChecked = True And ck_SwTmoves.IsChecked = False Then

            'leveransbrev BIS
            If Not ProofOfDelivery_ToCopyAlways(ProofOfDelivery, newfileProofOfDelivery) = True Then
                Exit Sub

            Else
                'öppna excel leveransbrev BIS
                ProofOfDelivery_xlWorkBook = xlApp.Workbooks.Open(newfileProofOfDelivery)
            End If

            'platser bandel VTools
            If Not ProofOfDelivery_ToCopyAlways(CopyListOperationSite, NewListOperationSite) = True Then
                Exit Sub

            Else
                'öppna platser bandel VTools
                ListOperationSite_xlWorkBook = xlApp.Workbooks.Open(NewListOperationSite)
                ListOperationSite_xlWorksheet = ListOperationSite_xlWorkBook.Worksheets("Resultat")
            End If
        End If

    End Sub
#End Region

#Region "Stäng fil ExportSignaldataToBIS"
    Private Sub ExportSignaldataToBIS_Closed(sender As Object, e As EventArgs)
        Me.Close()
    End Sub
#End Region

#Region "Meny/Kommando Stäng fil"
    Private Sub MnFileClose_Click(sender As Object, e As RoutedEventArgs)
        Me.Close()
    End Sub
#End Region

#Region "Meny/Kommando Anläggningsdata"
    Private Sub MnRepRecreatePlantData_Click(sender As Object, e As RoutedEventArgs)
        Dim MRecreatePlantData As New RecreatePlantData
        MRecreatePlantData.ShowDialog()
    End Sub
#End Region

#Region "Meny/Granska"
    'Jämför XML-filer
    Private Sub CompareXMLFiles_Click(sender As Object, e As RoutedEventArgs)

    End Sub

    'visa leverad XML-fil
    Private Sub ReviewXMLFiles_Click(sender As Object, e As RoutedEventArgs)
        Dim ReviewXMLFilesANDA1 As New ReviewXMLFilesANDA
        ReviewXMLFilesANDA1.ShowDialog()
    End Sub

    'Öppna lokal databas VToolsInmatning
    Private Sub ReviewLocalDatabasVToolsInmatning_Click(sender As Object, e As RoutedEventArgs)
        Dim LokalDisk As String = "C:\Users"
        Dim LokalAnvändare As String = GetUserName()
        'Dim LokalMapp As String = "AppData\Local\Apps\2.0\Data\WW9H6OHK.OXW\43TAZ57Q.T3J\swex..tion_0000000000000000_0000.0001_94a38fa318fccbe3\Data" 'old
        Dim LokalMapp As String = "AppData\Local\Apps\2.0\Data\WW9H6OHK.OXW\43TAZ57Q.T3J\swex..tion_0000000000000000_0000.0001_771356ee45551863\Data"
        Dim LokalDatabas As String = "VToolsInmatning.accdb"

        'öppna databas
        Process.Start(LokalDisk & "\" & LokalAnvändare & "\" & LokalMapp & "\" & LokalDatabas)
    End Sub
#End Region

#Region "Meny/Hjälp"

    'Lathund exportverktyg XML-filer från VTool till Trafikverket
    Private Sub MnHelpManual_Click(sender As Object, e As RoutedEventArgs)
        Process.Start(LathundExportverktygXML)
    End Sub
#End Region

    Private Sub HandleVägDelarRowUpdated(ByVal sender As Object, ByVal e As OleDbRowUpdatedEventArgs)
        If e.Status = System.Data.UpdateStatus.Continue AndAlso e.StatementType = System.Data.StatementType.Insert Then
            e.Row("ID") = Int32.Parse(VtoolsDataAdaper.VägdelarTableAdapter.ScalarQuery.ToString)
            e.Row.AcceptChanges()
            'e.tatus = UpdateStatus.SkipCurrentRow
        End If
    End Sub

    Public Sub HandleRörelserRowUpdated(ByVal sender As Object, ByVal e As OleDbRowUpdatedEventArgs)
        If e.Status = System.Data.UpdateStatus.Continue AndAlso e.StatementType = System.Data.StatementType.Insert Then
            e.Row("ID") = Int32.Parse(VtoolsDataAdaper.RörelsevägarTableAdapter.ScalarQuery.ToString)
            e.Row.AcceptChanges()
            'e.tatus = UpdateStatus.SkipCurrentRow
        End If
    End Sub

    Public Sub HandleBisIDRowUpdated(ByVal sender As Object, ByVal e As OleDbRowUpdatedEventArgs)
        If e.Status = System.Data.UpdateStatus.Continue AndAlso e.StatementType = System.Data.StatementType.Insert Then
            e.Row("ID") = Int32.Parse(VtoolsDataAdaper.BisObjektTableAdapter.ScalarQuery.ToString)
            e.Row.AcceptChanges()
            'e.tatus = UpdateStatus.SkipCurrentRow
        End If
    End Sub

    Public Sub HandleKonfliktIDRowUpdated(ByVal sender As Object, ByVal e As OleDbRowUpdatedEventArgs)
        If e.Status = System.Data.UpdateStatus.Continue AndAlso e.StatementType = System.Data.StatementType.Insert Then
            e.Row("ID") = Int32.Parse(VtoolsDataAdaper.KonflikterTableAdapter.ScalarQuery.ToString)
            e.Row.AcceptChanges()
            'e.tatus = UpdateStatus.SkipCurrentRow
        End If
    End Sub

    Public Sub HandleFrontskyddIDRowUpdated(ByVal sender As Object, ByVal e As OleDbRowUpdatedEventArgs)
        If e.Status = System.Data.UpdateStatus.Continue AndAlso e.StatementType = System.Data.StatementType.Insert Then
            e.Row("ID") = Int32.Parse(VtoolsDataAdaper.FrontSkyddTableAdapter.ScalarQuery.ToString)
            e.Row.AcceptChanges()
            'e.tatus = UpdateStatus.SkipCurrentRow
        End If
    End Sub

    Public Sub HandleSlutpunkterRowUpdated(ByVal sender As Object, ByVal e As OleDbRowUpdatedEventArgs)
        If e.Status = System.Data.UpdateStatus.Continue AndAlso e.StatementType = System.Data.StatementType.Insert Then
            e.Row("ID") = Int32.Parse(VtoolsDataAdaper.SlutpunkterTableAdapter.ScalarQuery.ToString)
            e.Row.AcceptChanges()
            'e.tatus = UpdateStatus.SkipCurrentRow
        End If
    End Sub

    Public Sub HandleBacktobackRowUpdated(ByVal sender As Object, ByVal e As OleDbRowUpdatedEventArgs)
        If e.Status = System.Data.UpdateStatus.Continue AndAlso e.StatementType = System.Data.StatementType.Insert Then
            e.Row("ID") = Int32.Parse(VtoolsbacktobackDataAdapter.ScalarQuery.ToString)
            e.Row.AcceptChanges()
            'e.tatus = UpdateStatus.SkipCurrentRow
        End If
    End Sub

    Private Sub MnOpenProjectVTool_Click(sender As Object, e As RoutedEventArgs)
        Dim OpenProjectVtool1 As New OpenProjectVtool()
        OpenProjectVtool1.ShowDialog()
    End Sub

    Private Sub MnWritePDF_Click(sender As Object, e As RoutedEventArgs)

    End Sub



End Class
