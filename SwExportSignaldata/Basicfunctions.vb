﻿Imports System.IO

Module Basicfunctions

#Region "Public"
    Public MainFolder As String = "C:\temp\SwecoExportSignalDataXML"
    Public VtoolsDataSet As VToolsInmatningDataSet
    Public VtoolsDataAdaper As VToolsInmatningDataSetTableAdapters.TableAdapterManager
    Public VtoolsbacktobackDataAdapter As VToolsInmatningDataSetTableAdapters.BacktoBackTableAdapter
#End Region

#Region "Private"
    Private ReadOnly ReportFileName As String = "Report.txt"
    Private ReadOnly ChangeLog As String = "ChangeLog.txt"
#End Region

#Region "Error Report"
    'Create Templates directory
    Public Sub CreateErrorReportdirectory()
        'create Licens directory
        Dim di As DirectoryInfo = New DirectoryInfo(MainFolder & "\")
        Try
            ' Determine whether the directory exists.
            If di.Exists Then
                Return
            End If

            ' Try to create the directory.
            di.Create()

        Catch e As Exception
            MakeErrorReport("Create directory ErrorReport file. ", e)
            MessageBox.Show("The process failed: {0}", e.ToString())
        End Try
    End Sub

    'Create File
    Public Sub CreateErrorReport()
        Try
            If File.Exists(MainFolder & "\" & ReportFileName) Then
                File.Delete(MainFolder & "\" & ReportFileName)
            End If

            File.Create(MainFolder & "\" & ReportFileName)

        Catch ex As Exception
            Throw New ApplicationException("CreateErrorReportFile: ", ex)
        End Try
    End Sub

    'Error Report
    Public Sub MakeErrorReport(FunctionName As String, ex As System.Exception)
        Dim TxtString As String
        TxtString = FunctionName & " " & ex.Message & " ; Fel loggat av:" & Environment.UserName & " ,datum :" & System.DateTime.Now & Environment.NewLine
        File.AppendAllText(MainFolder & "\" & ReportFileName, TxtString)
    End Sub
#End Region


#Region "MakeErrorReport WithoutEx"
    Public Sub MakeErrorReport_WithoutEx(FunctionName As String, ErrorString As String)
        Dim TxtString As String
        TxtString = FunctionName & " " & ErrorString & " ; Fel loggat av:" & Environment.UserName & " ,datum :" & System.DateTime.Now & Environment.NewLine
        File.AppendAllText(MainFolder & "\" & ReportFileName, TxtString)
    End Sub
#End Region

#Region "MakeLogUpdate"
    Public Sub MakeLogUpdate(Action As String)
        Dim TxtString As String
        TxtString = Action & Environment.NewLine
        File.AppendAllText(MainFolder & "\" & ChangeLog, TxtString)
    End Sub
#End Region

    ''' <summary>
    ''' Funktion som hittar en given symbol och delar strängen i två delar 
    ''' Första delen retuneras och andra delen sparas i invariabeln
    ''' </summary>
    ''' <param name="Rad">
    ''' Rad innehåller strängen som skall genomsökas
    ''' Efter funktionen innehåller Rad strängen som följer Symbol
    ''' </param>
    ''' <param name="Symbol">
    ''' Symbol innehåller det tecken som skall letas efter
    ''' </param>
    ''' <returns>
    ''' Retunerar strängen fram till teknet i Symbol
    ''' </returns>
    ''' <remarks></remarks>
    Public Function FindInString(ByRef Rad As String, ByVal Symbol As String)
        Dim pos As Integer
        pos = Strings.InStr(1, Rad, Symbol)
        If pos > 0 Then
            FindInString = Trim(Mid(Rad, Len(Symbol), pos - Len(Symbol)))
            Rad = Trim(Mid(Rad, pos + Len(Symbol), Rad.Length))
        Else
            FindInString = Rad
            Rad = ""
        End If
    End Function


End Module
