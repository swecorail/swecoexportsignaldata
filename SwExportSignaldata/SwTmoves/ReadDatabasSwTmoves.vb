﻿Imports System.Data
Imports System.Data.OleDb


Module ReadDatabasSwTmoves


#Region " Public"

    Public Trafikplatssignatur As String ' BIS driftplatssignatur
    Public Rörelsevägstyp As String 'typ av rörelse
    Public BandelsnummerTemp As String 'Tempvariabel för interna kontroller

    'driftplatser per bandel
    Public DriftplatserXML As Integer = 0 'XML antal driftplatser som ska utvärderas
    Public StationIDBandel As Integer 'driftplatsnummer

    'rörelseväg börjanpunkt
    Public BpBisObjektNamnRv As String
    Public BpBisObjektObjektnummerRv As String
    Public BpBisObjekttypnummerRv As String

    Public Viapunkt As Boolean = False

    'rörelseväg slutpunkt
    Public SpBisObjektNamnRv As String
    Public SpBisObjektObjektnummerRv As String
    Public SpBisObjekttypnummerRv As String

    'upplåsningspunkt i rörelseväg
    Public BpBisObjektNamnUpl As String
    Public BpBisObjektObjektnummerUpl As String
    Public BpBisObjekttypnummerUpl As String
    Public SpBisObjektNamnUpl As String
    Public SpBisObjektObjektnummerUpl As String
    Public SpBisObjektObjekttypnummerUpl As String

    'Slutpunkt för alla rörelseer
    Public StationIDFiktiv As String 'fiktivt driftplatsnummer
    Public SpBisObjektNamnSp As String 'objektnamn 21,22...
    Public SpBisObjektObjectType As String 'objekttyp HS..
    Public SpBisObjektMoveType As String 'typ av rörelse tv,vv..
    Public SpTypObjektnamnBP As String 'typ av objektnamn börjapunkt

    Public SlutpunkterAllaXML As Integer = 0 'XML antal slutpunkter som ska utvärderas
    Public SlutpunktXML As Integer = 0 'XML antal slutpunkt som ska utvärderas
    Public SpBisObjektObjektnummerSp As String 'xml objektnummer BIS
    Public SpBisObjektObjekttypnummerSp As String 'xml objekttyp BIS
    Public SpBisProtectDistance As String 'xml skyddsträck BIS
    Public SpBisProtectleght As String 'xml skyddsavstånd BIS
    Public SpBisObjektnummerUpplasning As String 'xml objektnummer BIS Upplåsning
    Public SpBisObjekttypUpplasning As String ''xml objekttyp BIS Upplåsning

    'Slutpunkt för en rörelse
    Public SwTmoveslutpunktXMLPost As Integer 'antal rörelsevägstyper som ska behandlas
    Public SpBisObjektObject_tv As Boolean 'typ av rörelse tågväg
    Public SpBisObjektObject_vv As Boolean 'typ av rörelse växlingsväg
    Public SpBisObjektObject_ftv As Boolean 'typ av rörelse Förenklad tågväg
    Public SpBisObjektObject_stv As Boolean 'typ av rörelse Särskild tågväg
    Public SpBisObjektObject_lib As Boolean 'typ av rörelse Blocksträcka
    Public SpBisObjektObject_tam As Boolean 'typ av rörelse TAMsträcka
    Public SpBisObjektObject_btv As Boolean 'typ av rörelse Bevakad tågväg

    'frontskydd
    Public BisObjektNamnFrontskydd As String 'objektnamn
    Public BisObjektFrontskyddType As String 'objekttyp HS..
    Public TrafikplatssignaturFrontskydd As String ' BIS driftplatsnummer
    Public BisObjektObjektnummerFrontskydd As String 'XML objektnummer BIS 
    Public BisObjektObjekttypnummerFrontskydd As String 'XML objekttyp BIS
    Public BisObjektSkyddsobjektFrontskydd As Boolean 'XML skyddsobjekt BIS
    Public BisObjektLageFrontskydd As Boolean 'XML Läge på objekt BIS, höger,vänster..
    Public FrontskyddBisObjektObjectRorelse As String 'typ av rörelse tv,vv..
    Public FrontskyddXML As Integer = 0 'XML antal front som ska utvärderas

    'Fientliga rörelser mot slutpunkt
    Public FientligaRorelserXML As Integer = 0 'XML antal fientliga rörelser mot slutpunkt som ska utvärderas
    Public FientligaRorelserOmEjID As Integer 'Id nummer om ej
    Public FientligaRorelserID As Integer 'Id nummer fientlig rörelse
    Public BisObjektNamnFientligaRorelser As String 'objektnamn
    Public BisObjektFientligaRorelserType As String 'objekttyp HS..
    Public FientligaRorelserTyp As String 'typ av rörelse tv,vv..

    Public BisObjektObjektnummerientligaRorelser As String 'XML objektnummer BIS 
    Public BisObjektObjekttypnummerientligaRorelser As String 'XML objekttyp BIS

    'Fientliga rörelser mot slutpunkt om ej växel
    Public FientligaRorelserOmEjXML As Integer = 0 'XML antal om ej växel rörelser mot slutpunkt som ska utvärderas
    Public TrafikplatsSignaturFientligaRorelserOmEj As String ' BIS driftplatsignatur
    Public TrafikplatsNummerFientligaRorelserOmEj As Integer ' BIS driftplatsnummer
    Public BisObjektNamnFientligaRorelserOmEj As String 'objektnamn
    Public BisObjektTypFientligaRorelserOmEj As String 'objekttyp
    Public BisObjektTypFientligaRorelserOmEjSp As String 'objekttyp SP

    Public BisObjektIdrörelseFientligaRorelserOmEj As String 'Unikt Id för rörelsevägen
    Public BisObjektLageFientligaRorelserOmEj As Boolean 'XML Läge på objekt BIS, höger,vänster..
    Public BisObjektObjektnummerientligaRorelserOmEj As String 'XML objektnummer BIS 
    Public BisObjektObjekttypnummerientligaRorelserOmEj As String 'XML objekttyp BIS

    Public OrginalNametable As DataTable = New DataTable
    Public OrginalAdapter As OleDb.OleDbDataAdapter
#End Region

#Region " Public Array"
    'Stations
    Public SwTmovesArrayStationsPost As Integer = 0
    Public SwTmovesPeekArrayStations As Integer = 0
    Public ArrayStations(70, 7) As Object

    'Movements
    Public SwTmovesArrayMovementsPost As Integer = 0
    Public SwTmovesPeekArrayMovements As Integer = 0
    Public ArrayMovements(200, 13) As Object

    'EndObj
    Public SwTmovesArrayEndObjPost As Integer = 0 'antal poster
    Public SwTmovesPeekArrayEndObj As Integer = 0 'använder post nr
    Public ArrayEndObje(100, 3) As Object 'storlek array

    'typ av rörelseväg
    Public SwTmovesArrayTypAvRorelsevag As Integer = 0 'antal poster
    Public SwTmovesPeekArrayTypAvRorelsevag As Integer = 0 'använder post nr
    Public ArrayTypAvRorelsevag(50, 4) As Object 'storlek array

    'frontobjekt
    Public SwTmovesArrayTypAvFrontobjekt As Integer = 0 'antal poster
    Public SwTmovesPeekArrayTypAvFrontobjekt As Integer = 0 'använder post nr
    Public ArrayTypAvFrontobjekt(100, 8) As Object 'storlek array

    'Fientliga rörelser mot slutpunkt
    Public SwTmovesArrayFientligaRorelser As Integer = 0 'antal poster
    Public ArrayFientligaRorelser(100, 5) As Object 'storlek array

    'Fientliga rörelser mot slutpunkt om ej växel
    Public SwTmovesArrayFientligaRorelserOmEj As Integer = 0 'antal poster
    Public ArrayFientligaRorelserOmEj(100, 5) As Object 'storlek array

#End Region

#Region "Public Övriga"
    Public StationID As Integer
    Public EndObjFKStObjStID As String
    Public BorjanpunktBisObjectType As String
    Public SlutpunktBisObjectType As String
    Public SlutpunktBisObjectTypeSp As String
    Public BpBisStationsIDUpl As String
    Public BpBisObjectTypeUpl As String
    Public SpBisStationsIDUpl As String
    Public SpTRH As Boolean = False 'rörelseväg tågväg
    Public SpVRK As Boolean = False ' 'rörelseväg växlingsväg

#End Region

#Region "SwTmoves databas close ANDA"
    Public Sub CloseDatabasSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)

        'close the connection to database
        If Project_OleDbCn.State = ConnectionState.Open Then
            Project_OleDbCn.Close()
        End If
    End Sub
#End Region

    Public Sub CloseDBconnection(ByVal closeconnection As OleDbConnection)

        If closeconnection.State = ConnectionState.Connecting Then
            closeconnection.Close()
        End If

    End Sub

#Region "stations objekt identitet"
    Public Sub StationFKStObjStIDSwTmoves(ByVal arraynr As Integer)
        StationIDFiktiv = ArrayMovements(arraynr, 1) 'FKStObjStID
    End Sub
#End Region

#Region "Read array Movements, Post ID. Unik nummer rörelseväg BP-SP"
    Public Function IDMoments(ByVal arraynr As Integer) As String
        Dim id As String
        id = ArrayMovements(arraynr, 11) 'ID
        Return id
    End Function
#End Region

#Region " Kortnamn på Trafikplatssignatur"
    Public Sub TrafikplatssignaturSwTmoves(ByVal arraynr As Integer)
        Trafikplatssignatur = ArrayStations(arraynr, 4) 'ShortName
    End Sub
#End Region

#Region "ArrayStation"
    Public Sub ArrayStation(ByVal driftplatsXML As Integer)
        Console.WriteLine("--- Read Bandel ---")

        StationIDBandel = ArrayStations(driftplatsXML, 0) 'driftplatsnummer

        Console.WriteLine("ID :" & ArrayStations(driftplatsXML, 0))
        Console.WriteLine("StationName :" & ArrayStations(driftplatsXML, 1))
        Console.WriteLine("Type :" & ArrayStations(driftplatsXML, 2))
        Console.WriteLine("Desc :" & ArrayStations(driftplatsXML, 3))
        Console.WriteLine("ShortName :" & ArrayStations(driftplatsXML, 4))
        Console.WriteLine("TrackSection :" & ArrayStations(driftplatsXML, 5))
        Console.WriteLine("Projekteringsverktyg :" & ArrayStations(driftplatsXML, 6))
        Console.WriteLine("Sökväg projekt :" & ArrayStations(driftplatsXML, 7))
        Console.WriteLine("")
    End Sub
#End Region

#Region "SwTmoves databas läs av Driftplatsnummer"
    Public Sub ReadDriftplatsnummerSwTmoves(ByVal connection As String, ByVal TableData As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            Console.WriteLine("--- SQL SwTmoves databas läs av Driftplatsnummer ---")

            'SQL
            Dim FROM_string As String = "Stations"
            Dim WHERE_string As String = "ID" & " = " & TableData & ""
            Dim ORDERBY_string As String = "TrackSection"
            Dim SQLquery As String = "SELECT * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            'read rows in Stations
            Do While R_T_Connection.Read
                ArrayStations(SwTmovesArrayStationsPost, 0) = R_T_Connection.Item("ID")
                ArrayStations(SwTmovesArrayStationsPost, 1) = R_T_Connection.Item("StationName")
                ArrayStations(SwTmovesArrayStationsPost, 2) = If(IsDBNull(R_T_Connection.Item("Type")), String.Empty, R_T_Connection.Item("Type"))
                ArrayStations(SwTmovesArrayStationsPost, 3) = If(IsDBNull(R_T_Connection.Item("Desc")), String.Empty, R_T_Connection.Item("Desc"))
                ArrayStations(SwTmovesArrayStationsPost, 4) = R_T_Connection.Item("ShortName")
                ArrayStations(SwTmovesArrayStationsPost, 5) = R_T_Connection.Item("TrackSection")

                Console.WriteLine("ID:" & ArrayStations(SwTmovesArrayStationsPost, 0) &
                                  " StationName:" & ArrayStations(SwTmovesArrayStationsPost, 1) &
                                  " Type:" & ArrayStations(SwTmovesArrayStationsPost, 2) &
                                  " Desc:" & ArrayStations(SwTmovesArrayStationsPost, 3) &
                                  " ShortName:" & ArrayStations(SwTmovesArrayStationsPost, 4) &
                                  " TrackSection:" & ArrayStations(SwTmovesArrayStationsPost, 5))

                SwTmovesArrayStationsPost += 1
            Loop

            Console.WriteLine("Antal TrackSection " & SwTmovesArrayStationsPost)
            Console.WriteLine("")

            'close the connection
            'Project_OleDbCn.Close()

        Catch ex As Exception
            DisplayError(ex, "Databas läs av Driftplatsnummer.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas läs av Stations"
    Public Sub ReadStationsdataSwTmoves(ByVal connection As String, ByVal TableData As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            Console.WriteLine("--- SwTmoves databas läs av Stations ---")

            'SQL
            Dim FROM_string As String = "Stations"
            Dim WHERE_string As String = "TrackSection" & " = '" & TableData & "'"
            Dim ORDERBY_string As String = "ID"
            Dim SQLquery As String = "SELECT * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            'read rows in Stations
            Do While R_T_Connection.Read
                Console.WriteLine("Stations nr " & SwTmovesArrayStationsPost)

                ArrayStations(SwTmovesArrayStationsPost, 0) = R_T_Connection.Item("ID")
                ArrayStations(SwTmovesArrayStationsPost, 1) = R_T_Connection.Item("StationName")
                ArrayStations(SwTmovesArrayStationsPost, 2) = If(IsDBNull(R_T_Connection.Item("Type")), String.Empty, R_T_Connection.Item("Type"))
                ArrayStations(SwTmovesArrayStationsPost, 3) = If(IsDBNull(R_T_Connection.Item("Desc")), String.Empty, R_T_Connection.Item("Desc"))
                ArrayStations(SwTmovesArrayStationsPost, 4) = R_T_Connection.Item("ShortName")
                ArrayStations(SwTmovesArrayStationsPost, 5) = R_T_Connection.Item("TrackSection")
                ArrayStations(SwTmovesArrayStationsPost, 6) = "SwTmoves"

                Console.WriteLine("ID:" & ArrayStations(SwTmovesArrayStationsPost, 0))
                Console.WriteLine(" StationName:" & ArrayStations(SwTmovesArrayStationsPost, 1))
                Console.WriteLine(" Type:" & ArrayStations(SwTmovesArrayStationsPost, 2))
                Console.WriteLine(" Desc:" & ArrayStations(SwTmovesArrayStationsPost, 3))
                Console.WriteLine(" ShortName:" & ArrayStations(SwTmovesArrayStationsPost, 4))
                Console.WriteLine(" TrackSection:" & ArrayStations(SwTmovesArrayStationsPost, 5))
                Console.WriteLine(" Projekteringsverktyg:" & ArrayStations(SwTmovesArrayStationsPost, 6))
                Console.WriteLine("")
                SwTmovesArrayStationsPost += 1
            Loop

            Console.WriteLine("Antal Stations i verktyg SwTmoves " & SwTmovesArrayStationsPost &
                              ". Totalt antal driftplatser " & SwTmovesArrayStationsPost)
            Console.WriteLine("")

            'close the connection
            Project_OleDbCn.Close()

        Catch ex As Exception
            DisplayError(ex, "Databas läs av Station.")

        End Try

    End Sub
#End Region

#Region "SwTmoves databas läs av endast en Stations"
    Public Sub ReadOnlyStationsdataSwTmoves(ByVal connection As String, ByVal TableData As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            Console.WriteLine("--- SwTmoves databas läs av Stations ---")

            'SQL
            Dim FROM_string As String = "Stations"
            Dim WHERE_string As String = "ID" & " = " & TableData & ""
            Dim ORDERBY_string As String = "TrackSection"
            Dim SQLquery As String = "SELECT * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            'read rows in Stations
            Do While R_T_Connection.Read
                ArrayStations(SwTmovesArrayStationsPost, 0) = R_T_Connection.Item("ID")
                ArrayStations(SwTmovesArrayStationsPost, 1) = R_T_Connection.Item("StationName")
                ArrayStations(SwTmovesArrayStationsPost, 2) = If(IsDBNull(R_T_Connection.Item("Type")), String.Empty, R_T_Connection.Item("Type"))
                ArrayStations(SwTmovesArrayStationsPost, 3) = If(IsDBNull(R_T_Connection.Item("Desc")), String.Empty, R_T_Connection.Item("Desc"))
                ArrayStations(SwTmovesArrayStationsPost, 4) = R_T_Connection.Item("ShortName")
                ArrayStations(SwTmovesArrayStationsPost, 5) = R_T_Connection.Item("TrackSection")
                ArrayStations(SwTmovesArrayStationsPost, 6) = "SwTmoves"
                ArrayStations(SwTmovesArrayStationsPost, 7) = ""

                Console.WriteLine("ID:" & ArrayStations(SwTmovesArrayStationsPost, 0))
                Console.WriteLine(" StationName:" & ArrayStations(SwTmovesArrayStationsPost, 1))
                Console.WriteLine(" Type:" & ArrayStations(SwTmovesArrayStationsPost, 2))
                Console.WriteLine(" Desc:" & ArrayStations(SwTmovesArrayStationsPost, 3))
                Console.WriteLine(" ShortName:" & ArrayStations(SwTmovesArrayStationsPost, 4))
                Console.WriteLine(" TrackSection:" & ArrayStations(SwTmovesArrayStationsPost, 5))
                Console.WriteLine(" Projekteringsverktyg:" & ArrayStations(SwTmovesArrayStationsPost, 6))
                Console.WriteLine(" Sökväg projekt:" & ArrayStations(SwTmovesArrayStationsPost, 7))
                Console.WriteLine("")

                SwTmovesArrayStationsPost += 1
            Loop

            Console.WriteLine("Antal Stations i verktyg SwTmoves :" & SwTmovesArrayStationsPost)
            Console.WriteLine("")

            'close the connection
            Project_OleDbCn.Close()

        Catch ex As Exception
            DisplayError(ex, "Databas läs av endast en Stations.")
        End Try

    End Sub
#End Region

#Region "SwTmoves databas läs av Platsnummer"
    Public Sub ReadPlatsnummerSwTmooves(ByVal connection As String, ByVal TableData As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            Console.WriteLine("--- SwTmoves databas läs av Platsnummer ---")

            'SQL
            Dim FROM_string As String = "Stations"
            Dim WHERE_string As String = "ID" & " = " & TableData & ""
            Dim ORDERBY_string As String = "TrackSection"
            Dim SQLquery As String = "SELECT * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            BandelsnummerTemp = ""

            'read rows in Stations
            Do While R_T_Connection.Read
                BandelsnummerTemp = R_T_Connection.Item("TrackSection")

                Console.WriteLine("ID:" & R_T_Connection.Item("ID") &
                                  " StationName:" & R_T_Connection.Item("StationName") &
                                  " Type:" & If(IsDBNull(R_T_Connection.Item("Type")), String.Empty, R_T_Connection.Item("Type")) &
                                  " Desc:" & If(IsDBNull(R_T_Connection.Item("Desc")), String.Empty, R_T_Connection.Item("Desc")) &
                                  " ShortName:" & R_T_Connection.Item("ShortName") &
                                  " TrackSection:" & R_T_Connection.Item("TrackSection"))
            Loop

            Console.WriteLine("")

            'close the connection
            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadBandelsnummerSwTmooves: ", ex)
            DisplayError(ex, "Databas läs av Platsnummer.")

        End Try
    End Sub
#End Region

#Region "SwTmoves databas läs av Movements"
    Public Sub ReadMovementsdataSwTmooves(ByVal connection As String, ByVal TableData As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand

        Try

            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            'SQL
            Dim FROM_string As String = "Movements"
            Dim WHERE_string As String = "FKStationID " & " = " & TableData & ""
            Dim ORDERBY_string As String = "FKStObjName"
            Dim SQLquery As String = "SELECT * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader


            SwTmovesArrayMovementsPost = 0

            'read rows in Stations
            Do While R_T_Connection.Read

                ArrayMovements(SwTmovesArrayMovementsPost, 0) = R_T_Connection.Item("FKStationID")
                ArrayMovements(SwTmovesArrayMovementsPost, 1) = R_T_Connection.Item("FKStObjStID")
                ArrayMovements(SwTmovesArrayMovementsPost, 2) = R_T_Connection.Item("FKStObjName")
                ArrayMovements(SwTmovesArrayMovementsPost, 3) = R_T_Connection.Item("FKStObjType")
                ArrayMovements(SwTmovesArrayMovementsPost, 4) = R_T_Connection.Item("FKEObjStID")
                ArrayMovements(SwTmovesArrayMovementsPost, 5) = R_T_Connection.Item("FKEObjName")
                ArrayMovements(SwTmovesArrayMovementsPost, 6) = R_T_Connection.Item("FKEObjType")
                ArrayMovements(SwTmovesArrayMovementsPost, 7) = R_T_Connection.Item("MovType")
                ArrayMovements(SwTmovesArrayMovementsPost, 8) = If(IsDBNull(R_T_Connection.Item("UlpName")), String.Empty, R_T_Connection.Item("UlpName"))
                ArrayMovements(SwTmovesArrayMovementsPost, 9) = If(IsDBNull(R_T_Connection.Item("UlpStID")), String.Empty, R_T_Connection.Item("UlpStID"))
                ArrayMovements(SwTmovesArrayMovementsPost, 10) = If(IsDBNull(R_T_Connection.Item("Ulptype")), String.Empty, R_T_Connection.Item("Ulptype"))
                ArrayMovements(SwTmovesArrayMovementsPost, 11) = R_T_Connection.Item("ID")
                ArrayMovements(SwTmovesArrayMovementsPost, 12) = R_T_Connection.Item("ProtectDistance")
                ArrayMovements(SwTmovesArrayMovementsPost, 13) = R_T_Connection.Item("Protectleght")

                Console.WriteLine("FKStationID:" & ArrayMovements(SwTmovesArrayMovementsPost, 0) &
                                      " FKStObjStID:" & ArrayMovements(SwTmovesArrayMovementsPost, 1) &
                                      " FKStObjName:" & ArrayMovements(SwTmovesArrayMovementsPost, 2) &
                                      " FKStObjType:" & ArrayMovements(SwTmovesArrayMovementsPost, 3) &
                                      " FKEObjStID:" & ArrayMovements(SwTmovesArrayMovementsPost, 4) &
                                      " FKEObjName:" & ArrayMovements(SwTmovesArrayMovementsPost, 5) &
                                      " FKEObjType:" & ArrayMovements(SwTmovesArrayMovementsPost, 6) &
                                      " MovType:" & ArrayMovements(SwTmovesArrayMovementsPost, 7) &
                                      " UlpName:" & ArrayMovements(SwTmovesArrayMovementsPost, 8) &
                                      " UlpStID:" & ArrayMovements(SwTmovesArrayMovementsPost, 9) &
                                      " Ulptype:" & ArrayMovements(SwTmovesArrayMovementsPost, 10) &
                                      " ID:" & ArrayMovements(SwTmovesArrayMovementsPost, 11) &
                                      " ProtectDistance:" & ArrayMovements(SwTmovesArrayMovementsPost, 12) &
                                      " Protectleght:" & ArrayMovements(SwTmovesArrayMovementsPost, 13))

                SwTmovesArrayMovementsPost += 1
            Loop

            Project_OleDbCn.Close()

            Console.WriteLine("Antal Movements " & SwTmovesArrayMovementsPost)


        Catch ex As Exception
            DisplayError(ex, "Databas läs av Movement")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas läs av alla Slutpunktsobjekt"
    Public Sub ReadEndObjSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            Console.WriteLine("-- SwTmoves databas läs av alla Slutpunktsobjekt --")

            'SQL
            Dim FROM_string As String = "EndObj"
            Dim WHERE_string As String = "FKSiObjStID" & " = " & StationIDFiktiv & ""
            Dim ORDERBY_string As String = "FKSiObjName"
            Dim SQLquery As String = "SELECT * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            'startvärde
            SwTmovesArrayEndObjPost = 0

            'read rows in ReadEnd
            Do While R_T_Connection.Read
                ArrayEndObje(SwTmovesArrayEndObjPost, 0) = R_T_Connection.Item("FKSiObjStID")
                ArrayEndObje(SwTmovesArrayEndObjPost, 1) = R_T_Connection.Item("FKSiObjName")
                ArrayEndObje(SwTmovesArrayEndObjPost, 2) = R_T_Connection.Item("FKSiObjType")
                ArrayEndObje(SwTmovesArrayEndObjPost, 3) = R_T_Connection.Item("MoveType")

                Console.WriteLine("FKSiObjStID:" & ArrayEndObje(SwTmovesArrayEndObjPost, 0) &
                                  " FKSiObjName:" & ArrayEndObje(SwTmovesArrayEndObjPost, 1) &
                                  " FKSiObjType:" & ArrayEndObje(SwTmovesArrayEndObjPost, 2) &
                                  " MoveType:" & ArrayEndObje(SwTmovesArrayEndObjPost, 3))

                SwTmovesArrayEndObjPost += 1
            Loop

            Console.WriteLine("Antal Slutpunktsobjekt " & SwTmovesArrayEndObjPost)
            Console.WriteLine("")

            'close the connection
            Project_OleDbCn.Close()

        Catch ex As Exception
            DisplayError(ex, "Databas läs av alla Slutpunktsobjekt.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas läs av alla Frontskyddsobjekt"
    Public Sub ReadFrontObjectsSwTmoves(ByVal connection As String, ByVal typrorelse As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            Console.WriteLine("-- SwTmoves databas läs av alla Frontskyddsobjekt för Slutpunkt (SP) och skapa Array --")

            'SQL
            Dim FROM_string As String = "FrontObjects"
            Dim WHERE_string As String = "FKEObjStID " & " = " & StationIDFiktiv &
                                         " AND FKEObjName " & " = '" & SpBisObjektNamnSp &
                                         "' AND FKMoveType " & " = '" & typrorelse &
                                         "' AND FKEObjType " & " = '" & SpBisObjektObjectType & "'"

            Dim ORDERBY_string As String = "FKEObjName"
            Dim SQLquery As String = "SELECT * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            'read rows in ReadEnd
            Do While R_T_Connection.Read
                ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 0) = R_T_Connection.Item("ID")
                ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 1) = R_T_Connection.Item("FKEObjStID")
                ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 2) = R_T_Connection.Item("FKEObjName")
                ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 3) = R_T_Connection.Item("FKEObjType")
                ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 4) = R_T_Connection.Item("FKMoveType")
                ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 5) = R_T_Connection.Item("FKFronProStID")
                ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 6) = R_T_Connection.Item("FKFronProName")
                ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 7) = R_T_Connection.Item("FKFronProType")
                ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 8) = R_T_Connection.Item("FrontProLeg")

                Console.WriteLine("ID:" & ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 0))
                Console.WriteLine("FKEObjStID:" & ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 1))
                Console.WriteLine("FKEObjName:" & ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 2))
                Console.WriteLine("FKEObjType:" & ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 3))
                Console.WriteLine("FKMoveType:" & ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 4))
                Console.WriteLine("FKFronProStID:" & ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 5))
                Console.WriteLine("FKFronProName:" & ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 6))
                Console.WriteLine("FKFronProType:" & ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 7))
                Console.WriteLine("FrontProLeg:" & ArrayTypAvFrontobjekt(SwTmovesArrayTypAvFrontobjekt, 8))
                Console.WriteLine("")

                SwTmovesArrayTypAvFrontobjekt += 1
            Loop
            Console.WriteLine("Antal Frontskyddsobjekt " & SwTmovesArrayTypAvFrontobjekt & " för slutpunkt " & SpBisObjektNamnSp)
            Console.WriteLine("")

            'close the connection
            Project_OleDbCn.Close()

        Catch ex As Exception
            DisplayError(ex, "Databas läs av alla Frontskyddsobjekt.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "Array tabell Frontskyddsobjekt"
    Public Sub ArrayObjektfrontskydd(ByVal counter As Integer)

        FrontskyddBisObjektObjectRorelse = ArrayTypAvFrontobjekt(counter, 4) ' typ av rörelse tv,vv mm
        StationIDFiktiv = ArrayTypAvFrontobjekt(counter, 5) 'driftplatsnummer
        BisObjektNamnFrontskydd = ArrayTypAvFrontobjekt(counter, 6) 'objektnamn
        BisObjektFrontskyddType = ArrayTypAvFrontobjekt(counter, 7) 'objektyp HS CV..
        BisObjektLageFrontskydd = ArrayTypAvFrontobjekt(counter, 8) 'läge höger,vänster,upp,ned ...


        Console.WriteLine("--- Läs av array post i frontskyddsobjekt ---")
        'Console.WriteLine("ID :" & ArrayTypAvFrontobjekt(FrontskyddXML, 0))
        'Console.WriteLine("FKEObjStID :" & ArrayTypAvFrontobjekt(FrontskyddXML, 1))
        'Console.WriteLine("FKEObjName :" & ArrayTypAvFrontobjekt(FrontskyddXML, 2))
        'Console.WriteLine("FKEObjType :" & ArrayTypAvFrontobjekt(FrontskyddXML, 3))
        Console.WriteLine("FKMoveType :" & FrontskyddBisObjektObjectRorelse)
        Console.WriteLine("FKFronProStID :" & StationIDFiktiv)
        Console.WriteLine("FKFronProName :" & BisObjektNamnFrontskydd)
        Console.WriteLine("FKFronProType :" & BisObjektFrontskyddType)
        Console.WriteLine("FrontProLeg :" & BisObjektLageFrontskydd)

        Console.WriteLine("")
    End Sub
#End Region

#Region "SwTmoves databas läs av SignalObjects Frontskyddsobjekt"
    Public Sub ReadSignalFrontskyddsobjektSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand
        Dim check As Integer = 0

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            'SQL
            Dim FROM_string As String = "SignalObjects"
            Dim WHERE_string As String = "FKStationID " & " = " & StationIDFiktiv &
                                         " AND ObjectName " & " = '" & BisObjektNamnFrontskydd &
                                         "' AND ObjectType " & " = '" & BisObjektFrontskyddType & "'"
            Dim ORDERBY_string As String = "ObjectName"
            Dim SQLquery As String = "Select * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("-- SwTmoves databas läs av SignalObjects Frontskyddsobjekt --")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            Dim objektnummer As String = ""
            BisObjektObjektnummerFrontskydd = ""

            'read rows in Stations
            Do While R_T_Connection.Read
                BisObjektObjektnummerFrontskydd = R_T_Connection.Item("ObjectNR")

                Console.WriteLine("SwTmoves databas läs av SignalObjects Objektnummer för Frontskydd: " & BisObjektObjektnummerFrontskydd)
                Console.WriteLine("")

                objektnummer = objektnummer & "/" & BisObjektObjektnummerFrontskydd
                check += 1
            Loop

            'Emtyp
            If BisObjektObjektnummerFrontskydd = "" Then
                Dim textinfo As String = "Driftplatsnummer: " & StationIDFiktiv & " Objektnamn: " & BisObjektNamnFrontskydd & " Objekttyp: " & BisObjektFrontskyddType &
                    " saknar BIS objektnummer för 'Frontskydd'. Sätts defult till 0"

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "SP rörelse frontskydd, saknar objektnummer
                                                        ", 10, textinfo, QualityControl_Counter)

                BisObjektObjektnummerFrontskydd = "0"
                QualityControl_Counter += 1
            End If

            'check objekttyp
            If Not BisObjektObjektnummerFrontskydd = "" Then
                BisObjektObjekttypnummerFrontskydd = ObjectTypeBIS(StationIDFiktiv, BisObjektNamnFrontskydd, BisObjektFrontskyddType)
            End If

            Project_OleDbCn.Close()

        Catch ex As Exception
            DisplayError(ex, "Databas läs av signalObjects Frontskyddsobjekt.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas läs av fronskyddsobjekt dess objekttyp"
    Public Sub ReadObjekttypfrontskyddSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim dataAdapterB As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand
        Dim commandB As OleDbCommand
        Dim check As Integer = 0
        Dim checkB As Integer = 0
        Dim arrayObjekttyp(15, 3) As Object

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            'SQL
            Dim FROM_stringB As String = "SignalObjects"
            Dim WHERE_stringB As String = "ObjectNR " & " = " & BisObjektObjektnummerFrontskydd &
                                         " AND ObjectType " & " = '" & BisObjektFrontskyddType & "'"
            Dim ORDERBY_stringB As String = "ObjectNR"
            Dim SQLqueryB As String = "Select * " & "FROM " & FROM_stringB & " WHERE " & WHERE_stringB & " ORDER BY " & ORDERBY_stringB

            Console.WriteLine("-- SwTmoves databas läs av om objektid finns på flera stationer för frontskydd --")
            Console.WriteLine("SQL query. " & SQLqueryB)

            ' Create the SelectCommand.
            commandB = New OleDbCommand(SQLqueryB, Project_OleDbCn)
            commandB.CommandTimeout = 20
            dataAdapterB.SelectCommand = commandB
            Dim R_T_ConnectionB = commandB.ExecuteReader

            'read rows in Stations
            Do While R_T_ConnectionB.Read
                arrayObjekttyp(checkB, 0) = R_T_ConnectionB.Item("FKStationID")
                arrayObjekttyp(checkB, 1) = R_T_ConnectionB.Item("ObjectName")
                arrayObjekttyp(checkB, 2) = R_T_ConnectionB.Item("ObjectType")
                arrayObjekttyp(checkB, 3) = R_T_ConnectionB.Item("ObjectNR")

                Console.WriteLine("FKStationID: " & arrayObjekttyp(checkB, 0))
                Console.WriteLine("ObjectName: " & arrayObjekttyp(checkB, 1))
                Console.WriteLine("ObjectType: " & arrayObjekttyp(checkB, 2))
                Console.WriteLine("ObjectNR: " & arrayObjekttyp(checkB, 3))
                Console.WriteLine("")

                checkB += 1
            Loop

            ' Fronskydd och dess objekttyp
            Dim FROM_string As String = "ObjektLage"
            Dim WHERE_string As String = "objnr " & " = " & BisObjektObjektnummerFrontskydd & ""
            Dim ORDERBY_string As String = "objnr"
            Dim SQLquery As String = "Select * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("-- SwTmoves databas läs av frontskydd och dess objekttyp --")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            Dim objektnummer As String = ""
            BisObjektObjekttypnummerFrontskydd = ""

            'read rows in Stations
            Do While R_T_Connection.Read

                'check plnrfr lika med FKStationID i array
                For index As Integer = 0 To checkB - 1

                    'objekttyp och objektnummer
                    If CheckplnfrFKStationID(BisObjektFrontskyddType, arrayObjekttyp(index, 2), BisObjektObjektnummerFrontskydd, arrayObjekttyp(index, 3)) = True Then

                        'stationsID
                        If CheckstationsIdplnfr(arrayObjekttyp(index, 0), R_T_Connection.Item("plnrfr")) = True Then
                            BisObjektObjekttypnummerFrontskydd = R_T_Connection.Item("objtypnr")
                            Console.WriteLine("objtypnr : " & R_T_Connection.Item("objtypnr"))
                            Console.WriteLine("")
                            Exit Do
                        End If
                    End If
                Next

                check += 1
            Loop

            'Emtyp
            If BisObjektObjekttypnummerFrontskydd = "" Then
                Dim textinfo As String = "Driftplatsnummer: " & StationIDFiktiv & " Objektnamn: " & BisObjektNamnFrontskydd & " Objekttyp: " & BisObjektFrontskyddType &
                    " saknar BIS objekttypnummer för 'Frontskydd'. Sätts defult till 6000"

                Console.WriteLine(textinfo)

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "SP rörelse frontskydd, saknar objekttypnummer", 1, textinfo, QualityControl_Counter)

                BisObjektObjekttypnummerFrontskydd = "6000"
                QualityControl_Counter += 1
            End If

            Console.WriteLine("")

            Project_OleDbCn.Close()

        Catch ex As Exception
            DisplayError(ex, "Databas läs av fronskyddsobjekt dess objekttyp.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "StationsID"
    Public Sub StationsIDSwTmoves(ByVal arraynr As Integer)
        StationID = ArrayStations(arraynr, 0) 'ID
    End Sub
#End Region

#Region "Börjapunkt och slutpunkt upplåsningspunkt, BIS nummer,typ"
    Public Sub ObjektIDBpSpSwTmoves(ByVal arraynr As Integer)

        'börjanpunkt rörelseväg
        BpBisObjektNamnRv = ArrayMovements(arraynr, 2)
        BorjanpunktBisObjectType = ArrayMovements(arraynr, 3)

        'slutpunkt rörelseväg
        SpBisObjektNamnRv = ArrayMovements(arraynr, 5)
        SlutpunktBisObjectType = ArrayMovements(arraynr, 6)

        'typ av rörelse rörelseväg
        If ArrayMovements(arraynr, 7) = "tv" Then
            Rörelsevägstyp = "tv"
        ElseIf ArrayMovements(arraynr, 7) = "vv" Then
            Rörelsevägstyp = "vv"
        Else
            Rörelsevägstyp = ArrayMovements(arraynr, 7)
        End If

        'upplåsningspunkt börjanpunkt rörelse
        BpBisObjektNamnUpl = ArrayMovements(arraynr, 8)

        'om det inte finns någon upplåsningspunkt projekterat så anses slutpunkten
        'vara upplåsningspunkten
        If BpBisObjektNamnUpl = "" Then
            BpBisObjektNamnUpl = ArrayMovements(arraynr, 5)
            BpBisStationsIDUpl = ArrayMovements(arraynr, 4)
            BpBisObjectTypeUpl = ArrayMovements(arraynr, 6)

            Dim textinfo As String = "Driftplatsnummer: " & StationIDFiktiv & ". Rörelseväg " & Rörelsevägstyp & " börjanpunkt : " & BpBisObjektNamnRv & " slutpunkt: " & SpBisObjektNamnRv &
                    " saknar upplåsningspunkt. Slutpunkt " & SpBisObjektNamnRv & " anses vara upplåsningspunkt."

            WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "BP rörelse upplåsningspunkt saknas", 10, textinfo, QualityControl_Counter)
            QualityControl_Counter += 1

            'viapunkt finns angiven
        Else
            BpBisObjektNamnUpl = ArrayMovements(arraynr, 8)
            BpBisStationsIDUpl = ArrayMovements(arraynr, 9)
            BpBisObjectTypeUpl = ArrayMovements(arraynr, 10)

        End If

        Console.WriteLine("-- Börjapunkt och slutpunkt upplåsningspunkt, BIS nummer,typ --")
        Console.WriteLine("Rörelseväg: " & Rörelsevägstyp & " , " & BpBisObjektNamnRv & " , " & BorjanpunktBisObjectType &
                          " , " & SpBisObjektNamnRv & " , " & SlutpunktBisObjectType)
        Console.WriteLine("Upplåsningspunkt: " & BpBisObjektNamnUpl & ", " & BpBisObjectTypeUpl)
        Console.WriteLine("")
    End Sub
#End Region

#Region "SwTmoves databas läs av SignalObjects börjanpunkt i rörelseväg"
    Public Sub ReadSignalObjectsBPSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand
        Dim check As Integer = 0

        'ObjectType

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            'SQL
            Dim FROM_string As String = "SignalObjects"
            Dim WHERE_string As String = "FKStationID " & " = " & StationID &
                                         " AND ObjectName " & " = '" & BpBisObjektNamnRv &
                                         "' AND ObjectType " & " = '" & BorjanpunktBisObjectType & "'"
            Dim ORDERBY_string As String = "ObjectName"
            Dim SQLquery As String = "Select * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("-- SwTmoves databas läs av SignalObjects börjanpunkt i rörelseväg --")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            Dim objektnummer As String = ""
            BpBisObjektObjektnummerRv = ""

            'read rows in Stations
            Do While R_T_Connection.Read
                BpBisObjektObjektnummerRv = R_T_Connection.Item("ObjectNR")

                Console.WriteLine("SwTmoves databas läs av SignalObjects börjanpunkt: " & BpBisObjektObjektnummerRv)
                Console.WriteLine("")

                objektnummer = objektnummer & "/" & BpBisObjektObjektnummerRv
                check += 1
            Loop

            'empty
            If BpBisObjektObjektnummerRv = "" Then
                Dim textinfo As String = "Driftplatsnummer: " & StationID & " Objektnamn: " & BpBisObjektNamnRv & " Objekttyp: " & BorjanpunktBisObjectType &
                         ". Saknar BIS objektnummer för 'Signalobjekt börjanpunkt rörelseväg'. Objektnummer måste defineras för att kunna bestämma typ av objekt"

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "BP rörelse saknar objektnummer", 1, textinfo, QualityControl_Counter)
                BpBisObjektObjektnummerRv = "0"
                QualityControl_Counter += 1
                Exit Sub
            End If

            'check objekttyp
            If Not BpBisObjektObjektnummerRv = "" Then
                BpBisObjekttypnummerRv = ObjectTypeBIS(StationID, BpBisObjektNamnRv, BorjanpunktBisObjectType)
            End If

            Project_OleDbCn.Close()

        Catch ex As Exception
            DisplayError(ex, "Databas läs av signalObjects börjanpunkt i rörelseväg.")
            Project_OleDbCn.Close()
        End Try

    End Sub

#Region "Översätt objekttyp till BIS objekttyp"
    Private Function ObjectTypeBIS(ByVal stationID As Integer, ByVal ObjektNamnRv As String, ByVal ObjectType As String) As String

        Select Case ObjectType

            'Fiktiv objekt kräver ID driftplats. Växel
            Case "CV"
                Return "CV" 'objekttyp 3300

            'Fiktiv objekt kräver ID driftplats. Stoppbock
            Case "SB"
                Return "SB" 'objekttyp 3310

            'Signal (främst ATC)
            Case "HS", "HD", "HF", "RFS", "FS", "FSK", "BSK", "SL"
                Return "6000"

             'Balisgrupp ATC
            Case "--"
                Return "6001"

            'Tavla (främst ATC)
            Case "--"
                Return "6002"

            'Signalpunktstavla ERTMS
            Case "ERTMSTavla"
                Return "6006"

            'Spårspärr
            Case "SPARR"
                Return "10015"

             'Spårledning
            Case "--"
                Return "10019"

            'Tavla (ej ATC)
            Case "Tavla", "Slutptvl"
                Return "10020"

            'Fiktiv objekt. Exemplvis slutpunktstavla TAM
            Case "FObjekt"
                Return "10022"

            'Signal (ej ATC)
            Case "DS", "SkyddSL", "Vxldvsi"
                Return "10023"

            'Stoppbock fällbar
            Case "--"
                Return "10050"

                'Objekttyp går ej att hitta
            Case Else
                Return "6000"

                Dim textinfo As String = "Driftplatsnummer: " & stationID & " Objektnamn: " & ObjektNamnRv & " Objekttyp: " & ObjectType &
                              ". Projekterad objekttyp inte implementerad som giltig BIS objekttyp. Omprojektering eller kontakta programmerar för ändring i program." &
                              " Typ av BIS objekt ges värde 6000, som innebär att objektyp är 'Signal (främst ATC)'."

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "Objekttyp BIS", 1, textinfo, QualityControl_Counter)
                QualityControl_Counter += 1
        End Select
    End Function
#End Region

#End Region

#Region "SwTmoves databas läs av SignalObjects slutpunkt i rörelseväg"
    Public Sub ReadSignalObjectsSPSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand
        Dim check As Integer = 0

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            'SQL
            Dim FROM_string As String = "SignalObjects"
            Dim WHERE_string As String = "FKStationID " & " = " & StationID &
                                         " AND ObjectName " & " = '" & SpBisObjektNamnRv &
                                         "' AND ObjectType " & " = '" & SlutpunktBisObjectType & "'"
            Dim ORDERBY_string As String = "ObjectName"
            Dim SQLquery As String = "Select * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("-- SwTmoves databas läs av SignalObjects slutpunkt i rörelseväg --")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            Dim objektnummer As String = ""
            SpBisObjektObjektnummerRv = ""

            'read rows in Stations
            Do While R_T_Connection.Read
                SpBisObjektObjektnummerRv = R_T_Connection.Item("ObjectNR")

                Console.WriteLine("ObjectNR Slutpunkt: " & SpBisObjektObjektnummerRv)
                Console.WriteLine("")

                objektnummer = objektnummer & "/" & SpBisObjektObjektnummerRv
                check += 1
            Loop

            'empty
            If SpBisObjektObjektnummerRv = "" Then
                Dim textinfo As String = "Driftplatsnummer: " & StationID & " Objektnamn: " & SpBisObjektNamnRv & " Objekttyp: " & SlutpunktBisObjectType &
                         ". Saknar BIS objektnummer för 'Signalobjekt slutpunkt rörelseväg'. Objektnummer måste defineras för att kunna bestämma typ av objekt"

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationID, "SP rörelse saknar objektnummer", 1, textinfo, QualityControl_Counter)
                SpBisObjektObjektnummerRv = "0"
                QualityControl_Counter += 1
                Exit Sub
            End If

            'check objekttyp
            If Not SpBisObjektObjektnummerRv = "" Then
                SpBisObjekttypnummerRv = ObjectTypeBIS(StationID, SpBisObjektNamnRv, SlutpunktBisObjectType)
            End If

            Project_OleDbCn.Close()

        Catch ex As Exception
            DisplayError(ex, "Databas läs av signalObjects slutpunkt i rörelseväg.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas läs av Börjanpunkt i rörelseväg och dess objekttyp"
    Public Sub ReadObjekttypBPSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim dataAdapterB As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand
        Dim commandB As OleDbCommand
        Dim check As Integer = 0
        Dim checkB As Integer = 0
        Dim arrayObjekttyp(5, 3) As Object

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            'SQL
            Dim FROM_stringB As String = "SignalObjects"
            Dim WHERE_stringB As String = "ObjectNR " & " = " & BpBisObjektObjektnummerRv &
                                        " AND ObjectType " & " = '" & BorjanpunktBisObjectType & "'"
            Dim ORDERBY_stringB As String = "ObjectNR"
            Dim SQLqueryB As String = "Select * " & "FROM " & FROM_stringB & " WHERE " & WHERE_stringB & " ORDER BY " & ORDERBY_stringB

            Console.WriteLine("-- SwTmoves databas läs av om objektid finns på flera stationer för Börjanpunkt rörelseväg --")
            Console.WriteLine("SQL query. " & SQLqueryB)

            ' Create the SelectCommand.
            commandB = New OleDbCommand(SQLqueryB, Project_OleDbCn)
            commandB.CommandTimeout = 20
            dataAdapterB.SelectCommand = commandB
            Dim R_T_ConnectionB = commandB.ExecuteReader

            'read rows in Stations
            Do While R_T_ConnectionB.Read
                arrayObjekttyp(checkB, 0) = R_T_ConnectionB.Item("FKStationID")
                arrayObjekttyp(checkB, 1) = R_T_ConnectionB.Item("ObjectName")
                arrayObjekttyp(checkB, 2) = R_T_ConnectionB.Item("ObjectType")
                arrayObjekttyp(checkB, 3) = R_T_ConnectionB.Item("ObjectNR")

                Console.WriteLine("FKStationID: " & arrayObjekttyp(checkB, 0))
                Console.WriteLine("ObjectName: " & arrayObjekttyp(checkB, 1))
                Console.WriteLine("ObjectType: " & arrayObjekttyp(checkB, 2))
                Console.WriteLine("ObjectNR: " & arrayObjekttyp(checkB, 3))
                Console.WriteLine("")

                checkB += 1
            Loop

            ' Börjanpunkt och dess objekttyp 
            Dim FROM_string As String = "ObjektLage"
            Dim WHERE_string As String = "objnr " & " = " & BpBisObjektObjektnummerRv & ""
            Dim ORDERBY_string As String = "plnrfr"
            Dim SQLquery As String = "Select * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("-- SwTmoves databas läs av Börjanpunkt i rörelseväg och dess objekttyp --")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            Dim objektnummer As String = ""
            BpBisObjekttypnummerRv = ""

            'read rows in Stations
            Do While R_T_Connection.Read

                'check plnrfr lika med FKStationID i array
                For index As Integer = 0 To checkB - 1

                    'objekttyp och objektnummer
                    If CheckplnfrFKStationID(BorjanpunktBisObjectType, arrayObjekttyp(index, 2), BpBisObjektObjektnummerRv, arrayObjekttyp(index, 3)) = True Then

                        'stationsID
                        If CheckstationsIdplnfr(arrayObjekttyp(index, 0), R_T_Connection.Item("plnrfr")) = True Then
                            BpBisObjekttypnummerRv = R_T_Connection.Item("objtypnr")
                            Console.WriteLine("objtypnr : " & R_T_Connection.Item("objtypnr"))
                            Console.WriteLine("")
                            Exit Do
                        End If
                    End If
                Next

                objektnummer = objektnummer & "/ Driftplatsnummer: " & R_T_Connection.Item("plnrfr") &
                                            " Objekttyp: " & R_T_Connection.Item("objtypnr")
                check += 1
            Loop

            'empty
            If BpBisObjekttypnummerRv = "" Then
                Dim textinfo As String = "Driftplatsnummer: " & StationID & " Objektnamn: " & BpBisObjektNamnRv & " Objekttyp: " & BorjanpunktBisObjectType &
                         ". Saknar BIS objekttypnummer för 'Signalobjekt börjanpunkt rörelseväg'. Objekttypnummer tilldelas värde 6000"

                Console.WriteLine(textinfo)

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationID, "BP rörelse saknar objekttypnummer", 1, textinfo, QualityControl_Counter)
                BpBisObjekttypnummerRv = "6000"
                QualityControl_Counter += 1
            End If


            Console.WriteLine("")

            Project_OleDbCn.Close()

        Catch ex As Exception
            DisplayError(ex, "Databas läs av börjanpunkt i rörelseväg och dess objekttyp.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas läs av Slutpunkt och dess objekttyp för rörelseväg"
    Public Sub ReadObjekttypSPSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim dataAdapterB As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand
        Dim commandB As OleDbCommand
        Dim check As Integer = 0
        Dim checkB As Integer = 0
        Dim arrayObjekttyp(5, 3) As Object

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            'SQL
            Dim FROM_stringB As String = "SignalObjects"
            Dim WHERE_stringB As String = "ObjectNR " & " = " & SpBisObjektObjektnummerRv &
                                         " AND ObjectType " & " = '" & SlutpunktBisObjectType & "'"
            Dim ORDERBY_stringB As String = "ObjectNR"
            Dim SQLqueryB As String = "Select * " & "FROM " & FROM_stringB & " WHERE " & WHERE_stringB & " ORDER BY " & ORDERBY_stringB

            Console.WriteLine("-- SwTmoves databas läs av om objektid finns på flera stationer för slutpunkt rörelseväg --")
            Console.WriteLine("SQL query. " & SQLqueryB)

            ' Create the SelectCommand.
            commandB = New OleDbCommand(SQLqueryB, Project_OleDbCn)
            commandB.CommandTimeout = 20
            dataAdapterB.SelectCommand = commandB
            Dim R_T_ConnectionB = commandB.ExecuteReader

            'read rows in Stations
            Do While R_T_ConnectionB.Read
                arrayObjekttyp(checkB, 0) = R_T_ConnectionB.Item("FKStationID")
                arrayObjekttyp(checkB, 1) = R_T_ConnectionB.Item("ObjectName")
                arrayObjekttyp(checkB, 2) = R_T_ConnectionB.Item("ObjectType")
                arrayObjekttyp(checkB, 3) = R_T_ConnectionB.Item("ObjectNR")

                Console.WriteLine("FKStationID: " & arrayObjekttyp(checkB, 0))
                Console.WriteLine("ObjectName: " & arrayObjekttyp(checkB, 1))
                Console.WriteLine("ObjectType: " & arrayObjekttyp(checkB, 2))
                Console.WriteLine("ObjectNR: " & arrayObjekttyp(checkB, 3))
                Console.WriteLine("")

                checkB += 1
            Loop

            ' Slutpunkt och dess objekttyp
            Dim FROM_string As String = "ObjektLage"
            Dim WHERE_string As String = "objnr " & " = " & SpBisObjektObjektnummerRv & ""
            Dim ORDERBY_string As String = "plnrfr"
            Dim SQLquery As String = "Select * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("-- SwTmoves databas läs av Slutpunkt i rörelseväg och dess objekttyp --")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            Dim objektnummer As String = ""
            SpBisObjekttypnummerRv = ""

            'read rows in Stations
            Do While R_T_Connection.Read

                'check plnrfr lika med FKStationID i array
                For index As Integer = 0 To checkB - 1

                    'objekttyp och objektnummer
                    If CheckplnfrFKStationID(SlutpunktBisObjectType, arrayObjekttyp(index, 2), SpBisObjektObjektnummerRv, arrayObjekttyp(index, 3)) = True Then

                        'stationsID
                        If CheckstationsIdplnfr(arrayObjekttyp(index, 0), R_T_Connection.Item("plnrfr")) = True Then
                            SpBisObjekttypnummerRv = R_T_Connection.Item("objtypnr")
                            Console.WriteLine("objtypnr : " & R_T_Connection.Item("objtypnr"))
                            Console.WriteLine("")
                            Exit Do
                        End If
                    End If
                Next

                objektnummer = objektnummer & "/ Driftplatsnummer: " & R_T_Connection.Item("plnrfr") &
                                            " Objekttyp: " & R_T_Connection.Item("objtypnr")
                check += 1
            Loop

            'empty
            If SpBisObjekttypnummerRv = "" Then
                Dim textinfo As String = "Driftplatsnummer: " & StationID & " Objektnamn: " & SpBisObjektNamnRv & " Objekttyp: " & SlutpunktBisObjectType &
                         ". Saknar BIS objekttypnummer för 'Signalobjekt slutpunkt rörelseväg'. Objekttypnummer tilldelas värde 6000"

                Console.WriteLine(textinfo)

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationID, "SP rörelse saknar objekttypnummer", 1, textinfo, QualityControl_Counter)
                SpBisObjekttypnummerRv = "6000"
                QualityControl_Counter += 1
            End If

            Console.WriteLine("")

            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadObjekttypSPSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av slutpunkt i rörelseväg och dess objekttyp.")
            Project_OleDbCn.Close()
        End Try

    End Sub

    Private Function CheckstationsIdplnfr(ByVal v1 As Object, ByVal v2 As Object) As Boolean
        If v1 = v2 Then
            Console.WriteLine("StationsId: " & v1)
            Return True
        Else
            Return False
        End If
    End Function

    Private Function CheckplnfrFKStationID(ByVal slutpunktBisObjectType As String, ByVal v1 As Object, ByVal spBisObjektObjektnummerRv As String, ByVal v2 As Object) As Boolean
        If slutpunktBisObjectType = v1 And
           spBisObjektObjektnummerRv = v2 Then
            Console.WriteLine("objekttyp och objektnummer")
            Console.WriteLine("objekttyp : " & v1)
            Console.WriteLine("objektnummer : " & v2)
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "SwTmoves databas läs av SignalObjects upplåsning börjanpunkt i rörelseväg"
    Public Sub ReadSignalObjectsBPUplSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand
        Dim check As Integer = 0

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            'SQL
            Dim FROM_string As String = "SignalObjects"
            Dim WHERE_string As String = "FKStationID " & " = " & BpBisStationsIDUpl &
                                         " AND ObjectName " & " = '" & BpBisObjektNamnUpl &
                                         "' AND ObjectType " & " = '" & BpBisObjectTypeUpl & "'"
            Dim ORDERBY_string As String = "ObjectName"
            Dim SQLquery As String = "Select * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("-- SwTmoves databas läs av SignalObjects upplåsning börjanpunkt i rörelseväg --")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            Dim objektnummer As String = ""
            BpBisObjektObjektnummerUpl = ""

            'read rows in Stations
            Do While R_T_Connection.Read
                BpBisObjektObjektnummerUpl = R_T_Connection.Item("ObjectNR")

                Console.WriteLine("ObjectNR upplåsning: " & BpBisObjektObjektnummerUpl)
                Console.WriteLine("")

                objektnummer = objektnummer & "/" & BpBisObjektObjektnummerUpl
                check += 1
            Loop

            'empty
            If BpBisObjektObjektnummerUpl = "" Then
                Dim textinfo As String = "Driftplatsnummer: " & BpBisStationsIDUpl & " Objektnamn: " & BpBisObjektNamnUpl & " Objekttyp: " & BpBisObjectTypeUpl &
                         ". Saknar BIS objektnummer för 'Signalobjekt upplåsning börjanpunkt i rörelseväg'. Objektnummer måste defineras för att kunna bestämma typ av objekt."

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, BpBisStationsIDUpl, "BP rörelse upplåsningspunkt saknar objektnummer", 1, textinfo, QualityControl_Counter)
                SpBisObjektObjektnummerRv = "0"
                QualityControl_Counter += 1
            End If

            'check objekttyp
            If Not BpBisObjektObjektnummerUpl = "" Then
                BpBisObjekttypnummerUpl = ObjectTypeBIS(BpBisStationsIDUpl, BpBisObjektNamnUpl, BpBisObjectTypeUpl)
            End If

            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadSignalObjectsBPUplSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av signalObjects upplåsning börjanpunkt i rörelseväg")
            Project_OleDbCn.Close()
        End Try
    End Sub
#End Region

#Region "SwTmoves databas läs av Börjanpunkt i rörelseväg upplåsning och dess objekttyp"
    Public Sub ReadObjekttypBPUplSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim dataAdapterB As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand
        Dim commandB As OleDbCommand
        Dim check As Integer = 0
        Dim checkB As Integer = 0
        Dim arrayObjekttyp(5, 3) As Object

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            'SQL
            Dim FROM_stringB As String = "SignalObjects"
            Dim WHERE_stringB As String = "ObjectNR " & " = " & BpBisObjektObjektnummerUpl &
                                        " AND ObjectType " & " = '" & BpBisObjectTypeUpl & "'"
            Dim ORDERBY_stringB As String = "ObjectNR"
            Dim SQLqueryB As String = "Select * " & "FROM " & FROM_stringB & " WHERE " & WHERE_stringB & " ORDER BY " & ORDERBY_stringB

            Console.WriteLine("-- SwTmoves databas läs av om objektid finns på flera stationer för börjanpunkt rörelseväg upplåsning--")
            Console.WriteLine("SQL query. " & SQLqueryB)

            ' Create the SelectCommand.
            commandB = New OleDbCommand(SQLqueryB, Project_OleDbCn)
            commandB.CommandTimeout = 20
            dataAdapterB.SelectCommand = commandB
            Dim R_T_ConnectionB = commandB.ExecuteReader

            'read rows in Stations
            Do While R_T_ConnectionB.Read
                arrayObjekttyp(checkB, 0) = R_T_ConnectionB.Item("FKStationID")
                arrayObjekttyp(checkB, 1) = R_T_ConnectionB.Item("ObjectName")
                arrayObjekttyp(checkB, 2) = R_T_ConnectionB.Item("ObjectType")
                arrayObjekttyp(checkB, 3) = R_T_ConnectionB.Item("ObjectNR")

                Console.WriteLine("FKStationID: " & arrayObjekttyp(checkB, 0))
                Console.WriteLine("ObjectName: " & arrayObjekttyp(checkB, 1))
                Console.WriteLine("ObjectType: " & arrayObjekttyp(checkB, 2))
                Console.WriteLine("ObjectNR: " & arrayObjekttyp(checkB, 3))
                Console.WriteLine("")

                checkB += 1
            Loop

            ' Börjanpunkt upplåsning och dess objekttyp
            Dim FROM_string As String = "ObjektLage"
            Dim WHERE_string As String = "objnr " & " = " & BpBisObjektObjektnummerUpl & ""
            Dim ORDERBY_string As String = "plnrfr"
            Dim SQLquery As String = "Select * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("-- SwTmoves databas läs av Börjanpunkt upplåsning i rörelseväg och dess objekttyp --")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            Dim objektnummer As String = ""
            BpBisObjekttypnummerUpl = ""

            'read rows in Stations
            Do While R_T_Connection.Read

                'check plnrfr lika med FKStationID i array
                For index As Integer = 0 To checkB - 1

                    'objekttyp och objektnummer
                    If CheckplnfrFKStationID(BpBisObjectTypeUpl, arrayObjekttyp(index, 2), BpBisObjektObjektnummerUpl, arrayObjekttyp(index, 3)) = True Then

                        'stationsID
                        If CheckstationsIdplnfr(arrayObjekttyp(index, 0), R_T_Connection.Item("plnrfr")) = True Then
                            BpBisObjekttypnummerUpl = R_T_Connection.Item("objtypnr")
                            Console.WriteLine("objtypnr : " & R_T_Connection.Item("objtypnr"))
                            Console.WriteLine("")
                            Exit Do
                        End If
                    End If
                Next

                objektnummer = objektnummer & "/ Driftplatsnummer: " & R_T_Connection.Item("plnrfr") &
                                            " Objekttyp: " & R_T_Connection.Item("objtypnr")
                check += 1
            Loop

            'empty
            If BpBisObjekttypnummerUpl = "" Then
                Dim textinfo As String = "Driftplatsnummer: " & BpBisStationsIDUpl & " Objektnamn: " & BpBisObjektNamnUpl & " Objekttyp: " & BpBisObjectTypeUpl &
                         ". Saknar BIS objekttypnummer för 'Signalobjekt börjanpunkt upplåsning rörelseväg'. Objekttypnummer tilldelas värde 6000"

                Console.WriteLine(textinfo)

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, BpBisStationsIDUpl, "BP rörelse upplåsningspunkt saknar objekttypnummer", 1, textinfo, QualityControl_Counter)
                BpBisObjekttypnummerUpl = "6000"
                QualityControl_Counter += 1
            End If

            Console.WriteLine("")

            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadObjekttypBPUplSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av börjanpunkt i rörelseväg upplåsning och dess objekttyp.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region " Slutpunkt i alla rörelsevägar BIS nummer,typ"
    Public Sub ObjektIDSpSpSwTmoves(ByVal arraynr As Integer)

        'slutpunkt rörelseväg
        SpBisObjektNamnSp = ArrayEndObje(arraynr, 1) 'objektnamn
        SpBisObjektObjectType = ArrayEndObje(arraynr, 2) 'objekttyp
        'SpBisObjektObjectRorelse = ArrayEndObje(arraynr, 3) 'typ av rörelseväg

        'typ av rörelse rörelseväg
        If ArrayEndObje(arraynr, 3) = "tv" Then
            Rörelsevägstyp = "TRH"
        ElseIf ArrayEndObje(arraynr, 3) = "vv" Then
            Rörelsevägstyp = "VRK"
        Else
            Rörelsevägstyp = ArrayEndObje(arraynr, 3)
        End If

        Console.WriteLine("-- Slutpunkt i alla rörelsevägar BIS nummer,typ --")
        'Console.WriteLine("slutpunkt rörelseväg: " & SpBisObjektNamnSp & " , " & SpBisObjektObjectType & " , " & SpBisObjektObjectRorelse & "(" & Rörelsevägstyp & ")")
        Console.WriteLine("")

    End Sub
#End Region

#Region "SwTmoves databas läs av SignalObjects slutpunkt för alla rörelse i en slutpunkt"
    Public Sub ReadSignalObjectsSPAllSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand
        Dim check As Integer = 0

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            'SQL
            Dim FROM_string As String = "SignalObjects"
            Dim WHERE_string As String = "FKStationID " & " = " & StationIDFiktiv &
                                         " AND ObjectName " & " = '" & SpBisObjektNamnSp &
                                         "' AND ObjectType " & " = '" & SpBisObjektObjectType & "'"
            Dim ORDERBY_string As String = "ObjectName"
            Dim SQLquery As String = "Select * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("-- SwTmoves databas läs av SignalObjects slutpunkt för alla rörelse i en slutpunkt --")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            Dim objektnummer As String = ""
            SpBisObjektObjektnummerSp = ""

            'read rows in Stations
            Do While R_T_Connection.Read
                SpBisObjektObjektnummerSp = If(IsDBNull(R_T_Connection.Item("ObjectNR")), String.Empty, R_T_Connection.Item("ObjectNR"))

                Console.WriteLine("SwTmoves databas läs av SignalObjects för alla rörelse i en slutpunkt: " & SpBisObjektObjektnummerSp)
                Console.WriteLine("")

                objektnummer = objektnummer & "/" & SpBisObjektObjektnummerSp
                check += 1
            Loop

            'Emtyp
            If SpBisObjektObjektnummerSp = "" Then
                Dim textinfo As String = "Driftplatsnummer: " & StationIDFiktiv & " Objektnamn: " & SpBisObjektNamnSp & " Objekttyp: " & SpBisObjektObjectType &
                    " saknar BIS objektnummer. Sätts defult till 0"

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "SP rörelse Alla till, saknar objektnummer", 1, textinfo, QualityControl_Counter)
                SpBisObjektObjektnummerSp = "0"
                QualityControl_Counter += 1
            End If

            'check objekttyp
            If Not SpBisObjektObjektnummerSp = "" Then
                SpBisObjektObjekttypnummerSp = ObjectTypeBIS(StationIDFiktiv, SpBisObjektNamnSp, SpBisObjektObjectType)
            End If


            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadSignalObjectsSPAllSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av SignalObjects slutpunkt för alla rörelse i en slutpunkt.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas läs av om objektid finns på flera stationer för Slutpunkt alla rörelseväg. "
    Public Sub ReadObjekttypSPAllSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim dataAdapterB As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand
        Dim commandB As OleDbCommand
        Dim check As Integer = 0
        Dim checkB As Integer = 0
        Dim arrayObjekttyp(5, 3) As Object

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            'SQL
            Dim FROM_stringB As String = "SignalObjects"
            Dim WHERE_stringB As String = "ObjectNR " & " = " & SpBisObjektObjektnummerSp &
                                        " AND ObjectType " & " = '" & SpBisObjektObjectType & "'"
            Dim ORDERBY_stringB As String = "ObjectNR"
            Dim SQLqueryB As String = "Select * " & "FROM " & FROM_stringB & " WHERE " & WHERE_stringB & " ORDER BY " & ORDERBY_stringB

            Console.WriteLine("-- SwTmoves databas läs av om objektid finns på flera stationer för Slutpunkt alla rörelseväg. --")
            Console.WriteLine("SQL query. " & SQLqueryB)

            ' Create the SelectCommand.
            commandB = New OleDbCommand(SQLqueryB, Project_OleDbCn)
            commandB.CommandTimeout = 20
            dataAdapterB.SelectCommand = commandB
            Dim R_T_ConnectionB = commandB.ExecuteReader

            'read rows in Stations
            Do While R_T_ConnectionB.Read
                arrayObjekttyp(checkB, 0) = R_T_ConnectionB.Item("FKStationID")
                arrayObjekttyp(checkB, 1) = R_T_ConnectionB.Item("ObjectName")
                arrayObjekttyp(checkB, 2) = R_T_ConnectionB.Item("ObjectType")
                arrayObjekttyp(checkB, 3) = R_T_ConnectionB.Item("ObjectNR")

                Console.WriteLine("FKStationID: " & arrayObjekttyp(checkB, 0))
                Console.WriteLine("ObjectName: " & arrayObjekttyp(checkB, 1))
                Console.WriteLine("ObjectType: " & arrayObjekttyp(checkB, 2))
                Console.WriteLine("ObjectNR: " & arrayObjekttyp(checkB, 3))
                Console.WriteLine("")

                checkB += 1
            Loop

            ' Slutpunkt alla och dess objekttyp
            Dim FROM_string As String = "ObjektLage"
            Dim WHERE_string As String = "objnr " & " = " & SpBisObjektObjektnummerSp & ""
            Dim ORDERBY_string As String = "plnrfr"
            Dim SQLquery As String = "Select * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("-- SwTmoves databas läs av slutpunkt för alla rörelse och dess objekttyp --")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            Dim objektnummer As String = ""
            SpBisObjektObjekttypnummerSp = ""

            'read rows in Stations
            Do While R_T_Connection.Read

                'check plnrfr lika med FKStationID i array
                For index As Integer = 0 To checkB - 1

                    'objekttyp och objektnummer
                    If CheckplnfrFKStationID(SpBisObjektObjectType, arrayObjekttyp(index, 2), SpBisObjektObjektnummerSp, arrayObjekttyp(index, 3)) = True Then

                        'stationsID
                        If CheckstationsIdplnfr(arrayObjekttyp(index, 0), R_T_Connection.Item("plnrfr")) = True Then
                            SpBisObjektObjekttypnummerSp = R_T_Connection.Item("objtypnr")
                            Console.WriteLine("objtypnr : " & R_T_Connection.Item("objtypnr"))
                            Console.WriteLine("")
                            Exit Do
                        End If
                    End If
                Next

                objektnummer = objektnummer & "/ Driftplatsnummer: " & R_T_Connection.Item("plnrfr") &
                                            " Objekttyp: " & R_T_Connection.Item("objtypnr")
                check += 1
            Loop

            'Empty
            If SpBisObjektObjekttypnummerSp = "" Then

                Dim textinfo As String = "Driftplatsnummer: " & StationIDFiktiv & " Objektnamn: " & SpBisObjektNamnSp & " Objekttyp: " & SpBisObjektObjectType &
                    " saknar BIS objekttypnummer för 'Slutpunkt alla rörelser' . Objekttypnummer tilldelas värde 6000"

                Console.WriteLine(textinfo)

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "SP rörelse Alla, saknar objekttypnummer", 1, textinfo, QualityControl_Counter)
                SpBisObjektObjekttypnummerSp = "6000"
                QualityControl_Counter += 1
            End If

            Console.WriteLine("")

            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadObjekttypSPAllSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av om objektid finns på flera stationer för Slutpunkt alla rörelseväg.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas läs av skyddssträck,skyddavstånd från slutpunkt all rörelse"
    Public Sub ReadSignalObjectsskyddstrAllSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            Console.WriteLine("-- SwTmoves databas läs av skyddssträck,skyddavstånd från slutpunkt all rörelse --")

            'SQL
            Dim FROM_string As String = "Movements"
            Dim WHERE_string As String = "FKStObjStID " & " = " & StationIDFiktiv &
                                         " AND FKStObjName " & " = '" & SpBisObjektNamnSp &
                                         "' AND FKStObjType " & " = '" & SpBisObjektObjectType & "'"
            '"' AND MovType " & " = '" & SpBisObjektObjectRorelse & "'"
            Dim ORDERBY_string As String = "FKStObjName"
            Dim SQLquery As String = "Select * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            'temp kontroll samma data vid projektering
            Dim oldSpBisProtectDistance As String
            Dim oldSpBisProtectleght As String

            'read rows in Stations
            Do While R_T_Connection.Read
                SpBisProtectDistance = R_T_Connection.Item("ProtectDistance") 'skyddsträcka
                SpBisProtectleght = R_T_Connection.Item("Protectleght") 'skyddsavstånd
                SpBisObjektnummerUpplasning = If(IsDBNull(R_T_Connection.Item("UlpName")), String.Empty, R_T_Connection.Item("UlpName")) 'Upplåsning objektnamn
                SpBisObjekttypUpplasning = If(IsDBNull(R_T_Connection.Item("Ulptype")), String.Empty, R_T_Connection.Item("Ulptype")) 'Upplåsning typ av rörelse

                oldSpBisProtectDistance = SpBisProtectDistance
                oldSpBisProtectleght = SpBisProtectleght

                Console.WriteLine("skyddsträcka: " & SpBisProtectDistance & "skyddsavstånd: " & SpBisProtectleght)
                Console.WriteLine("Upplåsning objektnamn: " & SpBisObjektnummerUpplasning & " Upplåsning typ av rörelse: " & SpBisObjekttypUpplasning)

            Loop

            'Only one entry
            'If check > 1 Then
            '    MessageBox.Show("Finns flera objekt som har samma BIS objektnummer för Slutpunkt. " & SpBisObjektObjektnummerSp)
            'End If

            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadSignalObjectsskyddstrAllSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av skyddssträck,skyddavstånd från slutpunkt all rörelse.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region " SwTmoves databas läs av skyddssträck,skyddavstånd från slutpunkt all rörelse"
    Public Sub ReadSignalObjectsSPRorelseväg()
        Dim TypAvrorelse As String = ""
        Dim tvCounter As Integer = 0
        Dim vvCounter As Integer = 0
        Dim ftvCounter As Integer = 0
        Dim stvCounter As Integer = 0
        Dim libCounter As Integer = 0
        Dim tamCounter As Integer = 0
        Dim btvCounter As Integer = 0


        SpBisObjektObject_tv = False
        SpBisObjektObject_vv = False

        Console.WriteLine("-- SwTmoves databas läs av skyddssträck,skyddavstånd från slutpunkt all rörelse --")

        'typ av rörelse rörelsevägar och antal
        For counterA = 0 To SwTmovesArrayMovementsPost - 1
            TypAvrorelse = ArrayMovements(counterA, 7)

            If TypAvrorelse = "tv" Then
                tvCounter += 1
            ElseIf TypAvrorelse = "vv" Then
                vvCounter += 1
            ElseIf TypAvrorelse = "ftv" Then
                ftvCounter += 1
            ElseIf TypAvrorelse = "stv" Then
                stvCounter += 1
            ElseIf TypAvrorelse = "lib" Then
                libCounter += 1
            ElseIf TypAvrorelse = "tam" Then
                tamCounter += 1
            ElseIf TypAvrorelse = "btv" Then
                btvCounter += 1
            Else
                MessageBox.Show("Typen av rörelseväg kan inte läsa av för skyddssträck,skyddavstånd från slutpunkt all rörelse. Kan vara felaktig projektering. Typ av rörelseväg: " & TypAvrorelse)
            End If
        Next

        'Tågväg
        If tvCounter >= 1 Then
            SpBisObjektObject_tv = True
        End If

        'Växlingsväg
        If vvCounter >= 1 Then
            SpBisObjektObject_vv = True
        End If

        'Förenkladtågväg
        If ftvCounter >= 1 Then
            SpBisObjektObject_ftv = True
        End If

        'Särskildtågväg
        If stvCounter >= 1 Then
            SpBisObjektObject_stv = True
        End If

        'Blocksträcka
        If libCounter >= 1 Then
            SpBisObjektObject_lib = True
        End If

        'TAMsträcka
        If tamCounter >= 1 Then
            SpBisObjektObject_tam = True
        End If

        'Bevakadtågväg
        If btvCounter >= 1 Then
            SpBisObjektObject_btv = True
        End If

        Console.WriteLine("Totalt antal Tågväg,Växlingsväg,Förenkladtågväg,Särskildtågväg,Blocksträcka,TAMsträcka,Bevakadtågväg: " &
                          tvCounter & " , " &
                          vvCounter & " , " &
                          ftvCounter & " , " &
                          stvCounter & " , " &
                          libCounter & " , " &
                          tamCounter & " , " &
                          btvCounter)
        Console.WriteLine("")

    End Sub
#End Region

#Region "SwTmoves Array EndObj. Läs av om det finns några slutpunkter på driftplatsen"
    Public Sub ArrayEndObjTRH(ByVal typrorelse As String)
        Dim EndObjFKSiObjStID As Integer
        Dim EndObjFKSiObjName As String
        Dim EndObjFKSiObjType As String
        Dim EndObjMoveType As String
        Dim MovementsProtectDistance As String 'skyddsträcka
        Dim MovementsProtectleght As String 'skyddsavstånd
        Dim MovementsMovType As String 'typ av rörelse
        Dim MovementsFKStObjName As String 'börjanpunktsnamn
        Dim MovementsFKEObjName As String 'Slutpunktsnamn

        Dim SPSignal As Boolean = False 'slutpunkt hittad
        Dim SPMovements As Integer = 0 'antal slutpunkter
        Dim SPMovementsProtectDistance As String = ""
        Dim SPMovementsProtectleght As String = ""

        'loop EndObj
        Do While SwTmovesPeekArrayEndObj < SwTmovesArrayEndObjPost

            'EndObj
            EndObjFKSiObjStID = ArrayEndObje(SwTmovesPeekArrayEndObj, 0)
            EndObjFKSiObjName = ArrayEndObje(SwTmovesPeekArrayEndObj, 1)
            EndObjFKSiObjType = ArrayEndObje(SwTmovesPeekArrayEndObj, 2)
            EndObjMoveType = ArrayEndObje(SwTmovesPeekArrayEndObj, 3)

            'loop Movements
            Do While SwTmovesPeekArrayMovements < SwTmovesArrayMovementsPost

                'Movements
                MovementsFKStObjName = ArrayMovements(SwTmovesPeekArrayMovements, 2)
                MovementsFKEObjName = ArrayMovements(SwTmovesPeekArrayMovements, 5)
                MovementsMovType = ArrayMovements(SwTmovesPeekArrayMovements, 7)
                MovementsProtectDistance = ArrayMovements(SwTmovesPeekArrayMovements, 12)
                MovementsProtectleght = ArrayMovements(SwTmovesPeekArrayMovements, 13)

                Console.WriteLine("Slutpunkt/Rörelsetyp: " & EndObjFKSiObjName & " , " & EndObjMoveType)
                'PrintArrayMovementsSwTmoves("--- Slutpunkt för tågväg/Växlingsväg tabel Moments---", SwTmovesPeekArrayMovements)
                'Console.WriteLine("Array moment / :" & SwTmovesPeekArrayMovements & " , " & SwTmovesArrayMovementsPost)

                'riktig slutpunkt för driftplats
                If MovementsFKEObjName = EndObjFKSiObjName And MovementsMovType = typrorelse Then

                    SPSignal = True
                    SpBisProtectDistance = MovementsProtectDistance
                    SpBisProtectleght = MovementsProtectleght

                    'check om samma värde finns vid flera börjanpunkter
                    If SPMovements >= 1 Then

                        If SPMovementsProtectDistance <> SpBisProtectDistance Then
                            WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "K", 2, "Skyddsträcka gällande slutpunkt " & MovementsFKEObjName & " har olika skyddssträcka. Angiven sträcka: " &
                                                                    SpBisProtectDistance & "/" & SPMovementsProtectDistance, QualityControl_Counter)
                            QualityControl_Counter += 1
                        End If

                        If SPMovementsProtectleght <> SpBisProtectleght Then
                            WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "K", 2, "Skyddsavstånd gällande slutpunkt " & MovementsFKEObjName & " har olika skyddsavstånd. Angiven avstånd: " &
                                                                   SPMovementsProtectleght & "/" & SpBisProtectleght, QualityControl_Counter)
                            QualityControl_Counter += 1
                        End If

                        SPMovementsProtectDistance = SpBisProtectDistance
                        SPMovementsProtectleght = SpBisProtectleght
                    End If

                    SPMovements += 1
                End If

                SwTmovesPeekArrayMovements += 1
            Loop

            '---------------------------------------------------
            'Genomgång av Movements klart.
            'finns det några slutpunkter för kontrollerad signal
            '---------------------------------------------------

            'slutpunkt finns
            If SPSignal = True Then

                'läs av BIS data
                SpBisObjektNamnSp = EndObjFKSiObjName
                SpBisObjektObjectType = EndObjFKSiObjType

                'startvärde array Movements
                SwTmovesPeekArrayMovements = 0

                'nästa slutpunkt som ska avläsas
                SwTmovesPeekArrayEndObj += 1

                'alla tågvägar avlästa
                If typrorelse = "tv" And SwTmovesPeekArrayEndObj >= SwTmovesArrayEndObjPost Then
                    SpBisObjektObject_tv = False
                    SwTmovesPeekArrayEndObj = 0
                End If

                'alla växlingsvägar avlästa
                If typrorelse = "vv" And SwTmovesPeekArrayEndObj >= SwTmovesArrayEndObjPost Then
                    SpBisObjektObject_vv = False
                    SwTmovesPeekArrayEndObj = 0
                End If
                Exit Do

            Else
                'startvärde array Movements
                SwTmovesPeekArrayMovements = 0
            End If

            SPSignal = False
            SwTmovesPeekArrayEndObj += 1
        Loop

    End Sub
#End Region

#Region "SwTmoves Array EndObj. Skapa Typ av rörelse"
    Public Sub ArrayEndObjRorelsevagar(ByVal typrorelse As String)

        Dim counterMovement As Integer = 0
        Dim EndObjFKSiObjStID As Integer
        Dim EndObjFKSiObjName As String
        Dim EndObjFKSiObjType As String
        Dim EndObjMoveType As String
        Dim MovementsProtectDistance As String 'skyddsträcka
        Dim MovementsProtectleght As String 'skyddsavstånd
        Dim MovementsMovType As String 'typ av rörelse
        Dim MovementsFKStObjName As String 'börjanpunktsnamn
        Dim MovementsFKStObjType As String 'objekttyp
        Dim MovementsFKEObjName As String 'Slutpunktsnamn

        Dim SPSignal As Boolean = False 'slutpunkt hittad
        Dim SPMovements As Integer = 0 'antal slutpunkter
        Dim SPMovementsProtectDistance As String = ""
        Dim SPMovementsProtectleght As String = ""

        'EndObj
        EndObjFKSiObjStID = StationIDFiktiv
        EndObjFKSiObjName = SpBisObjektNamnSp
        EndObjFKSiObjType = SpBisObjektObjectType
        EndObjMoveType = typrorelse

        Console.WriteLine("EndObjFKSiObjStID : " & StationIDFiktiv)
        Console.WriteLine("EndObjFKSiObjName : " & SpBisObjektNamnSp)
        Console.WriteLine("EndObjFKSiObjType : " & SpBisObjektObjectType)
        Console.WriteLine("EndObjMoveType : " & typrorelse)
        Console.WriteLine("")

        'loop Movements
        Do While counterMovement < SwTmovesArrayMovementsPost

            'Movements
            MovementsFKStObjName = ArrayMovements(counterMovement, 2)
            MovementsFKEObjName = ArrayMovements(counterMovement, 5)
            MovementsFKStObjType = ArrayMovements(counterMovement, 6)
            MovementsMovType = ArrayMovements(counterMovement, 7)
            MovementsProtectDistance = ArrayMovements(counterMovement, 12)
            MovementsProtectleght = ArrayMovements(counterMovement, 13)

            'kontroll skyddsträck och skyddavstånd
            If EndObjFKSiObjName = MovementsFKEObjName And 'slutpunkt
               EndObjFKSiObjType = MovementsFKStObjType And 'objektyp HS
                EndObjMoveType = MovementsMovType Then ' rörelsetyp tv,vv

                'check om samma värde finns vid flera börjanpunkter
                If SPMovements >= 1 Then

                    If SPMovementsProtectDistance <> SpBisProtectDistance Then
                        MessageBox.Show("Skyddsträcka gällande slutpunkt " & MovementsFKEObjName & " har olika skyddssträcka. Angiven sträcka: " & SpBisProtectDistance & "/" & SPMovementsProtectDistance)
                    End If

                    If SPMovementsProtectleght <> SpBisProtectleght Then
                        MessageBox.Show("Skyddsavstånd gällande slutpunkt " & MovementsFKEObjName & " har olika skyddsavstånd. Angiven avstånd: " & SPMovementsProtectleght & "/" & SpBisProtectleght)
                    End If

                    Console.WriteLine("  --------- Ut från EndObj/Moment: ------------- " & SPMovementsProtectDistance & " , " & SPMovementsProtectleght)
                End If

                'Startvärde som kontrollera framåt
                If SPMovements = 0 Then
                    SpBisProtectDistance = MovementsProtectDistance
                    SpBisProtectleght = MovementsProtectleght

                    SPMovementsProtectDistance = MovementsProtectDistance
                    SPMovementsProtectleght = MovementsProtectleght
                End If

                SPMovements += 1
            End If

            counterMovement += 1
        Loop

        'saknas skyddavstånd/skyddsträcka för slutpunkt i driftplats.
        'finns troligen på linjeplats
        If SPMovements = 0 Then
            SpBisProtectDistance = -1
            SpBisProtectleght = -1
        End If

        Console.WriteLine("Skyddsträcka :" & SpBisProtectDistance)
        Console.WriteLine("Skyddavstånd :" & SpBisProtectleght)
        Console.WriteLine("")

    End Sub
#End Region

#Region "SwTmoves Array EndObj. Skapa Typ av rörelse tågväg tv,btv"
    Public Sub ArrayEndObjbtv(ByVal typtv As String, ByVal typbtv As String)

        Dim counterMovement As Integer = 0
        Dim EndObjFKSiObjStID As Integer
        Dim EndObjFKSiObjName As String
        Dim EndObjFKSiObjType As String
        Dim EndObjMoveTypetv As String
        Dim EndObjMoveTypebtv As String
        Dim MovementsProtectDistance As String 'skyddsträcka
        Dim MovementsProtectleght As String 'skyddsavstånd
        Dim MovementsMovType As String 'typ av rörelse
        Dim MovementsFKStObjName As String 'börjanpunktsnamn
        Dim MovementsFKStObjType As String 'objekttyp
        Dim MovementsFKEObjName As String 'Slutpunktsnamn

        Dim SPSignal As Boolean = False 'slutpunkt hittad
        Dim SPMovements As Integer = 0 'antal slutpunkter
        Dim SPMovementsProtectDistance As String = ""
        Dim SPMovementsProtectleght As String = ""

        'EndObj
        EndObjFKSiObjStID = StationIDFiktiv
        EndObjFKSiObjName = SpBisObjektNamnSp
        EndObjFKSiObjType = SpBisObjektObjectType
        EndObjMoveTypetv = typtv
        EndObjMoveTypebtv = typbtv

        Console.WriteLine("EndObjFKSiObjStID : " & StationIDFiktiv)
        Console.WriteLine("EndObjFKSiObjName : " & SpBisObjektNamnSp)
        Console.WriteLine("EndObjFKSiObjType : " & SpBisObjektObjectType)
        Console.WriteLine("EndObjMoveType : " & typtv & " , " & typbtv)
        Console.WriteLine("")

        'loop Movements
        Do While counterMovement < SwTmovesArrayMovementsPost

            'Movements
            MovementsFKStObjName = ArrayMovements(counterMovement, 2)
            MovementsFKEObjName = ArrayMovements(counterMovement, 5)
            MovementsFKStObjType = ArrayMovements(counterMovement, 6)
            MovementsMovType = ArrayMovements(counterMovement, 7)
            MovementsProtectDistance = ArrayMovements(counterMovement, 12)
            MovementsProtectleght = ArrayMovements(counterMovement, 13)

            'objektnamn
            If EndObjFKSiObjName = MovementsFKEObjName Then

                'objekttyp
                If EndObjFKSiObjType = MovementsFKStObjType Then

                    ' rörelsetyp tv,vv
                    If EndObjMoveTypetv = MovementsMovType Or EndObjMoveTypebtv = MovementsMovType Then

                        'check om samma värde finns vid flera börjanpunkter
                        If SPMovements >= 1 Then

                            If SPMovementsProtectDistance <> SpBisProtectDistance Then
                                MessageBox.Show("Skyddsträcka gällande slutpunkt " & MovementsFKEObjName & " har olika skyddssträcka. Angiven sträcka: " & SpBisProtectDistance & "/" & SPMovementsProtectDistance)
                            End If

                            If SPMovementsProtectleght <> SpBisProtectleght Then
                                MessageBox.Show("Skyddsavstånd gällande slutpunkt " & MovementsFKEObjName & " har olika skyddsavstånd. Angiven avstånd: " & SPMovementsProtectleght & "/" & SpBisProtectleght)
                            End If

                            Console.WriteLine("  --------- Ut från EndObj/Moment: ------------- " & SPMovementsProtectDistance & " , " & SPMovementsProtectleght)
                        End If

                        'Startvärde som kontrollera framåt
                        If SPMovements = 0 Then
                            SpBisProtectDistance = MovementsProtectDistance
                            SpBisProtectleght = MovementsProtectleght

                            SPMovementsProtectDistance = MovementsProtectDistance
                            SPMovementsProtectleght = MovementsProtectleght
                        End If

                        SPMovements += 1
                    End If
                End If
            End If

            counterMovement += 1
        Loop

        'saknas skyddavstånd/skyddsträcka för slutpunkt i driftplats.
        'finns troligen på linjeplats
        If SPMovements = 0 Then
            SpBisProtectDistance = -1
            SpBisProtectleght = -1
        End If

        Console.WriteLine("Skyddsträcka :" & SpBisProtectDistance)
        Console.WriteLine("Skyddavstånd :" & SpBisProtectleght)
        Console.WriteLine("")

    End Sub
#End Region
    Public Sub ArrayEndSP(ByVal SP As String, ByVal typrorelse As String)

        Dim counterMovement As Integer = 0
        Dim MovementsMovType As String 'typ av rörelse
        Dim MovementsFKStObjName As String 'börjanpunktsnamn
        Dim MovementsFKStObjType As String 'objekttyp
        Dim MovementsFKEObjName As String 'Slutpunktsnamn

        'slutpunkt
        Dim LSignal As String = ""
        Dim SpRorelse As Char = ""
        Dim blockstrSP As Boolean = False 'blocksträcka

        'börjapunkt
        Dim LBP As String = ""
        Dim BpRorelse As Char = ""
        Dim blockstrBP As Boolean = False 'blocksträcka

        'är projekterad slutpunkt en blocksignal
        LSignal = SP
        SpRorelse = LSignal.Substring(0, 1)

        If SpRorelse = "L" Or SpRorelse = "N" Or SpRorelse = "U" Then
            blockstrSP = True
        End If

        'loop Movements
        Do While counterMovement < SwTmovesArrayMovementsPost

            'Movements
            MovementsFKStObjName = ArrayMovements(counterMovement, 2)
            MovementsFKEObjName = ArrayMovements(counterMovement, 5)
            MovementsFKStObjType = ArrayMovements(counterMovement, 6)
            MovementsMovType = ArrayMovements(counterMovement, 7)

            'rätt typ av rörelseväg
            If MovementsMovType = typrorelse Then

                'blocksträcka börjanpunkt
                LBP = MovementsFKStObjName
                BpRorelse = LBP.Substring(0, 1)

                If BpRorelse = "L" Or BpRorelse = "N" Or BpRorelse = "U" Then
                    blockstrBP = True
                End If

                'slutpunkt och slutpunkt lika namn
                If MovementsFKEObjName = SP Then
                    If blockstrBP = True And blockstrSP = True Then
                        SpTypObjektnamnBP = "Blocksträcka"

                        Console.WriteLine("BP namn: " & MovementsFKStObjName)
                        Console.WriteLine("BP typ: " & blockstrBP)
                        Console.WriteLine("SP namn: " & SP)
                        Console.WriteLine("BP typ: " & blockstrSP)

                        Exit Sub
                    ElseIf blockstrBP = True And blockstrSP = False Then
                        SpTypObjektnamnBP = "Blocksträcka"

                        Console.WriteLine("BP namn: " & MovementsFKStObjName)
                        Console.WriteLine("BP typ: " & blockstrBP)
                        Console.WriteLine("SP namn: " & SP)
                        Console.WriteLine("BP typ: " & blockstrSP)
                        Exit Sub
                    End If
                End If

                'börjanpunkt och projekterad slutpunkt lika namn
                If MovementsFKStObjName = SP Then
                    If blockstrBP = True Then
                        SpTypObjektnamnBP = "Blocksträcka"
                        Exit Sub
                    End If
                End If
            End If
            counterMovement += 1
        Loop
    End Sub
    Public Sub ArrayFrontskydd(ByVal SP As String, ByVal typrorelse As String)

        Dim counterMovement As Integer = 0
        Dim MovementsMovType As String 'typ av rörelse
        Dim MovementsFKStObjName As String 'börjanpunktsnamn
        Dim MovementsFKStObjType As String 'objekttyp
        Dim MovementsFKEObjName As String 'Slutpunktsnamn

        'slutpunkt
        Dim LSignal As String = ""
        Dim SpRorelse As Char = ""
        Dim blockstrSP As Boolean = False 'blocksträcka

        'börjapunkt
        Dim LBP As String = ""
        Dim BpRorelse As Char = ""
        Dim blockstrBP As Boolean = False 'blocksträcka

        'är projekterad slutpunkt en blocksignal
        LSignal = SP
        SpRorelse = LSignal.Substring(0, 1)

        If SpRorelse = "L" Or SpRorelse = "N" Or SpRorelse = "U" Then
            blockstrSP = True
        End If

        'loop Movements
        Do While counterMovement < SwTmovesArrayMovementsPost

            'Movements
            MovementsFKStObjName = ArrayMovements(counterMovement, 2)
            MovementsFKEObjName = ArrayMovements(counterMovement, 5)
            MovementsFKStObjType = ArrayMovements(counterMovement, 6)
            MovementsMovType = ArrayMovements(counterMovement, 7)

            'rätt typ av rörelseväg
            If MovementsMovType = typrorelse Then

                'blocksträcka börjanpunkt
                LBP = MovementsFKStObjName
                BpRorelse = LBP.Substring(0, 1)

                If BpRorelse = "L" Or BpRorelse = "N" Or BpRorelse = "U" Then
                    blockstrBP = True
                End If

                'slutpunkt och slutpunkt lika namn
                If MovementsFKEObjName = SP Then
                    If blockstrBP = True And blockstrSP = True Then
                        SpTypObjektnamnBP = "Blocksträcka"

                        Console.WriteLine("slutpunkt och slutpunkt lika namn: " & SpTypObjektnamnBP)
                        Console.WriteLine("BP namn: " & MovementsFKStObjName)
                        Console.WriteLine("BP typ: " & blockstrBP)
                        Console.WriteLine("SP namn: " & SP)
                        Console.WriteLine("BP typ: " & blockstrSP)
                        Console.WriteLine("")
                        Exit Sub
                    ElseIf blockstrBP = True And blockstrSP = False Then
                        SpTypObjektnamnBP = "Blocksträcka"
                        Exit Sub
                    End If
                End If

                'börjanpunkt och projekterad slutpunkt lika namn
                If MovementsFKStObjName = SP Then
                    If blockstrBP = True Then
                        SpTypObjektnamnBP = "Blocksträcka"

                        Console.WriteLine("börjanpunkt och slutpunkt lika namn: " & SpTypObjektnamnBP)
                        Console.WriteLine("BP namn: " & MovementsFKStObjName)
                        Console.WriteLine("BP typ: " & blockstrBP)
                        Console.WriteLine("SP namn: " & SP)
                        Console.WriteLine("BP typ: " & blockstrSP)
                        Console.WriteLine("")
                        Exit Sub
                    End If
                End If
            End If
            counterMovement += 1
        Loop
    End Sub
    Public Sub ReadEndObject()
        Dim counterEndObj As Integer = 0
        Dim tv As Integer = 0
        Dim vv As Integer = 0
        Dim ftv As Integer = 0
        Dim stv As Integer = 0
        Dim libC As Integer = 0
        Dim tam As Integer = 0
        Dim btv As Integer = 0

        'loop array EndOb för kontroll vilka rörelsetyper som finns för slutpunkt
        Do While counterEndObj < SwTmovesArrayEndObjPost

            If StationIDFiktiv = ArrayEndObje(counterEndObj, 0) And
               SpBisObjektNamnSp = ArrayEndObje(counterEndObj, 1) And
               SpBisObjektObjectType = ArrayEndObje(counterEndObj, 2) Then

                If ArrayEndObje(counterEndObj, 3) = "tv" Then
                    tv += 1

                ElseIf ArrayEndObje(counterEndObj, 3) = "vv" Then
                    vv += 1

                ElseIf ArrayEndObje(counterEndObj, 3) = "ftv" Then
                    ftv += 1

                ElseIf ArrayEndObje(counterEndObj, 3) = "stv" Then
                    stv += 1

                ElseIf ArrayEndObje(counterEndObj, 3) = "lib" Then
                    libC += 1

                ElseIf ArrayEndObje(counterEndObj, 3) = "tam" Then
                    tam += 1

                ElseIf ArrayEndObje(counterEndObj, 3) = "btv" Then
                    btv += 1
                End If
            End If

            'loop
            counterEndObj += 1
        Loop

        'antal rörelsevägstyper som ska behandlas
        SwTmoveslutpunktXMLPost = 0

        'tågväg
        If tv > 0 Then
            SpBisObjektObject_tv = True
            SwTmoveslutpunktXMLPost += 1
        End If

        'växlingsväg
        If vv > 0 Then
            SpBisObjektObject_vv = True
            SwTmoveslutpunktXMLPost += 1
        End If

        'Förenkladtågväg
        If ftv > 0 Then
            SpBisObjektObject_ftv = True
            SwTmoveslutpunktXMLPost += 1
        End If

        'Särskildtågväg
        If stv > 0 Then
            SpBisObjektObject_stv = True
            SwTmoveslutpunktXMLPost += 1
        End If

        'Blocksträcka
        If libC > 0 Then
            SpBisObjektObject_lib = True
            SwTmoveslutpunktXMLPost += 1
        End If

        'TAMsträcka
        If tam > 0 Then
            SpBisObjektObject_tam = True
            SwTmoveslutpunktXMLPost += 1
        End If

        'Bevakadtågväg
        If btv > 0 Then
            SpBisObjektObject_btv = True
            SwTmoveslutpunktXMLPost += 1
        End If

        Console.WriteLine("Slutpunkt från array EndObj")
        Console.WriteLine("FKSiObjStID :" & StationIDFiktiv)
        Console.WriteLine("FKSiObjName :" & SpBisObjektNamnSp)
        Console.WriteLine("FKSiObjType :" & SpBisObjektObjectType)
        Console.WriteLine("Antal rörelsetyper som ska behandlas :" & SwTmoveslutpunktXMLPost)
        Console.WriteLine("")
        Console.WriteLine("Tågväg :" & SpBisObjektObject_tv)
        Console.WriteLine("Växlingsväg :" & SpBisObjektObject_vv)
        Console.WriteLine("Förenkladtågväg :" & SpBisObjektObject_ftv)
        Console.WriteLine("Särskildtågväg :" & SpBisObjektObject_stv)
        Console.WriteLine("Blocksträcka :" & SpBisObjektObject_lib)
        Console.WriteLine("TAMsträcka :" & SpBisObjektObject_tam)
        Console.WriteLine("Bevakadtågväg :" & SpBisObjektObject_btv)
        Console.WriteLine("")
    End Sub

#Region "SwTmoves Array EndObj. Läs av antal slutpunkter driftplats"
    Public Sub ArrayEndObjSpSignal()
        Dim EndObjFKSiObjStID As Integer
        Dim EndObjFKSiObjName As String
        Dim EndObjFKSiObjType As String
        Dim EndObjMoveType As String
        Dim MovementsProtectDistance As String 'skyddsträcka
        Dim MovementsProtectleght As String 'skyddsavstånd
        Dim MovementsMovType As String 'typ av rörelse
        Dim MovementsFKStObjName As String 'börjanpunktsnamn
        Dim MovementsFKEObjName As String 'Slutpunktsnamn


        SpTRH = False 'ingen slutpunkt hittad

        'loop EndObj
        Do While SlutpunkterAllaXML < SwTmovesArrayEndObjPost

            'Console.WriteLine("--- loop EndObj:" & SlutpunkterXML & " , " & SwTmovesArrayEndObjPost)
            'Console.WriteLine("")

            'EndObj
            EndObjFKSiObjStID = ArrayEndObje(SlutpunkterAllaXML, 0)
            EndObjFKSiObjName = ArrayEndObje(SlutpunkterAllaXML, 1)
            EndObjFKSiObjType = ArrayEndObje(SlutpunkterAllaXML, 2)
            EndObjMoveType = ArrayEndObje(SlutpunkterAllaXML, 3)

            Console.WriteLine("EndObj: " & EndObjFKSiObjStID & " , " & EndObjFKSiObjName & " , " & EndObjFKSiObjType & " , " & EndObjMoveType)

            'loop Movements
            Do While SwTmovesPeekArrayMovements < SwTmovesArrayMovementsPost

                'Movements
                MovementsFKStObjName = ArrayMovements(SwTmovesPeekArrayMovements, 2)
                MovementsFKEObjName = ArrayMovements(SwTmovesPeekArrayMovements, 5)
                MovementsMovType = ArrayMovements(SwTmovesPeekArrayMovements, 7)
                MovementsProtectDistance = ArrayMovements(SwTmovesPeekArrayMovements, 12)
                MovementsProtectleght = ArrayMovements(SwTmovesPeekArrayMovements, 13)

                Console.WriteLine("riktig slutpunkt för driftplats: " & MovementsFKEObjName & " = " & EndObjFKSiObjName)

                'riktig slutpunkt för driftplats
                If MovementsFKEObjName = EndObjFKSiObjName Then

                    'slutpunkt finns
                    SpTRH = True

                    'läs av BIS data
                    SpBisObjektNamnSp = EndObjFKSiObjName
                    SpBisObjektObjectType = EndObjFKSiObjType

                    'rörelsetyp tågväg
                    If MovementsMovType = "tv" Then
                        SpBisObjektObject_tv = True
                    End If

                    'rörelsetyp växlingsväg
                    If MovementsMovType = "vv" Then
                        SpBisObjektObject_vv = True
                    End If

                    Console.WriteLine("Slutpunkter finns. Börjanpunkt/Slutpunkt : " & MovementsFKStObjName & " , " & MovementsFKEObjName &
                                      " , " & MovementsMovType & " , " & MovementsProtectDistance & " , " & MovementsProtectleght)

                    'slutpunktsobjekt hitttad 
                    Exit Sub
                End If

                'loop nästa
                SwTmovesPeekArrayMovements += 1
            Loop

            'slutpunkter hittad
            If SpTRH = True Then
                SwTmovesPeekArrayMovements = 0

                Console.WriteLine("Slutpunk hittad Slutpunkt/Objekttyp/Tågväg/Växlingsväg: " & SpBisObjektNamnSp & " , " & SpBisObjektObjectType & " , " & SpBisObjektObject_tv &
                                   " , " & SpBisObjektObject_vv)
                Console.WriteLine("")
                Exit Sub
            End If

            'loop nästa
            SlutpunkterAllaXML += 1

            'ingen slutpunkt hittad och är sista post som finns i array EndObj
            If SpTRH = False Then
                SwTmovesPeekArrayMovements = 0

                If SlutpunkterAllaXML >= SwTmovesArrayEndObjPost Then
                    SpTRH = False
                End If
            End If
        Loop
    End Sub
#End Region

#Region "SwTmoves databas Fientliga rörelser mot slutpunkt"
    Public Sub ReadFientligaRorelserSwTmoves(ByVal connection As String, ByVal typrorelse As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            'SQL
            Dim FROM_string As String = "HostileMovements"
            Dim WHERE_string As String = "FKObjStID " & " = " & StationIDFiktiv &
                                         " AND FKObjName " & " = '" & SpBisObjektNamnSp &
                                         "' AND FKObjMovType " & " = '" & typrorelse &
                                         "' AND FKObjType " & " = '" & SpBisObjektObjectType & "'"
            Dim ORDERBY_string As String = "ID"
            Dim SQLquery As String = "SELECT * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("-- SwTmoves databas Fientliga rörelser mot slutpunkt och skapa Array --")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            'startvärde pekare
            SwTmovesArrayFientligaRorelser = 0

            'read rows in ReadEnd
            Do While R_T_Connection.Read

                ArrayFientligaRorelser(SwTmovesArrayFientligaRorelser, 0) = R_T_Connection.Item("ID")
                ArrayFientligaRorelser(SwTmovesArrayFientligaRorelser, 1) = R_T_Connection.Item("FKHostMovID")
                ArrayFientligaRorelser(SwTmovesArrayFientligaRorelser, 2) = R_T_Connection.Item("FKObjStID")
                ArrayFientligaRorelser(SwTmovesArrayFientligaRorelser, 3) = R_T_Connection.Item("FKObjType")
                ArrayFientligaRorelser(SwTmovesArrayFientligaRorelser, 4) = R_T_Connection.Item("FKObjName")
                ArrayFientligaRorelser(SwTmovesArrayFientligaRorelser, 5) = R_T_Connection.Item("FKObjMovType")

                Console.WriteLine("ID:" & ArrayFientligaRorelser(SwTmovesArrayFientligaRorelser, 0))
                Console.WriteLine("FKHostMovID:" & ArrayFientligaRorelser(SwTmovesArrayFientligaRorelser, 1))
                Console.WriteLine("FKObjStID:" & ArrayFientligaRorelser(SwTmovesArrayFientligaRorelser, 2))
                Console.WriteLine("FKObjType:" & ArrayFientligaRorelser(SwTmovesArrayFientligaRorelser, 3))
                Console.WriteLine("FKObjName:" & ArrayFientligaRorelser(SwTmovesArrayFientligaRorelser, 4))
                Console.WriteLine("FKObjMovType:" & ArrayFientligaRorelser(SwTmovesArrayFientligaRorelser, 5))
                Console.WriteLine("")

                SwTmovesArrayFientligaRorelser += 1

            Loop
            Console.WriteLine("Antal fientliga rörelser " & SwTmovesArrayFientligaRorelser & " mot slutpunkt " & SpBisObjektObjectType & " " & SpBisObjektNamnSp)
            Console.WriteLine("")

            'close the connection
            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadFientligaRorelserSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av fientliga rörelser mot slutpunkt.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas Fientliga rörelser mot slutpunkt Alla"
    Public Sub ReadFientligaRorelserSPAllSwTmoves(connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            'SQL
            Dim FROM_string As String = "Movements"
            Dim WHERE_string As String = "ID " & " = " & FientligaRorelserID &
                                         " AND FKEObjType " & " = '" & BisObjektFientligaRorelserType &
                                           "' And MovType " & " = '" & FientligaRorelserTyp & "'"
            Dim ORDERBY_string As String = "FKEObjName"
            Dim SQLquery As String = "SELECT * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("-- SwTmoves databas Gemensam Slutpunkt för alla rörelser--")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            'startvärde pekare
            SwTmovesArrayFientligaRorelserOmEj = 0

            'read rows in ReadEnd
            Do While R_T_Connection.Read
                BisObjektTypFientligaRorelserOmEjSp = R_T_Connection.Item("FKEObjName")

                Console.WriteLine("--Objektnamn gemensam Slutpunkt för alla rörelser: " & BisObjektTypFientligaRorelserOmEjSp)

                SwTmovesArrayFientligaRorelserOmEj += 1
            Loop

            If BisObjektTypFientligaRorelserOmEjSp = "" Then
                Console.WriteLine("")
                Console.WriteLine("--Hittade ingen Slutpunkt för alla rörelser: " & BisObjektTypFientligaRorelserOmEjSp & " --")
                Console.WriteLine("")
            Else
                Console.WriteLine("")
            End If

            'close the connection
            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadFientligaRorelserSPAllSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av fientliga rörelser mot slutpunkt, alla.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas Fientliga rörelser mot en slutpunkt"
    Public Sub ReadFientligaRorelserSPOnlySwTmoves(connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand

        'FientligaRorelserID = R_T_Connection.Item("FKHostMovID") 'Id nummer fientlig rörelse
        'BisObjektFientligaRorelserType = R_T_Connection.Item("FKObjType") 'objekttyp HS..
        'BisObjektNamnFientligaRorelser = R_T_Connection.Item("FKObjName") 'objektnamn
        'FientligaRorelserTyp = R_T_Connection.Item("FKObjMovType") 'typ av rörelse tv,vv.

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            Console.WriteLine("-- SwTmoves databas Fientliga rörelser mot en slutpunkt --")

            'SQL
            Dim FROM_string As String = "Movements"
            Dim WHERE_string As String = "ID " & " = " & FientligaRorelserID &
                                         " AND FKEObjType " & " = '" & BisObjektFientligaRorelserType &
                                           "' And MovType " & " = '" & FientligaRorelserTyp & "'"

            Dim ORDERBY_string As String = "FKEObjName"
            Dim SQLquery As String = "SELECT * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            'startvärde pekare
            Dim counterSP = 0

            'read rows in ReadEnd
            Do While R_T_Connection.Read
                BisObjektIdrörelseFientligaRorelserOmEj = R_T_Connection.Item("ID")

                Console.WriteLine("--Unik Id för rörelseväg slutpunkt för alla rörelser: " & BisObjektIdrörelseFientligaRorelserOmEj)

                counterSP += 1
            Loop



            'close the connection
            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadFientligaRorelserSPOnlySwTmoves: ", ex)
            DisplayError(ex, "Databas läs av Fientliga rörelser mot en slutpunkt.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas Gemensam Slutpunkt för en rörelse"
    Public Sub ReadFientligaRorelserOnlySPSwTmoves(connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand

        'FientligaRorelserID = R_T_Connection.Item("FKHostMovID") 'Id nummer fientlig rörelse
        'BisObjektFientligaRorelserType = R_T_Connection.Item("FKObjType") 'objekttyp HS..
        'BisObjektNamnFientligaRorelser = R_T_Connection.Item("FKObjName") 'objektnamn
        'FientligaRorelserTyp = R_T_Connection.Item("FKObjMovType") 'typ av rörelse tv,vv.

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            Console.WriteLine("-- SwTmoves databas Gemensam Slutpunkt för en rörelse--")

            'SQL
            Dim FROM_string As String = "Movements"
            Dim WHERE_string As String = "ID " & " = " & FientligaRorelserID &
                                         " AND FKEObjType " & " = '" & BisObjektFientligaRorelserType &
                                           "' And MovType " & " = '" & FientligaRorelserTyp & "'"

            Dim ORDERBY_string As String = "FKEObjName"
            Dim SQLquery As String = "SELECT * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            'startvärde pekare
            Dim counterSP = 0

            'read rows in ReadEnd
            Do While R_T_Connection.Read
                BisObjektTypFientligaRorelserOmEjSp = R_T_Connection.Item("FKEObjName")

                Console.WriteLine("--Objektnamn gemensam Slutpunkt för alla rörelser: " & BisObjektTypFientligaRorelserOmEjSp)

                counterSP += 1
            Loop

            Console.WriteLine("--Objektnamn gemensam Slutpunkt för alla rörelser: " & BisObjektTypFientligaRorelserOmEjSp)

            'close the connection
            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadFientligaRorelserOnlySPSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av Gemensam Slutpunkt för en rörelse.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "Read array FientligaRorelser"
    Public Sub ArrayFientligaRorelse()

        FientligaRorelserOmEjID = ArrayFientligaRorelser(FientligaRorelserXML, 0) 'Id nummer om ej.. växel
        FientligaRorelserID = ArrayFientligaRorelser(FientligaRorelserXML, 1) 'Id nummer fientlig rörelse
        BisObjektFientligaRorelserType = ArrayFientligaRorelser(FientligaRorelserXML, 3) 'objekttyp HS..
        BisObjektNamnFientligaRorelser = ArrayFientligaRorelser(FientligaRorelserXML, 4) 'objektnamn
        FientligaRorelserTyp = ArrayFientligaRorelser(FientligaRorelserXML, 5) 'typ av rörelse tv,vv..

        Console.WriteLine("Read array FientligaRorelse")
        Console.WriteLine("ID nr Id nummer om ej.. växel:" & FientligaRorelserOmEjID)
        Console.WriteLine("FKHostMovID nr för fientlig rörelse:" & FientligaRorelserID)
        Console.WriteLine("FKObjType objekttyp HS.:" & BisObjektFientligaRorelserType)
        Console.WriteLine("FKObjName objektnamn:" & BisObjektNamnFientligaRorelser)
        Console.WriteLine("FKObjMovT Typ av rörelse tv,vv..:" & FientligaRorelserTyp)
        Console.WriteLine("")
    End Sub
#End Region

#Region " Moments rörelsevägsID"
    Public Sub MomentsIDFientlighetSwTmoves(ByVal arraynr As Integer)
        BisObjektIdrörelseFientligaRorelserOmEj = ArrayFientligaRorelser(arraynr, 0) 'ID
    End Sub
#End Region

#Region "SwTmoves databas läs av SignalObjects Frontskyddsobjekt"
    Public Sub ReadFientligaRorelseSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand
        Dim check As Integer = 0

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            Console.WriteLine("-- SwTmoves databas läs av SignalObjects Fientliga Rörelser --")

            'SQL
            Dim FROM_string As String = "SignalObjects"
            Dim WHERE_string As String = "FKStationID " & " = " & StationIDFiktiv &
                                         " AND ObjectName " & " = '" & BisObjektTypFientligaRorelserOmEjSp &
                                         "' AND ObjectType " & " = '" & BisObjektFientligaRorelserType & "'"
            Dim ORDERBY_string As String = "ObjectName"
            Dim SQLquery As String = "Select * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            Dim objektnummer As String = ""
            BisObjektObjektnummerientligaRorelser = ""

            'read rows in Stations
            Do While R_T_Connection.Read
                BisObjektObjektnummerientligaRorelser = R_T_Connection.Item("ObjectNR")

                Console.WriteLine("ObjectNR Frontskydd: " & BisObjektObjektnummerientligaRorelser)
                Console.WriteLine("")

                objektnummer = objektnummer & "/" & BisObjektObjektnummerientligaRorelser
                check += 1
            Loop

            'Emtyp
            If BisObjektObjektnummerientligaRorelser = "" Then
                Dim textinfo As String = "Driftplatsnummer: " & StationIDFiktiv & " Objektnamn: " & BisObjektTypFientligaRorelserOmEjSp & " Objekttyp: " & BisObjektFientligaRorelserType &
                    " saknar BIS objektnummer. Sätts defult till 0"

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "K", 1, textinfo, QualityControl_Counter)

                BisObjektObjektnummerientligaRorelser = "0"
                QualityControl_Counter += 1
            End If

            'Only one entry
            If check > 1 Then
                Dim textinfo As String = "Driftplatsnummer: " & StationIDFiktiv & " Objektnamn: " & BisObjektTypFientligaRorelserOmEjSp & " Objekttyp: " & BisObjektFientligaRorelserType &
                    ". Finns flera objekt som har samma BIS objektnummer för 'Frontskyddsobjekt'. Objektnummer: " & objektnummer

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "K", 21, textinfo, QualityControl_Counter)
                QualityControl_Counter += 1
            End If

            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadFientligaRorelseSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av SignalObjects Frontskyddsobjekt.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas läs av fronskyddsobjekt dess objekttyp"
    Public Sub ReadIDFientligaRorelseSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand
        Dim check As Integer = 0

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If
            Console.WriteLine("-- SwTmoves databas läs av fronskyddsobjekt dess objekttyp --")

            'SQL
            Dim FROM_string As String = "ObjektLage"
            Dim WHERE_string As String = "objnr " & " = " & BisObjektObjektnummerientligaRorelser &
                                         " AND bdlnrfr " & " = " & BandelsnummerTemp & ""
            Dim ORDERBY_string As String = "objnr"
            Dim SQLquery As String = "Select * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            Dim objektnummer As String = ""
            BisObjektObjekttypnummerientligaRorelser = ""

            'read rows in Stations
            Do While R_T_Connection.Read
                BisObjektObjekttypnummerientligaRorelser = R_T_Connection.Item("objtypnr")

                Console.WriteLine("Objtypnr fronskyddsobjekt: " & BisObjektObjekttypnummerientligaRorelser)
                Console.WriteLine("")

                objektnummer = objektnummer & "/" & BisObjektObjekttypnummerientligaRorelser
                check += 1
            Loop

            'Emtyp
            If BisObjektObjekttypnummerientligaRorelser = "" Then
                Dim textinfo As String = "Driftplatsnummer: " & StationIDFiktiv & " Objektnamn: " & BisObjektTypFientligaRorelserOmEjSp & " Objekttyp: " & BisObjektFientligaRorelserType &
                    " saknar BIS objekttypnummer. Sätts defult till 0"

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "K", 1, textinfo, QualityControl_Counter)

                BisObjektObjekttypnummerientligaRorelser = "0"
                QualityControl_Counter += 1
            End If

            'Only one entry
            If check > 1 Then
                Dim textinfo As String = "Driftplatsnummer: " & StationIDFiktiv & " Objektnamn: " & BisObjektTypFientligaRorelserOmEjSp & " Objekttyp: " & BisObjektFientligaRorelserType &
                    "Objektnummer: " & BisObjektObjektnummerientligaRorelser & ". Finns flera objekt som har samma BIS Objekttypnummer för 'Frontskyddsobjekt'. Objekttypnummer: " & objektnummer

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "K", 21, textinfo, QualityControl_Counter)
                QualityControl_Counter += 1
            End If

            Console.WriteLine("")

            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadIDFientligaRorelseSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av fronskyddsobjekt dess objekttyp.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas Fientliga rörelser mot slutpunkt IFPointHostMov om växel"
    Public Sub ReadFientligaRorelserIFPointHostMovSwTmoves(connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            Console.WriteLine("-- SwTmoves databas Fientliga rörelser Om ej växel. Skapa array ArrayFientligaRorelserOmEj --")

            'SQL
            Dim FROM_string As String = "IFPointHostMov"
            Dim WHERE_string As String = "FKMovID " & " = " & FientligaRorelserOmEjID & ""
            Dim ORDERBY_string As String = "FKMovID"
            Dim SQLquery As String = "SELECT * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            'startvärde loop
            SwTmovesArrayFientligaRorelserOmEj = 0

            'read rows in IFPointHostMov
            Do While R_T_Connection.Read

                ArrayFientligaRorelserOmEj(SwTmovesArrayFientligaRorelserOmEj, 0) = R_T_Connection.Item("ID")
                ArrayFientligaRorelserOmEj(SwTmovesArrayFientligaRorelserOmEj, 1) = R_T_Connection.Item("FKObjStID")
                ArrayFientligaRorelserOmEj(SwTmovesArrayFientligaRorelserOmEj, 2) = R_T_Connection.Item("FKObjType")
                ArrayFientligaRorelserOmEj(SwTmovesArrayFientligaRorelserOmEj, 3) = R_T_Connection.Item("FKObjName")
                ArrayFientligaRorelserOmEj(SwTmovesArrayFientligaRorelserOmEj, 4) = R_T_Connection.Item("Leg")
                ArrayFientligaRorelserOmEj(SwTmovesArrayFientligaRorelserOmEj, 5) = R_T_Connection.Item("FKMovID")

                Console.WriteLine("ID:" & ArrayFientligaRorelserOmEj(SwTmovesArrayFientligaRorelserOmEj, 0))
                Console.WriteLine("FKObjStID:" & ArrayFientligaRorelserOmEj(SwTmovesArrayFientligaRorelserOmEj, 1))
                Console.WriteLine("FKObjType:" & ArrayFientligaRorelserOmEj(SwTmovesArrayFientligaRorelserOmEj, 2))
                Console.WriteLine("FKObjName:" & ArrayFientligaRorelserOmEj(SwTmovesArrayFientligaRorelserOmEj, 3))
                Console.WriteLine("Leg:" & ArrayFientligaRorelserOmEj(SwTmovesArrayFientligaRorelserOmEj, 4))
                Console.WriteLine("FKMovID:" & ArrayFientligaRorelserOmEj(SwTmovesArrayFientligaRorelserOmEj, 5))

                SwTmovesArrayFientligaRorelserOmEj += 1
            Loop

            If SwTmovesArrayFientligaRorelserOmEj <= 0 Then
                Console.WriteLine("Fientlig rörelseväg saknas för slutpunkt : " & FientligaRorelserOmEjID)
            End If

            Console.WriteLine("")

            'close the connection
            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadFientligaRorelserIFPointHostMovSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av Fientliga rörelser mot slutpunkt IFPointHostMov om växel.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "Read array FientligaRorelser Om Ej"
    Public Sub ArFientligaRorelseOmEj()

        TrafikplatsNummerFientligaRorelserOmEj = ArrayFientligaRorelserOmEj(FientligaRorelserOmEjXML, 1) 'BIS driftplatsnummer
        BisObjektTypFientligaRorelserOmEj = ArrayFientligaRorelserOmEj(FientligaRorelserOmEjXML, 2) 'objekttyp HS..
        BisObjektNamnFientligaRorelserOmEj = ArrayFientligaRorelserOmEj(FientligaRorelserOmEjXML, 3) 'objektnamn
        BisObjektLageFientligaRorelserOmEj = ArrayFientligaRorelserOmEj(FientligaRorelserOmEjXML, 4) 'XML Läge på objekt BIS, höger,vänster..

        Console.WriteLine("--- Array FientligaRorelse Om Ej ---")
        Console.WriteLine("FKObjStID driftplatsnummer :" & TrafikplatsNummerFientligaRorelserOmEj)
        Console.WriteLine("FKObjType objekttyp HS.. :" & BisObjektTypFientligaRorelserOmEj)
        Console.WriteLine("FKObjName objektnamn :" & BisObjektNamnFientligaRorelserOmEj)
        Console.WriteLine("Leg läge på objekt  höger,vänster :" & BisObjektLageFientligaRorelserOmEj)
        Console.WriteLine("FKMovID, ID från fientliga rörelser:" & ArrayFientligaRorelserOmEj(FientligaRorelserOmEjXML, 5))
        Console.WriteLine("")
    End Sub
#End Region

#Region "SwTmoves databas läs av SignalObjects Om Ej"
    Public Sub ReadFientligaRorelseOmEjSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand
        Dim check As Integer = 0

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            Console.WriteLine("-- SwTmoves databas läs av Fientliga rörelser mot slutpunkt om ej växel --")

            'SQL
            Dim FROM_string As String = "SignalObjects"
            Dim WHERE_string As String = "FKStationID " & " = " & StationIDFiktiv &
                                         " AND ObjectName " & " = '" & BisObjektNamnFientligaRorelserOmEj &
                                         "' AND ObjectType " & " = '" & BisObjektTypFientligaRorelserOmEj & "'"
            Dim ORDERBY_string As String = "ObjectName"
            Dim SQLquery As String = "Select * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            Dim objektnummer As String = ""
            BisObjektObjektnummerientligaRorelserOmEj = ""

            'read rows in Stations
            Do While R_T_Connection.Read
                BisObjektObjektnummerientligaRorelserOmEj = R_T_Connection.Item("ObjectNR")

                Console.WriteLine("ObjectNR Om Ej: " & BisObjektObjektnummerientligaRorelserOmEj)
                Console.WriteLine("")

                objektnummer = objektnummer & "/" & BisObjektObjektnummerientligaRorelserOmEj
                check += 1
            Loop

            'Emtyp
            If BisObjektObjektnummerientligaRorelserOmEj = "" Then
                Dim textinfo As String = "Driftplatsnummer: " & StationIDFiktiv & " Objektnamn: " & BisObjektNamnFientligaRorelserOmEj & " Objekttyp: " & BisObjektTypFientligaRorelserOmEj &
                    " saknar BIS objektnummer för 'Fientliga rörelser mot slutpunkt om ej växel'. Sätts defult till 0"

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "SP rörelse frontskydd, saknar objektnummer
                                                        ", 10, textinfo, QualityControl_Counter)

                BisObjektObjektnummerientligaRorelserOmEj = "0"
                QualityControl_Counter += 1
            End If

            'Only one entry
            If check > 1 Then
                Dim textinfo As String = "Driftplatsnummer: " & StationIDFiktiv & " Objektnamn: " & BisObjektNamnFientligaRorelserOmEj & " Objekttyp: " & BisObjektTypFientligaRorelserOmEj &
                    ". Finns flera objekt som har samma BIS objektnummer för 'Fientliga rörelser mot slutpunkt om ej växel'. Objektnummer: " & objektnummer

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "Fientliga rörelser mot slutpunkt om ej växel, fler objektnummer", 50, textinfo, QualityControl_Counter)
                QualityControl_Counter += 1
            End If

            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadFientligaRorelseOmEjSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av SignalObjects Om Ej.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas läs av Om Ej objekttyp"
    Public Sub ReadIDFientligaRorelseOmEjSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim dataAdapterB As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand
        Dim commandB As OleDbCommand
        Dim check As Integer = 0
        Dim checkB As Integer = 0
        Dim arrayObjekttyp(5, 3) As Object

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            'SQL
            Dim FROM_stringB As String = "SignalObjects"
            Dim WHERE_stringB As String = "ObjectNR " & " = " & BisObjektObjektnummerientligaRorelserOmEj & ""
            Dim ORDERBY_stringB As String = "ObjectNR"
            Dim SQLqueryB As String = "Select * " & "FROM " & FROM_stringB & " WHERE " & WHERE_stringB & " ORDER BY " & ORDERBY_stringB

            Console.WriteLine("-- SwTmoves databas läs av om objektid Om Ej objekttyp  --")
            Console.WriteLine("SQL query. " & SQLqueryB)

            ' Create the SelectCommand.
            commandB = New OleDbCommand(SQLqueryB, Project_OleDbCn)
            commandB.CommandTimeout = 20
            dataAdapterB.SelectCommand = commandB
            Dim R_T_ConnectionB = commandB.ExecuteReader

            'read rows in Stations
            Do While R_T_ConnectionB.Read
                arrayObjekttyp(checkB, 0) = R_T_ConnectionB.Item("FKStationID")
                arrayObjekttyp(checkB, 1) = R_T_ConnectionB.Item("ObjectName")
                arrayObjekttyp(checkB, 2) = R_T_ConnectionB.Item("ObjectType")
                arrayObjekttyp(checkB, 3) = R_T_ConnectionB.Item("ObjectNR")

                Console.WriteLine("FKStationID: " & arrayObjekttyp(checkB, 0))
                Console.WriteLine("ObjectName: " & arrayObjekttyp(checkB, 1))
                Console.WriteLine("ObjectType: " & arrayObjekttyp(checkB, 2))
                Console.WriteLine("ObjectNR: " & arrayObjekttyp(checkB, 3))
                Console.WriteLine("")

                checkB += 1
            Loop

            'check ObjectName
            For index As Integer = 0 To checkB - 1

                If Not SpBisObjektNamnSp = arrayObjekttyp(index, 1) Then
                    Dim textinfo As String = "Driftplatsnummer: " & StationIDFiktiv & " Objektnamn: " & BisObjektNamnFientligaRorelserOmEj & " Objekttyp: " & BisObjektTypFientligaRorelserOmEj &
                        " Objektnummer: " & BisObjektObjektnummerientligaRorelserOmEj &
                            ". Finns även på Driftplatsnummer: " & arrayObjekttyp(index, 0) & " men har annat objektnamn: " & arrayObjekttyp(index, 1)

                    WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationID, "Om Ej objekttyp, objektnummer flera driftplatser", 20, textinfo, QualityControl_Counter)
                    QualityControl_Counter += 1
                End If
            Next

            'check ObjectType
            For index As Integer = 0 To checkB - 1
                If Not SpBisObjektObjectType = arrayObjekttyp(index, 2) Then
                    Dim textinfo As String = "Driftplatsnummer: " & StationIDFiktiv & " Objektnamn: " & BisObjektNamnFientligaRorelserOmEj & " Objekttyp: " & BisObjektTypFientligaRorelserOmEj &
                        " Objektnummer: " & BisObjektObjektnummerientligaRorelserOmEj &
                     ". Finns även på Driftplatsnummer: " & arrayObjekttyp(index, 0) & " men har annat objekttyp: " & arrayObjekttyp(index, 2)

                    WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationID, "Om Ej objekttyp, objekttyp flera driftplatser", 21, textinfo, QualityControl_Counter)
                    QualityControl_Counter += 1
                End If
            Next

            Console.WriteLine("-- SwTmoves databas läs av Om Ej objekttyp dess objekttyp --")

            'SQL
            Dim FROM_string As String = "ObjektLage"
            Dim WHERE_string As String = "objnr " & " = " & BisObjektObjektnummerientligaRorelserOmEj & ""
            Dim ORDERBY_string As String = "plnrfr"
            Dim SQLquery As String = "Select * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            Dim objektnummer As String = ""
            BisObjektObjekttypnummerientligaRorelserOmEj = ""

            'read rows in Stations
            Do While R_T_Connection.Read

                'check plnrfr lika med FKStationID i array
                For index As Integer = 0 To checkB - 1

                    'objekttyp och objektnummer
                    If CheckplnfrFKStationID(BisObjektTypFientligaRorelserOmEj, arrayObjekttyp(index, 2), BisObjektObjektnummerientligaRorelserOmEj, arrayObjekttyp(index, 3)) = True Then

                        'stationsID
                        If CheckstationsIdplnfr(arrayObjekttyp(index, 0), R_T_Connection.Item("plnrfr")) = True Then
                            BisObjektObjekttypnummerientligaRorelserOmEj = R_T_Connection.Item("objtypnr")
                            Console.WriteLine("objtypnr : " & R_T_Connection.Item("objtypnr"))
                            Console.WriteLine("")
                            Exit Do
                        End If
                    End If
                Next

                objektnummer = objektnummer & "/ Driftplatsnummer: " & R_T_Connection.Item("plnrfr") &
                                            " Objekttyp: " & R_T_Connection.Item("objtypnr")
                check += 1
            Loop

            'Emtyp
            If BisObjektObjekttypnummerientligaRorelserOmEj = "" Then
                Dim textinfo As String = "Driftplatsnummer: " & StationIDFiktiv & " Objektnamn: " & BisObjektNamnFientligaRorelserOmEj & " Objekttyp: " & BisObjektTypFientligaRorelserOmEj &
                    " saknar BIS objektnummer. Sätts defult till 0"

                Console.WriteLine(textinfo)

                WriteQualityControlVerifieringDataExcel(QualityControl_xlWorksheet, StationIDFiktiv, "K", 1, textinfo, QualityControl_Counter)

                BisObjektObjekttypnummerientligaRorelserOmEj = "0"
                QualityControl_Counter += 1
            End If

            Console.WriteLine("")

            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadIDFientligaRorelseOmEjSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av Om Ej objekttyp.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas läs av Driftplatsnummer Frontskydd"
    Public Sub ReadDriftplatsnummerFrontskyddejSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            'SQL
            Dim FROM_string As String = "Stations"
            Dim WHERE_string As String = "ID" & " = " & TrafikplatsNummerFientligaRorelserOmEj & ""
            Dim ORDERBY_string As String = "TrackSection"
            Dim SQLquery As String = "SELECT * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("--- SQL SwTmoves databas läs av Driftplatsnummer för fientliga rörelse om ej ---")
            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            'read rows in Stations
            Do While R_T_Connection.Read
                TrafikplatsSignaturFientligaRorelserOmEj = R_T_Connection.Item("ShortName")

                Console.WriteLine("ShortName:" & TrafikplatsSignaturFientligaRorelserOmEj)
            Loop

            Console.WriteLine("")

            'close the connection
            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadDriftplatsnummerFrontskyddejSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av Driftplatsnummer Frontskydd.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas läs av Driftplatsnummer Frontskydd"
    Public Sub ReadDriftplatsnummerFrontskyddSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            Console.WriteLine("--- SQL SwTmoves databas läs av Driftplatsnummer ---")

            'SQL
            Dim FROM_string As String = "Stations"
            Dim WHERE_string As String = "ID" & " = " & StationIDFiktiv & ""
            Dim ORDERBY_string As String = "TrackSection"
            Dim SQLquery As String = "SELECT * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            'read rows in Stations
            Do While R_T_Connection.Read
                TrafikplatssignaturFrontskydd = R_T_Connection.Item("ShortName")

                Console.WriteLine("ShortName:" & TrafikplatssignaturFrontskydd)
            Loop

            Console.WriteLine("")

            'close the connection
            Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadDriftplatsnummerFrontskyddSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av Driftplatsnummer Frontskydd.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "SwTmoves databas läs av Driftplatsnummer Om Ej"
    Public Sub ReadDriftplatsnummerOmEjSwTmoves(ByVal connection As String)
        Dim Project_OleDbCn As New OleDb.OleDbConnection(connection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand

        Try
            'open the connection to database
            If Not Project_OleDbCn.State = ConnectionState.Open Then
                Project_OleDbCn.Open()
            End If

            Console.WriteLine("--- SQL SwTmoves databas läs av Driftplatsnummer ---")

            'SQL
            Dim FROM_string As String = "Stations"
            Dim WHERE_string As String = "ID" & " = " & StationIDFiktiv & ""
            Dim ORDERBY_string As String = "TrackSection"
            Dim SQLquery As String = "SELECT * " & "FROM " & FROM_string & " WHERE " & WHERE_string & " ORDER BY " & ORDERBY_string

            Console.WriteLine("SQL query. " & SQLquery)

            ' Create the SelectCommand.
            command = New OleDbCommand(SQLquery, Project_OleDbCn)
            command.CommandTimeout = 20
            dataAdapter.SelectCommand = command
            Dim R_T_Connection = command.ExecuteReader

            'read rows in Stations
            Do While R_T_Connection.Read
                TrafikplatsSignaturFientligaRorelserOmEj = R_T_Connection.Item("ShortName")

                Console.WriteLine("ShortName:" & TrafikplatsSignaturFientligaRorelserOmEj)
            Loop

            Console.WriteLine("")

            'close the connection
            'Project_OleDbCn.Close()

        Catch ex As Exception
            MakeErrorReport("ReadDriftplatsnummerOmEjSwTmoves: ", ex)
            DisplayError(ex, "Databas läs av Driftplatsnummer Om Ej.")
            Project_OleDbCn.Close()
        End Try

    End Sub
#End Region

#Region "Läs av BIS objektyp med projekterad typ"
    Public Function ReadObjekttypBIS(ByVal BisObjectType As String) As String
        Dim BisType = "-1"

        'objekttyp
        Select Case BisObjectType

            'typ 6000
            Case "cc", ""

                BisType = "6000"

            Case "cc", ""
                BisType = "6000"
        End Select

        'objekttyp finns ej implementerad
        If BisType = "-1" Then
            MessageBox.Show("Objekttyp ej implementerad. Kontakta programmerare. Objekttyp: " & BisObjectType)
        End If

        Return BisType
    End Function
#End Region

    Public Sub SlutpunktsObjStIDSwTmoves(ByVal slutpunkterXML As Integer)
        SpBisObjektNamnSp = ArrayEndObje(slutpunkterXML, 1) ' ObjectName
        SpBisObjektObjectType = ArrayEndObje(slutpunkterXML, 2) ' ObjectType
    End Sub


    Public Sub initDataAdatpter(ByVal connection As String)
        Dim Sql As String = "SELECT * FROM OrginalName;"
        OrginalAdapter = New OleDbDataAdapter(Sql, connection)
    End Sub

#Region "Error Handler"
    Private Sub DisplayError(ByVal ex As Exception, ByVal Errortext As String)
        Dim errText As String = "Error - " & vbCrLf
        Do While (Not ex Is Nothing)
            errText = "Funktion " & Errortext & errText & errText & ex.Message & vbCrLf
            ex = ex.InnerException
        Loop
        MessageBox.Show(errText, "Ett fel är upptäckt.", MessageBoxButton.OK, MessageBoxImage.Error)

    End Sub
#End Region

End Module
