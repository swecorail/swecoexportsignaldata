﻿Imports System.Data
Imports System.Xml

Public Class VtoolsDatasetClass
    Private _DSet As DataSet
    Public ReadOnly Property VägDelar As DataTable
        Get
            Return _DSet.Tables("RoadParts")
        End Get
    End Property

    Public ReadOnly Property VägarAvVägdelar As DataTable
        Get
            Return _DSet.Tables("RoadsOfRoadParts")
        End Get
    End Property

    Public ReadOnly Property Fientligheter As DataTable
        Get
            Return _DSet.Tables("RoadsOfRoadParts")
        End Get
    End Property

    Public ReadOnly Property FrontSkydd As DataTable
        Get
            Return _DSet.Tables("FrontProtection")
        End Get
    End Property

    Public ReadOnly Property Slutpunkter As DataTable
        Get
            Return _DSet.Tables("Slutpunkt")
        End Get
    End Property

    Public Sub New()
        _DSet = New DataSet
    End Sub

    Public Sub XmlRead(FilePath As String)
        Dim xReader As XmlReader
        xReader = XmlReader.Create(FilePath)
        _DSet.ReadXml(xReader)
    End Sub


End Class
